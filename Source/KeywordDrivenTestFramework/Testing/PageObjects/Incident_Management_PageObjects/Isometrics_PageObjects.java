/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author SJonck
 */
public class Isometrics_PageObjects extends BaseClass {

    public static String assistAdd() {
        return "//div[@sourceid='30D4E41B-74D7-48DE-BBCB-EFEAD1F5C86B']//div[@title='Add']";
    }
    
    public static String records(){
        return "//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@id='divPager'][@class='recnum']/div[@class='record']";
    }

    public static String assetsTabXpath() {
        return "//div[@id='control_0F686E95-8AB8-4A68-85B3-7670D78BA16A']//span[text()='Assets']";//div[@id='control_0F686E95-8AB8-4A68-85B3-7670D78BA16A']//span[text()='Assets']
    }

    public static String processDropdownXpath() {
        return "//div[@id='control_3CD8F578-F09A-4506-BCC1-2E78DEB8775A']";
    }

    public static String processDescriptionXpath() {
        return "//div[@id='control_3CD8F578-F09A-4506-BCC1-2E78DEB8775A']//input";
    }

    public static String rtoRequiredDropdownXpath() {
        return "//div[@id='control_4DC86A43-C638-4786-9011-64C5F786E56F']";
    }

    public static String mandatoryDropdownXpath() {
        return "//div[@id='control_4548477B-C54F-4BF7-94E6-1501F86953C9']";
    }

    public static String backUpDropdown() {
        return "control_984AFB85-44EF-4D76-9317-A057CEC8C400";
    }

    public static String chevron_Down_ButtonXPath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@class='more options icon chevron down']";
    }

    public static String save_And_Close_XPath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[text()='Save and close']";
    }

    public static String save_XPath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[text()='Save']";
    }

    public static String concession_Status_XPath() {//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[text()='']
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//a[text()='Status']/../../../../../../..//td[@role='gridcell'][9]//div";
    }

    public static String data_Item_Concessions_XPath(String name) {
        return "//div[text()='" + name + "']";
    }

    public static String concession_Status_Dropdown_XPath() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//b[@class='select3-down']";
    }

    public static String concessions_Loading_HiddenXPath() {
        return "//div[@id='divFormLoading'][@class='form active transition hidden']";
    }

    public static String concessions_Loading_VisibleXPath() {
        return "//div[@id='divFormLoading'][@class='form active transition visible']";
    }

    public static String verification_And_Additional_Loading_VisibleXPath() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer active']//div[@class='ui text loader'][text()='Loading data...']";
    }

    public static String non_Editable_Grid_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[@class='grid k-grid k-widget k-reorderable']";
    }

    public static String search_Button_ConcessionXPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//span[@class='icon filter']/..//div[@title='Search']";
    }

    public static String search_Button_On_Filter_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[@id='act_filter_right']//div[@title='Search']";
    }

    public static String status_Filter_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//td[text()='Status']/..//a[@class='select3-choice']";
    }

    public static String table_Loading_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[@class='k-loading-image']";
    }

    public static String incident_Investigation_Tab_Xpath() {
        return "//li[@id='tab_3A331B31-BF4C-4C42-ADB3-936999FEE515']//div[text()='3.Incident Investigation']";
    }

    public static String process_Flow_Button_XPath() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//span[@original-title='Process flow']";
    }

    public static String investigation_Planning_Tab_XPath() {
        return "//div[@id='control_4A4AC927-B046-4637-B3BF-E289C923C98F']//div[text()='Investigation Planning']";
    }

    public static String supporting_Information_XPath() {
        return "//div[@id='control_9A5398B3-88BB-4636-923E-C08D32F88C92']//span[text()='Supporting information to guide the investigation team during the investigation']";
    }

    public static String data_Collection_Editable_Grid_XPath() {
        return "//div[@id='control_643A1426-B1AC-487E-983B-29CB33C5E342']//div[@class='grid k-grid k-widget k-reorderable editable']";
    }

    public static String add_Data_Collection_Button_XPath() {
        return "//div[@id='control_643A1426-B1AC-487E-983B-29CB33C5E342']//div[@title='Add']";
    }

    public static String file_Name_Data_Collection_Field_XPath() {
        return "//div[@id='control_96582EB8-B3CF-49C2-A0B2-86E1502C7918']//input[not(@style='display: none;')]";
    }

    public static String description_Data_Collection_Field_XPath() {
        return "//div[@id='control_F6FD8C0D-ECBA-4546-80C0-19E86BE85307']//input[not(@style='display: none;')]";
    }

    public static String save_New_Data_Collection_Item_XPath() {
        return "//div[@id='control_643A1426-B1AC-487E-983B-29CB33C5E342']//div[@title='Save']";
    }

    public static String InvestigationDueDateXPath() {
        return "//div[@id='control_7E7E7BF6-08EF-4008-BEDA-5B7E422D739F']//input";
    }

    public static String InvestigationScopeTextAreaXPath() {
        return "//div[@id='control_669476AC-06EA-4FAE-9872-D47FB2469048']//textarea";
    }

    public static String InvestigationDetailTabXPath() {
        return "//li[@id='tab_6F0A773F-7860-4EF0-BD5F-7E77A654B69B']//div[text()='Investigation Detail']";
    }

    public static String investigationDueDate() {
        return "//div[@id='control_7E7E7BF6-08EF-4008-BEDA-5B7E422D739F']//input";
    }

    public static String investigationScope() {
        return "//div[@id='control_669476AC-06EA-4FAE-9872-D47FB2469048']//textarea";
    }

    public static String threeIncidentsInvesgationXpath() {
        return "//div[text()='3.Incident Investigation']";
    }

    public static String ProcessActivityDropdownXPath() {
        return "//div[@id='control_CC6900A0-1399-4340-AB58-7D532D79BEC8']//li";
    }

    public static String RiskSourceDropdownXPath() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//li";
    }

    public static String RiskDropdownXPath() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//li";
    }

    public static String RiskDropdownXPath1() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//b[@class='select3-down drop_click']";
    }

    public static String InvestigationTypeDropdownXPath() {
        return "//div[@id='control_9033FCFD-1EC6-4C92-AFF0-0AAF4B37E191']//li";
    }

    public static String InvestigationTypeDropdownXPath1() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//b[@class='select3-down drop_click']";
    }

    public static String MechanicalSelectionXPath() {
        return "//*[@id='777c5cc0-1af2-499d-96a5-794cfe6293da']/i";
    }

    public static String FullInvestigationSubTabXPath() {
        return "//li[@id='tab_4B20A9A5-81C9-4639-BD19-06BA7E49D204']//div[text()='Full Investigation']";
    }

    public static String CriticalControlsLinkedToThisBusinessProcessXPath() {
        return "//div[@id='control_EC8ABFCB-A4A6-4A97-B272-4A9FB36E18C5']//span[text()='Critical controls linked to this Process/Activity']";
    }

    public static String WhyAnalysisPanelXPath() {
        return "//div[@id='control_0894E664-8086-41C8-BE16-C302D23A6582']//span[text()='Why Analysis']";
    }

    public static String WhyAnalysisAddButton() {
        return "//span[text()='Why Analysis']/../..//div[@title='Add']";
    }

    public static String noResults() {
        return "//div[@instanceid='FA8CDFB6-9132-4F68-91C2-C9E35BD1FEA9']//div[text()='No results returned']";
    }

    public static String whyAnalysisOrder() {
        return "//div[@id='control_7831FA38-FB43-46A4-893D-42D365D66A05']//input[@type='number']";
    }

    public static String whyAnalysisText() {
        return "//div[@id='control_520640B0-0730-4F01-9D34-D185B8CCDA27']//textarea";
    }

    public static String whyAnalysisAnswerText() {
        return "//div[@id='control_5A999D23-9FE7-4E9D-90E5-8A467467AB6A']//textarea";
    }

    public static String saveWhyAnalysisXpath() {
        return "//div[@sourceid='FA8CDFB6-9132-4F68-91C2-C9E35BD1FEA9']//td//div[@onclick]";
    }

    public static String RelatedUnwantedEventsXPath() {
        return "";
    }

    public static String RelatedRisksXPath() {
        return "//div[@id='control_1A82D518-EC5B-490F-A7C9-C1A94331EADC']//span[text()='Related Risks']";
    }

    public static String RelatedBowtiesXPath() {
        return "//div[@id='control_BE8F6FE0-ABD8-4DCD-9E83-960D64347B9E']//span[text()='Related Bowties']";
    }

    public static String RelatedJSAPanelXPath() {
        return "//div[@id='control_01B1F475-DC2E-4EEF-B0B4-6272EC0C05FC']//span[contains(text(),'Related JSA')]";
    }

    public static String RelatedObligationsPanelXPath() {
        return "//div[@id='control_235A4359-3C38-430A-96DA-28C5BD0FC7F0']//span[text()='Related Obligations']";
    }

    public static String RelatedPoliciesAndProceduresPanelXPath() {
        return "//div[@id='control_A70C27AC-8845-43CB-A2A9-060982C79937']//span[text()='Related Policies & Procedures']";
    }

    public static String controlAnalisisTab() {
        return "//span[text()='Critical controls linked to this Process/Activity']";
    }

    public static String controlAnalisisAddButon() {
        return "//span[text()='Critical controls linked to this Process/Activity']/../..//div[@title='Add']";
    }

    public static String controlDropdown() {
        return "//div[@id='control_BE70E991-0197-4A0A-970F-3457B2970BE0']";
    }

    public static String controlDescription() {
        return "//div[@id='control_599A83BE-64A4-4FAD-B12E-E8EC37E1F20F']//textarea";
    }

    public static String controlAnalysisDropdown() {
        return "//div[@id='control_3DC7FD7E-2E4E-4AAE-9374-AD5434B5DAB8']";
    }

    public static String causeSelectionDropdown() {
        return "//div[@id='control_EACB40D1-4D8E-4D6E-9137-20FBE2151CC0']";
    }

    public static String description() {
        return "//div[@id='control_4D375CF2-013F-4D43-84AE-F61E75DC7711']//textarea";
    }

    public static String controlFollowProDropdown() {
        return "//div[@id='control_A8FE6042-A8DE-49BD-9118-EE45F5E2AD3A']";
    }

    public static String controlFlowProcess() {
        return "//div[@id='btnProcessFlow_form_821BC9A7-867F-410A-9837-A4A8D3AFD781']";
    }

    public static String relatedLinkXpath() {
        return "//span[text()='Related Link']";
    }

    public static String anyDropdwonOptionXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String responsiblePerson(String option) {
        return "//div[contains(@class, 'transition visible')][contains(@class, 'select3-drop select3-drop-ddl')]//a[text()='" + option + "']";
    }

    public static String activeAddPhaseXpath() {
        return "//div[text()='Add phase']//../..//div[@class='step active']";
    }

    public static String activeEditPhaseXpath() {
        return "//div[text()='Edit phase']//../..//div[@class='step active']";
    }

    public static String controlAnalysisSaveButoon() {
        return "//div[@id='btnSave_form_821BC9A7-867F-410A-9837-A4A8D3AFD781']";
    }

    public static String supportDocumentsPanel() {
        return "//span[text()='Supporting Documents']";
    }

    public static String controlAnalysisId() {
        return "//div[@id='form_821BC9A7-867F-410A-9837-A4A8D3AFD781']//div[@class='recnum']//div[@class='record']";
    }

    public static String controlAnalysisActionId() {
        return "//div[@id='form_C8F39E88-B969-4F49-9E2C-89FB6942CB29']//div[@class='recnum']//div[@class='record']";
    }
    public static String incidentActionId() {
        return "//div[@id='form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[@class='recnum']//div[@class='record']";
    }


    public static String anyDropDownCheckXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']//../i[@class='jstree-icon jstree-checkbox']";
    }

    public static String absentDetails() {
        return "//div[@id='control_A90D4EB2-E421-4641-8079-B55D41F4DEE1']//textarea";
    }

    public static String individualDetails() {
        return "//div[@id='control_7FBCB2BE-8B9C-4F36-A8E8-BEE9000740BF']//textarea";
    }

     public static String taskDetails() {
        return "//div[@id='control_EFAE1279-7357-4BB0-BD0C-FC039FFAE35C']//textarea";
    } 
    public static String teamDetails() {
        return "//div[@id='control_7FBCB2BE-8B9C-4F36-A8E8-BEE9000740BF']//textarea";
    }
    
      public static String organisationalDetails() {
        return "//div[@id='control_A2E8FDDA-F1FB-4534-8866-23F173C6991A']//textarea";
    }

    public static String saveIncidentManagementFullInvestigation() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String actionDescription() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String departmentResponsibleDropdown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']";
    }

    public static String responsiblePersonDropdown() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']";
    }

    public static String actionDueDate() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String controlAnalysisActionTab() {
        return "//span[text()='Actions']";
    }

    public static String controlAnalysisActionAddButton() {
        return "//span[text()='Actions']/../..//div[@title='Add']";
    }

    public static String controlAnalysisActionProcessButton() {
        return "//div[@id='btnProcessFlow_form_C8F39E88-B969-4F49-9E2C-89FB6942CB29']";
    }
    
      public static String incidentActionProcessButton() {
        return "//div[@id='btnProcessFlow_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']";
    }

    public static String controlAnalysisActionSaveButton() {
        return "//div[@id='btnSave_form_C8F39E88-B969-4F49-9E2C-89FB6942CB29']";
    }
    
     public static String incidentActionsSaveButton() {
        return "//div[@id='btnSave_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']";
    }

    public static String activeToBeInitiatedXpath() {
        return "//div[text()='To be initiated']//../..//div[@class='step active']";
    }

    public static String ReasonForNoInvestigationFieldXPath() {
        return "//div[@id='control_EA250AFB-33A8-49BF-A022-51FBB6200AB5']//textarea";
    }

    public static String fullInvestigationTab() {
        return "//div[text()='Full Investigation']";
    }
    
    public static String actionTab(){
        return "//div[text()='Actions']";
    }
    
    public static String incidentActionAdd(){
        return "//div[text()='Incident Actions']/..//div[@title='Add']";
    }

}
