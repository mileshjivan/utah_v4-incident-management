/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author EMashishi
 */
public class InjuryClaimPageObject {

    public static String injuryClaimTab() {
        return "//div[text()='Injury Claim']";
    }
      public static String prevoiusRelatedInjuriesTable() {
        return "//div[text()='Previous Related Injuries']";
    }
    public static String thirdPartyDiplayedXpath(){
        return "//div[text()='Third party']";
    }

    public static String injuredPersonsSaveXpath() {
        return "//div[@id='btnSave_form_510481BC-5774-4957-920F-C8323A843BF9']";
    }

    public static String addDetailsVXpath() {
        return "//div[text()='2.Verification and Additional Detail']";
    }

    public static String injuryClaimAdd() {
        return "//div[@id='control_2EBAD31C-8852-480F-A8FA-D7E591BC4C01']//div[@i18n='add_new']";
    }

    public static String specificLoca() {
        return "//div[@id='control_2CC94640-D7B5-465D-8299-AD0DE2683B9C']//textarea";
    }

    public static String workersTabXpath() {
        return "//span[contains(text(),'Personal Details ')]";
    }

    public static String workersTabCheckOpenXpath() {
        return "//div[@class='c-pnl open']//span[contains(text(),'Personal Details')]";
    }

    public static String workersTabCloseXpath() {
        return "//div[@class='c-pnl']//span[contains(text(),'Personal Details')]";
    }

    public static String employeeDropdownXpath() {
        return "//div[@id='control_427765C1-D6C0-464A-95D9-B1772FD1F73E']";
    }

    public static String anySupervisorXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }
    
    public static String anyOptionCheckXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']//i[@class='jstree-icon jstree-checkbox']";
    }
    
     public static String responsiblePerson(String option) {
        return "//div[contains(@class, 'transition visible')][contains(@class, 'select3-drop select3-drop-ddl ')]//a[text()='" + option + "']";
    }

    public static String compensationClaimDropdownXpath() {
        return "//div[@id='control_D40A7BA0-995F-4E3B-87FE-F687E7505DAE']";
    }

    public static String specialCommunicationXpath() {
        return "//div[@id='control_00BB6CD7-2583-47F6-AAD6-21F4F69F1073']//textarea";
    }

    public static String languageDropdownXpath() {
        return "//div[@id='control_C0C63DF8-B710-4DD6-8952-8DB8CCF92CC3']";
    }

    public static String partnerDropDownXpath() {
        return "//div[@id='control_3592E91C-9306-4993-B508-7C334963F070']";
    }

    public static String fullTimeStudentsXpath() {
        return "//div[@id='control_B7013667-7290-4363-9D47-F971C6E50A64']";
    }
    public static String pleaseProvideTheDateOfBirthXpath() {
        return "//div[@id='control_A81EF86A-0DED-4056-A602-DDCE98E0D1CB']//input";
    }

    public static String averageGrossXpath() {
        return "//div[@id='control_22129204-6ABE-4D07-8380-57A892FC61D4']//input[@language]";
    }

    public static String IncidentTabXpath() {
        return "//div[@class='c-pnl open']//span[contains(text(),'Incident and ')]";
    }

    public static String IncidentTabOpneXpath() {
        return "//span[contains(text(),'Incident and ')]";
    }

    public static String whatHappenedXpath() {
        return "//div[@id='control_A4D11B61-B241-4960-A3B1-1BCD83803593']//textarea";
    }

    public static String youWereInjuredXpath() {
        return "//div[@id='control_A259DE1C-1AB0-414A-8C0F-7BA41BF103CA']//textarea";
    }

    public static String responsibleEmployerXpath() {
        return "//div[@id='control_93394F16-15BC-40C0-A47F-BEFB476EB19E']//input[@language]";
    }

    public static String accidentWasReportedXpath() {
        return "//div[@id='control_260C1626-D20A-47F9-BE9E-41F90C034CE3']//input[@language]";
    }

    public static String involvedVehiclesXpath() {
        return "//div[@id='control_DECA5907-FDC5-4F1C-BEAC-9448515BB06C']//input[@language]";
    }

    public static String StateXpath() {
        return "//div[@id='control_A3D725C3-EBD4-4F97-B210-9ED78B9DBA6E']//input[@language]";
    }

    public static String injuryCausesDropdownXpath() {
        return "//div[@id='control_26B71E34-9805-47AA-9148-AA093BEE3265']";
    }

    public static String additionalDescriptionXpath() {
        return "//div[@id='control_BBAF50B9-B3F0-43C0-9FCA-E8D138C97E88']//textarea";
    }

    public static String injuryDateXpath() {
        return "//div[@id='control_15783961-124B-45B9-A141-4AEBFC09AD65']//input";
    }

    public static String injuryTimeXpath() {
        return "//div[@id='control_85E24D88-0ADA-4474-B34D-8A960BF89D9D']//input";
    }

    public static String injuryConditionsXpath() {
        return "//div[@id='control_4C2CA712-17E8-420E-8E91-32DE889E8D5B']//input[@language]";
    }

    public static String stoppedWorkingDateXpath() {
        return "//div[@id='control_30C70E42-071D-47DF-A1C7-EF37D67919E5']//input";
    }

    public static String stoppedWorkingTimeXpath() {
        return "//div[@id='control_575F2C35-CF19-4D3A-9E71-22B6E75431D8']//input";
    }

    public static String reportDateXpath() {
        return "//div[@id='control_6C059C7D-83CB-4BDD-A7D9-684CD55E15AB']//input";
    }

    public static String prevoiusInjuryDropdownXpath() {
        return "//div[@id='control_DD187060-9792-40D3-980E-A151FE29E8CA']";
    }

    public static String workerEmploymentDetailsTab() {
        return "//div[@id='control_019E24C6-592C-48A2-A3EE-5E330FC90C63']//div[@class='c-pnl-heading']";
    }
    
     public static String workerEmploymentDetailsOpenTab() {
        return "//div[@id='control_3AE185E0-483D-4FD7-A3D2-3C6AA5CF1CCE']//span[contains(text(),'Worker')]/../../../div[@class='c-pnl open']";
    }
     public static String workerEmploymentDetailsClosedTab() {
        return "//div[@id='control_019E24C6-592C-48A2-A3EE-5E330FC90C63']//span[contains(text(),' Employment Details ')]/../../..//div[@class='c-pnl']";
    }


    public static String nameOrgPayingInjuredXpath() {
        return "//div[@id='control_958411C5-A30B-4B5F-A6AF-462BA0E390EC']//input[@language]";
    }

    public static String workPlaceAddressXpath() {
        return "//div[@id='control_A77D3E4F-7257-482C-9B59-511BC08178E9']//textarea";
    }

    public static String suburbXpath() {
        return "//div[@id='control_84491A7F-2C1A-4424-BC34-CBA180927BD1']//input[@language]";
    }

    public static String stateXpath() {
        return "//div[@id='control_E5753694-2F80-4F47-A368-34059C508827']//input[@language]";
    }

    public static String postCodeXpath() {
        return "//div[@id='control_3D492023-2214-4633-82F4-63E23A2EA45D']//input[@language]";
    }

    public static String nameofEmployerXpath() {
        return "//div[@id='control_442D04B7-12E3-47AA-BB21-62CCBD81D39D']//input[@language]";
    }

    public static String NumOfEmployerXpath() {
        return "//div[@id='control_268B8A0F-281B-4A58-88A2-89782CAB0970']//input[@language]";
    }

    public static String usualOccupationXpath() {
        return "//div[@id='control_1E5531A2-2DEB-4A9A-A9D5-1373D395E38C']//input[@language]";
    }

    public static String startedWorkDateXpath() {
        return "//div[@id='control_77208834-CFE0-4F7D-A349-FDCB8511F83D']//input";
    }

    public static String employmentTimInjuredDropdownXpath() {
        return "//div[@id='control_6624AFDA-BFAD-4F31-A7E3-1E8A33AA3B42']";
    }

    public static String addDetailsXpath() {
        return "//div[@id='control_E4899E89-D6F3-4836-A3B0-63C67E10DF32']//textarea";
    }

    public static String workerEarningTab() {
        return "//div[@id='control_79B87E14-C858-48CA-BC90-E2161C65B3C4']//div[@class='c-pnl-heading']";
    }

    public static String hoursWorkedXpath() {
        return "//div[@id='control_424AEDE8-8F93-4225-865E-D17061BB3930']//input[@language]";
    }

    public static String workingTimeFromXpath() {
        return "//div[@id='control_F2331164-D27F-4560-A25A-6F25192E2363']//input";
    }

    public static String workingTimeToXpath() {
        return "//div[@id='control_88B529BB-CDD3-4357-9D70-ADAB2EDAD6E3']//input";
    }

    public static String weeklyShiftXpath() {
        return "//div[@id='control_775AB4AB-AE44-4E05-8C62-4601A58BB57D']//input[@language]";
    }

    public static String preTaxRateXpath() {
        return "//div[@id='control_B558E3E5-987E-46A0-870E-9EA826AEF168']//input[@language]";
    }

    public static String overtimeAlloXpath() {
        return "//div[@id='control_52BD610B-3C60-4686-BC76-49BAF1D756DA']//input[@language]";
    }

    public static String preTaxRateWeeklyXpath() {
        return "//div[@id='control_64554F81-C93F-4CBF-919B-A9886E6265F0']//input[@language]";
    }

    public static String overtimeWorkedXpath() {
        return "//div[@id='control_16813406-C04A-4372-8948-35348DF97B6B']//input[@language]";
    }

    public static String returnWorkDetailsTab() {
        return "//span[text()='Treatment and Return To Work Details ']";
    }
    
      public static String returnWorkDetailsOpenTab() {
        return "//div[@class='c-pnl open']//span[text()='Treatment and Return To Work Details ']";
    }

    public static String doctorTreatmentXpath() {
        return "//div[@id='control_C250CEB7-3E21-490F-8D24-697B28321B10']//input[@language]";
    }

    public static String numOfTreatmentXpath() {
        return "//div[@id='control_A5563576-1F72-474D-89B4-77629268EFC0']//input[@language]";
    }

    public static String nameOfClinicXpath() {
        return "//div[@id='control_E559E8D8-5C42-4AB4-87A6-0CA5BC9FB80C']//textarea";
    }

    public static String contactDetailsXpath() {
        return "//div[@id='control_2354211E-71A2-4354-8E85-16273E1A544A']//input[@language]";
    }

    public static String sameEmployerDropdownXpath() {
        return "//div[@id='control_C92060F3-6CCF-43C4-89A0-B409DC1CB6E7']";
    }

    public static String returnDateXpath() {
        return "//div[@id='control_EC7D9DE7-5F93-4BE7-B13A-B237A8A1A564']//input";
    }
    
     public static String hourWorked() {
        return "//div[@id='control_508DD233-9746-4126-BBF4-212645186225']//input";
    }
    
    public static String newEmployerNameXpath() {
        return "//div[@id='control_69A0C044-EDC4-42AB-9F2F-06AA033E668B']//input[@language]";
    }
    
    public static String newEmployerContactXpath() {
        return "//div[@id='control_275E3BF2-9C0C-4484-A730-78F60698FCD4']//input[@language]";
    }

    public static String howHoursWrkedXpath() {
        return "//div[@id='control_508DD233-9746-4126-BBF4-212645186225']//input";
    }

    public static String dutiesdropdownXpath() {
        return "//div[@id='control_C7C234C4-7E8E-46ED-95E3-55727D8A5670']";
    }
    
     public static String returnNewEmployersdropdownXpath() {
        return "//div[@id='control_94CCD94B-FDAA-4911-8E77-49BDF8FF9DC4']";
    }

    public static String formToYouremployerddropdownXpath() {
        return "//div[@id='control_4311A037-5F9A-425F-B981-8AEA23353F57']";
    }

    public static String returnClaimsDateXpath() {
        return "//div[@id='control_818C6AD7-6E86-427C-9227-E75BFE0B049C']//input";
    }

    public static String DateOfMedCertDateXpath() {
        return "//div[@id='control_65BEE57D-01FD-4066-A8C4-F94AB14BEFA1']//input";
    }

    public static String employerLodgementDetailsTab() {
        return "//span[text()='Employer Lodgement Details ']";
    }

    public static String firstDateCompletedCliamsFormXpath() {
        return "//div[@id='control_D900DDBF-5DCB-47C9-978D-4FA49838A098']//input";
    }

    public static String firstDateMedCertXpath() {
        return "//div[@id='control_43BFA093-C65F-4B8B-B2E3-7E0D406F1382']//input";
    }

    public static String dateClaimAgentXpath() {
        return "//div[@id='control_92058FCF-3B95-43E4-985C-6E78D50ADC2F']//input";
    }

    public static String claimCostXpath() {
        return "//div[@id='control_0C29DECD-5760-41E3-8952-0CE411F1D8E5']//input[@language]";
    }

    public static String lostTimeXpath() {
        return "//div[@id='control_ECAB367F-EC0F-4296-9CA2-AF011C9057BC']//input";
    }

    public static String employerDropdownXpath() {
        return "//div[@id='control_1B0973DF-6798-419F-8D51-EADC2FB6284D']";
    }

    public static String positionXpath() {
        return "//div[@id='control_EA313F9F-252F-4FC5-A0B2-4D44905DD062']//input[@language]";
    }

    public static String schemeNumberXpath() {
        return "//div[@id='control_F5AFF548-675B-4310-A268-891D3F1DF0B5']//input[@language]";
    }

    public static String injuryCliamFormSave() {
        return "//div[@id='btnSave_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']";
    }

    public static String closeInjuryClaimForm() {
        return "//div[@id='form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String dutiesOption(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String waitTableInjuryClaim() {
        return "//div[@instanceid='D6F2F0CD-C202-4238-92BF-3A91A2B306D7'][@data-role]//table[@data-role]";
    }

    public static String closeInjuredPersonsForm() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }
    
    public static String thirdPartyDropdown(){
        return "//div[@id='control_26B71E34-9805-47AA-9148-AA093BEE3265']";
    }
    
    public static String employessDropdown(){
        return "//div[@id='control_427765C1-D6C0-464A-95D9-B1772FD1F73E']";
    }
    
    public static String languageDropDown(){
        return "//div[@id='control_C0C63DF8-B710-4DD6-8952-8DB8CCF92CC3']";
    }
    
    public static String communicationDisability(){
        return "//div[@id='control_00BB6CD7-2583-47F6-AAD6-21F4F69F1073']//textarea";
    }

}
