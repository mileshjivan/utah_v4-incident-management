/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author SJonck
 */
public class MonitoringAirPageObjects {

    public static String EnvironmentTabXPath() {
        return "//div[@id='control_BC138B8D-FA48-4352-A20A-7FB5AF468D6C']//div[text()='Environment']";
    }
    
    public static String anyActiveDropdownXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String refButton() {
        return "//div[@id='control_942C9A46-36E7-4872-A014-1661890599BF']//a[@class='k-pager-refresh k-link']";
    }
      public static String refButtonWater() {
        return "//div[@id='control_00278E07-DE4B-4ED8-8A29-41A4557D137C']//a[@class='k-pager-refresh k-link']";
    }


    public static String monitorReqquiredCheckbox() {
        return "//div[@id='control_9AD21B46-7462-4311-9D26-146E3DC51234']";
    }

    public static String TypeOfMonitoringAreaCheckbox(String monitor) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + monitor + "')]//../i[@class='jstree-icon jstree-checkbox']";
    }

    public static String monitoringAreasTapXpath(String monitor) {
        return "//div[text()='" + monitor + "']";
    }

    public static String monitorPanelXpath() {
        return "//span[text()='Monitoring']";
    }

    public static String businessUnitDropdownXpath() {
        return "//div[@id='control_739C430A-AAD9-47C4-80E3-30FE13AB4D33']";
    }

    public static String waterBusinessUnitDropdownXpath() {
        return "//div[@id='control_2DA9BFE1-0DFB-49D8-B8FA-91C41588F17F']";
    }
    
    public static String waterTypeDropdownXpath() {
        return "//div[@id='control_ECCB6A13-45D6-4733-B5BA-19D53E478387']";
    }
     public static String waterMeasurementTypeDropdownXpath() {
        return "//div[@id='control_500DF708-222A-4E1F-ADE3-03595ED3CF8C']";
    }
     
   public static String waterMonthDropdownXpath() {
        return "//div[@id='control_249D04EA-CF17-4906-B04B-85B8918F5250']";
    }
   
   public static String waterYearDropdownXpath() {
        return "//div[@id='control_AA23CE3F-66BA-4D1C-A149-564C368B1452']";
    }
   
   public static String waterMonitorPointDropdownXpath() {
        return "//div[@id='control_C14FC30B-429E-464A-91BF-32E867450C35']";
   }
   
    public static String waterFlowProcessXpath() {
        return "//div[@id='btnProcessFlow_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }
    
    public static String waterSaveXpath() {
        return "//div[@id='btnSave_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }


    public static String TypeOfMonitoringAreaOption(String monitor) {
        return "//a[contains(text(),'" + monitor + "')]";
    }

    public static String airQualityTypeDropdownXpath() {
        return "//div[@id='control_164E9ECF-98B7-44BE-810E-9E6D002BE0EB']";
    }

    public static String monthDropdownXpath() {
        return "//div[@id='control_4E1D4CE9-349A-491A-8D18-EFD53A2076C7']";
    }

    public static String yearDropdownXpath() {
        return "//div[@id='control_228918B1-E384-4DE9-9910-2D881B26DA23']";
    }

    public static String monitorPointDropdownXpath() {
        return "//div[@id='control_E96E3D7B-B465-4EEE-B615-F90459343BE6']";
    }

    public static String airMonitoringButtonXpath() {
        return "//div[@id='btnSave_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String getAirMonitorRecord() {
        return "//div[@class='recnum']//div[@class='record']";
    }
    
   

    public static String activeAddPhaseXpath() {
        return "//div[text()='Add phase']//../..//div[@class='step active']";
    }

    public static String activeEditPhaseXpath() {
        return "//div[text()='Edit phase']//../..//div[@class='step active']";
    }

    public static String airMonitorFlowProcessButton() {
//        return "//div[@id='btnProcessFlow_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
        return "//div[@id='btnProcessFlow_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String airSaveButton() {
        return "//div[@id='btnSave_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String AirSaveWait() {
        return "//div[@id='form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']//div[@class='ui inverted dimmer active']";
    }

    public static String AirNotActiveSaveWait() {
        return "//div[@id='form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']//div[@class='ui inverted dimmer']";
    }
    
    public static String discharge_to(){
        return "//div[@id='control_D053C5AA-410A-4A27-9437-08B17E171652']/div/div/input";
    }

}
