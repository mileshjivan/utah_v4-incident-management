/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author EMashishi
 */
public class IsoMetricsScheduleAuditPageObject {

    public static String newTimeCheckBoxXpath() {
        return "//div[@parent='ED4BD529-655B-46FF-8E5A-D14632078988'][4]";
    }

    public static String proStrtDateXpath() {
        return "//div[@id='control_316CCF4D-814A-4538-BA5A-E9245C8FF01C']//input";
    }

    public static String proEndDateXpath() {
        return "//div[@id='control_112EEFA2-F98A-445B-A27F-D0C0EE00008F']//input";
    }

    public static String proCommentsXpath() {
        return "//div[@id='control_1CB03513-F3F5-41DA-A672-E27CD1BB069C']//textarea";
    }

    public static String accteptTimeCheckBoxXpath() {
        return "//div[@parent='EE14A3EB-3A9E-4EB6-A94D-E63E43D8B930'][12]";
    }

    public static String flowProcessubuttonXpath() {
        return "//div[@id='btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']//span";
    }
    
     public static String pinProcessXpath() {
        return "//span[@original-title='Pin process flow']";
    }
    
    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    
     public static String saveWait3() {
        return "//div[@class='form active transition visible']//div[@class='ui inverted dimmer']/div[text()='Saving...']";
       
    }
     
      public static String saveWait4() {
        return "//div[@class='form transition visible active']//div[@class='ui inverted dimmer']/div[text()='Saving...']";
       
    }

    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
       
    }
    
    public static String laoding() {
        return "//div[@class='ui inverted dimmer active']/div[text()='Loading data...']";
    }
    
    public static String laoding2(){
        return "//div[@class='ui inverted dimmer'][@id='divWait']/div[text()='Loading data...']";
    }
    public static String saveLoading(){
        return "//div[@class='form active transition visible']//div[@class='ui inverted dimmer'][@id='divWait']";
    }

    public static String valiadteScheduleXpath() {
        return "//div[@class='step active'][@phaseid='EFE7C665-CE89-4858-86CB-16B2E809DE4C']";
    }
    
    public static String AddButtonXpath(){
        return "//div[@id='btnAddNew']";
    }
    
    public static String fullNameDownXpath(){
        return "//div[@id='control_74F3ABB3-D328-4733-BFC4-9D151C8478D3']";
    }
    
    public static String roleXpath(){
        return "//div[@id='control_9D9BAC87-D7A1-4552-A9B0-08F2ACCC4157']//input[@language]";
    }
    
    public static String teamStrtDateXpath(){
        return "//div[@id='control_B1A6F86B-56C1-4100-81F6-F9DE01644413']//input";
    }
    
    public static String teameEndDateXpath(){
        return "//div[@id='control_0957E548-2540-4095-98A0-A8C30DB9FB72']//input";
    }
    
    public static String fullName(String name){
        return "//a[text()='"+name+"']";
    }
    
    public static String saveTeam(){
        return "//div[@class='icon checkmark icon ten six grid-icon-save']";
    }
    
    public static String auditTeambuttonXpath(){
        return "//li[@id='tab_6DB1004F-AA3F-4790-8C97-0B42EC4290D0']";
    }

}
