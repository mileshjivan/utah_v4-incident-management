/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author Ethiene
 */
public class VerificationAndAdditionalPageObject {

    public static String addDetailsXpath() {
        return "//div[text()='2.Verification and Additional Detail']";
    }
    
    public static String getAssetRecord(){
        return "//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[@id='divPager'][@class='recnum']//div[@class='record']";
    } 
    
    public static String uploadPic() {
        return "upload.PNG";
    }
    
    public static String uploadbutton(){
        return "(//b[@class='linkbox-upload'])[2]";
    }

    public static String dvt2Pic() {
        return "dvt2.PNG";
    }

    public static String openPic() {
        return "open.PNG";
    }

    public static String Safety() {
        return "//div[@id='control_D9A59CA2-B160-48B0-87A5-EACCE62378D4']";
    }

    public static String anySupervisorXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String anyActiveDropdownXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String anyActiveOptionDropdownXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]";
    }

    public static String EnvironmentXpath() {
        return "//li[@id='tab_1ACBA560-AF30-4AC3-B7B0-EC5D264E1BD7']";
    }
    
    public static String saveSupportingdocument(){
        return "//div[@id='control_F1395E4D-7A99-4032-A127-20F4D7E31DE2']";
    }
  

    public static String EnvironmentDropDownXpath() {
        return "//div[@id='control_BE858B00-B8FF-4614-A61C-3F91A5373908']";
    }

    public static String QualitytXpath() {
        return "//li[@id='tab_83609D85-9F2F-4BAF-8CB2-887CA05FC034']";
    }

    public static String QualityDropDownXpath() {
        return "//div[@id='control_AF57F968-9366-4935-91F9-9360687E8D9F']";
    }

    public static String railwaySafetyXpath() {
        return "//li[@id='tab_C765F87F-EEE5-40F5-AF0F-C52C3C964384']";
    }

    public static String railwaySafetyDropDownXpath() {
        return "//div[@id='control_07D5114B-C2C7-4526-971E-3316526C95BC']";
    }

    public static String socialSusXpath() {
        return "//li[@id='tab_718C3400-E9C8-4C9A-98D2-87C0E4D43DB6']";
    }

    public static String socialSusDropDownXpath() {
        return "//div[@id='control_01699214-099D-41EF-9C3D-7E2C83510000']";
    }

    public static String occHygieneXpath() {
        return "//li[@id='tab_22127382-55B8-49C4-BBF2-89F59794D122']";
    }

    public static String occHygieneDropDownXpath() {
        return "//div[@id='control_8D7C561A-C604-484C-91E3-A5A029C75B51']";
    }

    public static String complianceXpath() {
        return "//li[@id='tab_A41DFD6D-7804-41EA-AB3D-9ABF5EDE0BE7']";
    }

    public static String complianceDropDownXpath() {
        return "//div[@id='control_5ACB10F8-13FF-42DF-873C-D79241E16399']";
    }

    public static String personInvolvedXpath() {
        return "//span[text()='Persons Involved']";
    }

    public static String personInvolvedAddXpath() {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//div[@i18n='add_new']";
    }

    public static String personInvolvedNameXpath() {
        return "//div[@id='control_F476AA51-55D8-4537-8C78-5B2EEC764093']//input[@language]";
    }

    public static String personInvolvedDecXpath() {
        return "//div[@id='control_5C18C41F-812C-4224-AEF1-79A07DEE0676']//textarea";
    }

    public static String witnessStatementsXpath() {
        return "//span[text()='Witness Statements']";
    }

    public static String waitTableWitness() {//div[@instanceid='70D0CF9E-D264-4EAA-8A27-C6D30650438D'][@data-role]//table[@data-role]
        return "//div[@instanceid='FCE0307F-50FE-403C-BDFB-ED293325F9DA'][@data-role]//table[@data-role]";
    }
    
    public static String injuredPersonswait(){
        return "//div[@instanceid='510481BC-5774-4957-920F-C8323A843BF9'][@data-role]//table[@data-role]";
    }

    public static String waitRegulatoryTable() {
        return "//div[@instanceid='70D0CF9E-D264-4EAA-8A27-C6D30650438D'][@data-role]//table[@data-role]";
    }

    public static String waitTableEquipAndTool() {
        return "//div[@instanceid='CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F'][@data-role]//table[@data-role]";
    }

    public static String euipmentInvolvedTabXpath() {
        return "//span[text()='Equipment Involved']";
    }

    public static String euipmentAddXpath() {
        return "//div[text()='Equipment and Tools']//..//div[@i18n='add_new']";
    }

    public static String euipmentTabXpath() {

        return "//div[text()='Equipment and Tools']//..//div[@i18n='add_new']";
    }

    public static String saveVerAndAddXpath() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@i18n='save']";
    }

    public static String assetTypeDropDownXpath() {
        return "//div[@id='control_C5608592-40BB-43E3-B988-AD520AB81BF1']";
    }

    public static String saveEquipToolsXpath() {
        return "//div[@id='btnSave_form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[@i18n='save']";
    }

    public static String closeEquipToolXpath() {
        return "//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//i[@class='home icon search']/../i[@class='close icon cross']";
    }

    public static String anyClickOptionXpath(String option) {
        return "//a[text()='" + option + "']";
    }

    public static String anyClickOptionContainsXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]";
    }

    public static String anyDropDownXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']//../i[@class='jstree-icon jstree-ocl']";
    }
    
    public static String anyDropDownCheckXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']//../i[@class='jstree-icon jstree-checkbox']";
    }
    
    public static String saveIncidentManagementFullInvestigation(){
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }
    
     public static String anyContainsDropDownCheckXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]//../i[@class='jstree-icon jstree-checkbox']";
    }
    
    
    

    public static String assetDropDownXpath() {

        return "//div[@id='control_0A75D091-94E4-4A68-A9D6-6D67562F2F4C']";

    }

    public static String equipmentDescXpath() {
        return "//div[@id='control_57465454-B90F-4638-97AB-CD04E42635E1']//input[@language]";
    }

    public static String operatorEmTypeDropdownpath() {
        return "//div[@id='control_80696A19-7EFC-4DC9-87EF-87DFDBBB5B15']";
    }

    public static String operatorDropdownpath() {
        return "//div[@id='control_F17E41FB-2A2D-410F-8219-6FD01F15E039']";
    }

    public static String timeOfDayDropdownpath() {
        return "//div[@id='control_8C6FBB79-AE8F-4A16-A835-7EBE7D6CEC26']";
    }

    public static String weatherCondDropdownpath() {
        return "//div[@id='control_96006618-3C3A-4573-961C-D249943B7266']";
    }

    public static String roadConditionsCondDropdownpath() {
        return "//div[@id='control_17554023-4D73-4833-8C46-30D23FC6FCAA']";
    }

    public static String roadTypeDropdownpath() {
        return "//div[@id='control_FBF71D96-5A7F-4B41-A3BD-93A64AA12285']";
    }

    public static String InsuranceCoverDropdownpath() {
        return "//div[@id='control_662EF4A6-19C6-44FF-B645-3250C2A3A908']";
    }

    public static String motorVehiTabXpath() {
        return "//span[text()='Motor Vehicle Accident Information']";
    }

    public static String descrOfDamageXpath() {
        return "//div[@id='control_A8336A99-8A31-491D-8776-C2F5A76EA3F9']//textarea";
    }

    public static String damageCostXpath() {
        return "//div[@id='control_AE6023AB-8BA0-4AC4-BFCE-E64EE9943D5B']//input[@language]";
    }

    public static String deductibleAmountXpath() {
        return "//div[@id='control_FEA0FD6D-ECC3-4CBF-9237-CCF0BB7D1181']//input[@language]";

    }

    public static String witnessStatementsAddXpath() {
        return "//div[text()='Witness Statements']//..//div[@i18n='add_new']";
    }

    public static String witnessNameXpath() {
        return "//div[@id='control_4E6AE5EC-D5F8-40FD-88E5-7FEF114D2EC9']//input[@language]";
    }

    public static String witnessSurnameXpath() {
        return "//div[@id='control_E01BF26E-A948-4BED-9935-AAEE045C3521']//input[@language]";
    }

    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    
    
    public static String EnvironmentSaveWait() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer active']";
    }
    
     public static String EnvironmentNotActiveSaveWait() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer']";
    }
    
    
    
    public static String saveInjuredPersonXpath(){
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer']";
    }
    
     public static String saveActiveInjuredPersonXpath(){
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer active']";
    }

    public static String assestWait(String name) {
        return "//div[text()='" + name + "']";
    }

    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";

    }
    
     public static String loadingDataIncidenrSearch() {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";

    }
    
    
    public static String SaveRegulatoryWait(){
        return "//div[@id='formWrapper_70D0CF9E-D264-4EAA-8A27-C6D30650438D']/..//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String loadingPermissions() {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading permissions']";

    }
    
    public static String loadingPermission(){
        return "//div[contains(text(),'Loading permissions')]";
    }

    public static String loadingPermissionActive() {
        return "//div[@class='ui inverted dimmer active']/div[text()='Loading permissions']";

    }
    
     public static String loadingActive() {
        return "";

    }
     
     public static String loadingForms() {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading form...']";

    }
     
     public static String environmentalSpillLoadingFormActive(){
         return "//div[@class='form active transition hidden']//div[text()='Loading form...']";
     }
     
      public static String environmentalSpillLoadingFormNotActive(){
         return "//div[@class='form active transition visible']//div[text()='Loading form...']";
     }


    public static String loadingFormsActive() {
        return "//div[@class='ui inverted dimmer active']/div[text()='Loading form...']";

    }
    
    public static String loadingFormsActiveSearch() {
        return "//div[@id='divFormLoading'][@class='form active transition visible']";
    }
    
    public static String loadingFormsActiveSearchDone() {
        return "//div[@id='divFormLoading'][@class='form active transition hidden']";
    }

    public static String witnessSummaryXpath() {
        return "//div[@id='control_F53C69CD-96A0-465E-9CBE-244FAE1B4780']//textarea";
    }
    
    public static String witnessStatementFlow(){
        return "//div[@id='btnProcessFlow_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']";
    }
    
    
     public static String regularyAuthorityFlow(){
        return "//div[@id='btnProcessFlow_form_70D0CF9E-D264-4EAA-8A27-C6D30650438D']";
    }
    public static String editPhaseActive(){
        return "//div[@id='divFlow_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA'][not(contains(@style, 'display: none;'))]//div[@class='step active']";
    }
    
    public static String regularyAuthorityeditPhaseActive(){
        return "//div[@id='divProcess_70D0CF9E-D264-4EAA-8A27-C6D30650438D'][not(contains(@style, 'display: none;'))]//div[@class='step active']";
    }
    
    public static String regularyAuthoritySave(){
        return "//div[@id='btnSave_form_70D0CF9E-D264-4EAA-8A27-C6D30650438D']";
    }


    public static String witnessSaveCloseButtonXpath() {
        return "//div[@id='control_85085CF3-A567-4BB4-AA88-785C6564C3C8']//div[text()='Save and close']";
    }
    public static String witnessSaveButtonXpath() {
        return "//div[@id='control_85085CF3-A567-4BB4-AA88-785C6564C3C8']//div[text()='Save and close']";
    }

    public static String isReportableToRegulatoryAuthorityCheckboxXpath() {
        return "//input[@id='chk101']/..";
    }

    public static String safetyTab() {
        return "//div[text()='Safety']";
    }

    public static String safetyRegulatoryAuthorityHeadingXpath() {
        return "//span[text()='Safety Regulatory Authority']";
    }

    public static String regulatoryAuthorityAddButtonXpath() {
        return "//div[@id='control_8C4372CA-39BA-4187-877C-9E2F02C32DF7']//div[text()='Regulatory Authority ']/..//div[@title='Add']";
    }

    public static String regulatoryAuthority() {
        return "//div[@id='control_B58E20B0-261B-481C-982B-0CFAD75B5997']";
    }

    public static String regulatoryAuthorityInput(String authority) {
        return "//a[text()='" + authority + "'][not(contains(@class,'jstree-anchor jstree-clicked'))]";
    }

     public static String regulatoryAuthorityInput1(String authority) {
        return "//a[text()='" + authority + "']";
    }
     
    public static String regulatoryAuthorityItem() {
        return "//div[@class='select3-drop select3-drop-ddl select3_6d25d9cb transition visible']//*[@id='e2a52df9-cd89-48b3-9ad6-16ceff1c4693_anchor']";
    }

    public static String contactPersonXpath() {
        return "//div[@id='control_9E07C773-D45B-43AA-B8D8-E37F2949A6C6']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String contactNumberXpath() {
        return "//div[@id='control_01757065-A495-4AEB-BEB5-3DD4910CA67E']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String contactEmailXpath() {
        return "//div[@id='control_FB3DCA41-4C12-4A60-B97A-2762D7966770']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String dateXpath() {
        return "//div[@id='control_ECB2B00F-CA0B-42DD-8014-638125746A46']//input[@data-role='datepicker']";
    }

    public static String timeXpath() {
        return "//div[@id='control_313DF594-0537-4584-BDC6-ED41CCD32957']//input[@class='is-timeEntry']";
    }

    public static String communicationSummaryXpath() {
        return "//div[@id='control_6DF5113D-B3F1-4B24-AC01-2F1F1A8B3AA5']//textarea";
    }

    public static String saveAndCloseXpath() {
        return "//div[@id='control_9C53896F-CD4D-4B1F-9CFE-7E8FE48244FA']//div[text()='Save and close']";
    }

    public static String EquipAndTools_Save_Xpath() {
        return"//div[@id='btnSave_form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[text()='Save']";
    }
    
    public static String fr6link(){
        return "//div[@id='control_2AB69419-7475-49CE-AF0E-CE0E6119B9B7']//b[@class='linkbox-link']";
    }
    public static String linkop(){
        return "//div[@id='control_8FAF5512-5015-4A5E-8E65-14626A22BE9D']//b[@class='linkbox-link']";
    }
    public static String linkalt(){
        return "//div[@id='control_4E4BC8C7-0144-47D5-B972-5A7B29E557BB']//b[@class='linkbox-link']";
    }
    public static String LinkURL(){
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle(){
        return "//div[@class='popupcontainer']//input[@id='urlTitle']";
    }
    public static String urlTitle2(){
        return "//div[@class='roundbox-content']//input[@id='urlTitle']";
    }
    public static String urlAddButton(){
        return "//div[@class='popupcontainer']//div[contains(text(),'Add')]";
    }
    public static String urlAddButton2(){
        return "//div[@class='roundbox-content']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath(){
        return "//iframe[@id='ifrMain']";
    }

}
