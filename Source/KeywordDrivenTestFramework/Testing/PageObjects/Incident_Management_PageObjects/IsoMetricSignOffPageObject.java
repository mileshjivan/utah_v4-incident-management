/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author EMashishi
 */
public class IsoMetricSignOffPageObject {

    //new
    public static String sighOffbuttonXpath() {
        return "//li[@id='tab_9AC05ECF-DEDC-44FE-A265-F2D1600C2EC9']";
    }

    public static String sighOffAddbuttonXpath() {
        return "//div[text()='Action Sign Off']//..//div[text()='Add']";
    }
    
      public static String selectFromDropdow1n(String option) {
            return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }
    

    public static String sighOffActionDownXpath() {
        return "//div[@id='control_7EB577FB-3A6B-4CAF-B5E5-1D9E6D8040F0']";
    }

    public static String sighOffActionXpath(String name) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + name + "']";
    }

    public static String sighOffCommentsXpath() {
        return "//div[@id='control_55A6635D-1D32-4DB9-953C-3329A76E4F05']//textarea";
    }

    public static String sighOffSendFeedBackDownXpath(String name) {
        return "//div[@id='formWrapper_E243A701-48B0-4D32-9EE8-EB3FE5B9D019']//a[text()='" + name + "']//..//i[@class='jstree-icon jstree-checkbox']";
    }
     public static String actionFeedbackXpath(String name){
        return "//a[text()='"+name+"']//..//i[@class='jstree-icon jstree-checkbox']";
    }
    

    public static String sighOffSaveButton() {
        return "//div[@id='btnSave_form_E243A701-48B0-4D32-9EE8-EB3FE5B9D019']";
    }

    public static String sighOffCloseButton() {
        return "//div[@id='form_493EF828-C4E6-4204-9AEC-3ADF6E0501C0']/div[1]/i[@class='close icon cross']";
    }

   //Sign off two 
    public static String sighoffTab() {
        return "//li[@id='tab_006AFF62-D481-4DC3-8668-042C24DAA78C']";
    }

    public static String sighoffSendCheckBox() {
        return "//div[@parent='006AFF62-D481-4DC3-8668-042C24DAA78C'][2]";
    }

    public static String auditRatingDownXpath() {
        return "//div[@id='control_5213D864-F82C-47D4-95B2-61AAEE184219']";
    }
    public static String auditRatingXpath(String name) {
        return "//a[text()='" + name + "']";
    }

    public static String conclusionXpath() {
        return "//div[@id='control_99523AA3-5DB6-4863-9A06-6D53E3328EDE']//textarea";
    }

    public static String saveRecordSignOff() {
        return "//div[@id='btnSave_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }

    //Audit Sign Off 
    public static String addAuditSignOff() {
        return "//div[text()='Audit Sign Off']/..//div[@id='btnAddNew']";
    }

    public static String AuditSignOffAuditorTypeDownXpath() {
        return "//div[@id='control_F8F35C1B-6B3C-4885-9834-C64C5DDA4555']";
    }

    public static String AuditSignOffAuditorTypeXpath(String name) {
        return "//a[text()='" + name + "']";
    }

    public static String AuditSignOffAuditorDownXpath() {
        return "//div[@id='control_DEA38CFE-258E-4507-BE92-180D8536EF89']";
    }
    
     public static String AuditSignOffOffDownXpath() {
        return "//div[@id='control_AC075E37-7F8F-4DEC-8DF0-02DC8D70BDFB']";
    }
     
       public static String AuditSignOffCommentsXpath() {
        return "//div[@id='control_5FC74CF8-0BE4-459D-9599-A35927F43D28']//textarea";
    }
       
       public static String saveAuditSignOff(){
           return "//div[@id='btnSave_form_A52889B0-7739-4666-BA87-CE3F77BAB495']";
       }
       
       public static String closeAuditSignOff(){
           return "//div[@id='form_A52889B0-7739-4666-BA87-CE3F77BAB495']/div[1]/i[@class='close icon cross']";
       }//li[@id='tab_AC024900-81BE-4806-8799-EE93B7191475']
       
        public static String auditProtocolTabXpath(){
           return "//li[@id='tab_AC024900-81BE-4806-8799-EE93B7191475']";
       }
        
        
        public static String auditProCompleteXpath(){
            return "//div[@parent='AC024900-81BE-4806-8799-EE93B7191475'][11]//li";
        }
        
        public static String processFlowXpath(){
            return "//div[@class='process-form form properties form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07 pinned active']//div[@class='step active']";
        }
        
        public static String auditRecordXpath(){
            return "//div[@class='form transition visible active' ]/div[1]//div[@class='record']";
        }
    
    
    //close button
    public static String auditFindingsXpath(){
        return "//div[@id='form_CEB65E1E-0FE0-410C-9786-1A7C036B69D0']/div[1]//i[@class='close icon cross']";
    }
    
    

}
