/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Testing.TestMarshall;

/**
 *
 * @author MJivan
 */
public class IsometricsPOCPageObjects {

    public static String IsometrixURL() {
        // Use ENUM
     return TestMarshall.currentEnvironment.isoMetrixEnvironment;
//        return "https://ch11.isometrix.net/IsoMetrix.Solutions.Master.V4.Scoping/default.aspx";
    }
    
    public static String IsometrixURLDatabase() {
        // Use ENUM
     return TestMarshall.databaseEnvironment.isoMetrixEnvironment;
//        return "https://ch11.isometrix.net/IsoMetrix.Solutions.Master.V4.Scoping/default.aspx";
    }
    
    public static String URL(String text)
    {
        return text;
    }
    
    public static String userProjectXpath(){
        return "//span[@class='user']";
    }

    public static String Username() {
        return "//input[@id='txtUsername']";
    }

    public static String Password() {
        return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() {
        return "//div[@id='btnLoginSubmit']";
    }

    public static String homepage_SolutionBranch_Btn() {
        return "//label[text()='Solution Branch']";
    }
    
    public static String homepage_SolutionsBranch_Btn() {
        return "//label[text()='Solutions Branch']";
    }
    
    public static String iframeXpath(){
        return "//iframe[@id='ifrMain']";
    }
    public static String search(){
       return "//div[text()='Search']";
   }
   
   public static String homepage_SolutionBranchCode_Btn() {
//        return"//div[@id='events_321F20B4-3A9E-4120-8C5F-3967AD338B4A_form']//..//..//td[4]//div";
       return "(//div[text()='Search'])[1]";
   }

   public static String backtohomepage_Btn() {
       return"//div[@class='active form transition visible']//i[@class='back icon arrow-left']";
   }

   public static String sb_text(){
       return "//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//div[@class='align-left']";
   }
   
   
   public static String isoSideMenu(){
            return "//i[@class='large sidebar link icon menu']";
    }
    public static String version(){
        return "//footer";
    }

}
