/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author Administrator
 */
public class MainScenario_PageObjects {

    public static String SupportingDocumentsTab() {
        return "//li[@id='tab_98124058-DEC1-41A5-82BF-7B07BA53107C']//div[text()='Supporting Documents']";
    }

    public static String VerificationAndAdditionalTab() {
        return "//li[@id='tab_90D1C7C9-45EE-402F-B76C-A61222A110E1']//div[text()='2.Verification and Additional Detail']";
    }

    public static String activeUnderInvesgationXpath() {
        return "//div[text()='3.Under Investigation']//../..//div[@class='step active']";
    }

    public static String SaveButtonXPath() {
        return "//div[text()='Save supporting documents']";
    }

    public static String PersonsInvolvedXpath() {
        return "//div[@class='nreqbox control pnl transition visible']//span[text()='Persons Involved']";
    }

    public static String WitnessStatementsXpath() {
        return "//div[@class='nreqbox control pnl transition visible']//span[text()='Witness Statements']";
    }

    public static String EquipmentInvolvedTabXpath() {
        return "//div[@class='nreqbox control pnl transition visible']//span[text()='Equipment Involved']";
    }

    public static String ValidateGridIsEditableXPath() {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//div[@class='grid k-grid k-widget k-reorderable editable']";
    }

    public static String ValidateGridIsNotEditableXPath() {
        return "//div[@id='control_5E4C6D37-0D4E-42D1-AC3D-B1E210B05DD9']//div[@class='grid k-grid k-widget k-reorderable']";
    }

    public static String EuipmentValidateGridIsNotEditableXPath() {
        return "//div[@id='control_45349B8B-5312-4623-BF54-BE4D75D4FB8F']//div[@class='grid k-grid k-widget k-reorderable']";
    }

    public static String ANewRowIsDisplayedXPath() {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//tbody//tr[@role='row']";
    }

    public static String SaveXPath() {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//div[@title='Save']";
    }

    public static String witnessSaveButtonXpath() {
        return "//div[@id='form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']//div[text()='Save']";
    }

    public static String CloseWitnessStatementsXPath() {
        return "//div[@id='form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']//i[@class='home icon search']/../i[@class='close icon cross']";
    }

    public static String WaitForItemXPath(String name) {
        return "//div[@id='control_D0351C3F-0C62-47C4-83CE-5DDB30BBD5DF']//div[text()='" + name + "']";
    }

    public static String RestrictionsTextAreaXPath() {
        return "//div[@id='control_53B19CFD-7FE1-40A3-B77D-D01693699C63']//textarea";
    }

    public static String RestrictionsTextAreaHiddenXPath() {
        return "//div[@id='control_53B19CFD-7FE1-40A3-B77D-D01693699C63'][@class='nreqbox control pnl transition hidden']//textarea";
    }

    public static String DrugAndAlcoholLoadingXPath() {
        return "//div[@class='ui active inverted dimmer transition visible']//div[id='txtWait']";
    }

    public static String DrugAndAlcoholNoResultsXPath() {
        return "//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']//div[text()='No results returned']";
    }

    public static String DrugAndAlcoholTableXPath() {
        return "//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']";
    }

    public static String ConductDrugAndAlcoholTestButtonXPath() {
        return "//div[@id='control_882404B6-DD63-4169-AAAB-1A71C3CCE79E']//div[text()='Conduct Drug and Alcohol test  ']";
    }

    public static String SaveAndCloseXPath() {
        return "//div[@id='control_C17F80E0-588B-4A71-814B-A2FEEE9D4B3B']//div[text()='Save and close']";
    }

    public static String LoadingDataActiveXPath() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer']//div[@class='ui text loader'][text()='Loading data...']";
    }

    public static String WaitForTableToLoadXPath() {
        return "//div[@class='ui inverted dimmer hidden']//div[@title='Loading...']";
    }

    public static String LoadingFormXPath() {//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer']//div[text()='Loading data...']
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer']//div[text()='Loading data...']";
    }//div[@class='ui active inverted dimmer']//div[text()='Loading form...']

    public static String MedicalTestDateXPath() {
        return "//div[@id='control_5735F7DF-7695-4096-9ADB-9C21AA9D3FD7']//input";
    }

    public static String AppointmentDateXPath() {
        return "//div[@id='control_4E1D0588-3512-41B8-BAF8-DC8517754FEF']//input";
    }

    public static String PersonSubmitToATestXPath() {
        return "//div[@id='control_711BFF97-3ECC-4FEC-8F3B-F6A3C080684C']//li";
    }

    public static String DrugTestResultXPath() {
        return "//div[@id='control_E47B8DA0-0088-4009-9B6F-4EE8AA8F96C1']//li";
    }

    public static String AlcoholTestResultXPath() {
        return "//div[@id='control_C50181BC-31EF-44B6-AD76-D2BE7DFF4844']//li";
    }

    public static String SaveDrugAndAlcoholTestXPath() {
        return "//div[@id='form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']//div[text()='Save']";
    }

    public static String RefreshAlcoholAndDrugsXPath() {
        return "//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']//a[@title='Refresh']";
    }

    public static String PersonBeingTestedTableXPath(String record) {
        return "//span[@title='" + record + "']";
    }

    public static String concessions_QualityTab_xpath() {
        return "//li[@id='tab_83609D85-9F2F-4BAF-8CB2-887CA05FC034']//div[contains(text(),'Quality')]";
    }

    public static String concessions_QualityConcession_Expandxpath() {
        return "//span[contains(text(),'Quality Concessions ')]";
    }

    public static String concessions_Add_Buttonxpath() {
        return "//div[contains(text(),'Concession')]//..//div[@title='Add']";
    }
    
    public static String concessions_RailwaySafetySupervisor_DropdownXpath(){
        return "//div[contains(text(),'Railway safety supervisor')]/../../div[2]//li";
    }

    public static String concessions_Description_TextAreaxpath() {
        return "//div[@id='control_04842A6A-3005-4D07-9131-6B25C1373134']//textarea";
//        return "//div[@class='select3-drop select3-drop-ddl select3_e6f1606e transition visible']//input[@placeholder='Type to search']";    
    }

    public static String concessions_Impact_TextAreaxpath() {
        return "//div[@id='control_9ECE066A-3D4C-4209-B2A3-54ED4D4DF4E8']//textarea";
    }

    public static String concessions_CorrectiveAction_TextAreaxpath() {
        return "//div[@id='control_83CBB14B-6E16-490E-B408-EF19607B33AD']//textarea";
    }

    public static String concessions_AllPartiesAffected_CheckBoxxpath() {
        return "//div[@id='control_667887E0-CE52-4221-BB30-216FE9041A02']//b[@class='select3-all']";
    }

    public static String concessions_ResponsiblePerson_SelectFieldxpath() {
        return "//div[@id='control_97EE57FD-6D77-47A7-94E4-7726E6C129C5']//li[contains(text(),'Please select')]";
    }

    public static String concessions_ResponsiblePerson_TypeSearchxpath() {
//        return "//ul[@class='select3-results jstree jstree-29 jstree-default jstree-loading']//..//input[@type='text']";
        return"//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//input[@type='text']";
    }

    public static String concessions_ResponsiblePerson_xpath(String person) {
//        return "//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//a[contains(text(),'" + person + "')]";
        return "//div[@id='divForms']/div[33]//a[contains(text(),'"+person+"')]";
    }

    public static String flowProcessXpath() {
        return "//div[@id='btnProcessFlow_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']";
    }
    
     public static String responsiblePerson(String option) {
        return "//div[contains(@class, 'transition visible')][contains(@class, 'select3-drop select3-drop-ddl')]//a[text()='" + option + "']";
    }

    public static String concessions_StartDate_InputAreaxpath() {
        return "//div[@id='control_F7F730D0-04B1-4408-B1C2-8B7977A9EEA5']//input";
    }

    public static String concessions_EndDate_InputAreaxpath() {
        return "//div[@id='control_271024EF-4AA9-406F-A3E0-9A688EDCCA44']//input";
    }

    public static String concessions_Priority_SelectFieldxpath() {
        return "//div[@id='control_5C97E0A8-1CEE-4C48-A5A8-241D19A335BF']//li";
    }
       
    public static String concessions_Priority_TypeSearchxpath() {
//        return "//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//input";
        return"//ul[@class='select3-results jstree jstree-31 jstree-default jstree-loading']//..//input";
    }

    public static String concessions_Priority_xpath(String priority) {
//        return "//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//a[contains(text(),'" + priority + "')]";
//        return"//ul[@class='select3-results jstree jstree-31 jstree-default jstree-loading']//..//a[contains(text(),'"+ priority +"')]";
        return "//div[@id='divForms']/div[34]//a[text()='"+priority+"']";
    }

    public static String concessions_SaveToContinue_Buttonxpath() {
        return "//div[@id='control_99941FE9-6B06-4946-A5EA-446872CDDDE6']//div[contains(text(),'Save to continue')]";
    }

    public static String RecordNumberXPath() {
        return "//div[contains(text(),'Record #')]";
    }

    public static String concessionsAction_Add_Buttonxpath() {
        return "//div[contains(text(),'Concession Action')]//..//div[@title='Add']";
    }

    public static String concessionActionActive() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E'][@class='form active transition visible']";

    }

    public static String Concession_Status_XPath() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//..//li[@title='Logged']";
    }

    public static String concessionsAction_Description_Textfieldxpath() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String concessionsAction_DepartmentResponsible_Buttonxpath() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
//        return "//div[contains(text(),'Action Detail')]/../../../../../..//div[@class='tabpanel']/div[2]/div[1]/div[1]/div[4]//li";
    }

    public static String concessionsAction_DepartmentResponsible_xpath(String department) {
//        return"//ul[@class='select3-results jstree jstree-33 jstree-default jstree-loading']//a[contains(text(),'"+ department +"')]";
//        return "//ul[@class='select3-results jstree jstree-32 jstree-default jstree-loading']//a[contains(text(),'" + department + "')]";
        return "//div[@id='divForms']/div[38]//a[text()='"+department+"']";

    }

    public static String concessionsAction_ResponsiblePerson_SelectFieldxpath() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li[contains(text(),'Please select')]";
    }

    public static String concessionsAction_ResponsiblePerson_TypeSearchxpath() {
        return"//ul[@class='select3-results jstree jstree-34 jstree-default jstree-loading']//..//input";
//        return "//ul[@class='select3-results jstree jstree-33 jstree-default jstree-loading']//..//input";
    }

    public static String concessionsAction_ResponsiblePerson_xpath(String person) {
//        return"//ul[@class='select3-results jstree jstree-34 jstree-default jstree-loading']//..//a[contains(text(),'"+ person +"')]";
//        return "//ul[@class='select3-results jstree jstree-33 jstree-default jstree-loading']//..//a[contains(text(),'" + person + "')]";
        return "//div[@id='divForms']/div[39]//a[contains(text(),'"+person+"')]";
    }

    public static String concessionsAction_ActionDueDate_Textfieldxpath() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String concessionsAction_Save_DropDownxpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@class='more options icon chevron down']";
    }

    public static String concessions_Save_DropDownxpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@class='more options icon chevron down']";
    }

    public static String concessionsAction_Save_DropDownMorexpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@class='options icon chevron down more']";
    }

    public static String concessionsAction_SaveAndClose_Buttonxpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@title='Save and close']";
    }

    public static String toBeInitiatedXpath() {
        return "//div[text()='To be initiated']//../..//div[@class='step active']";
    }

    public static String concessionActionsCloseButton() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String getConcessionActionRecordID() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@class='recnum']//div[@class='record']";
    }

    public static String inProgress() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//li[@title='In Progress']";
    }

    public static String concessions_SaveAndClose_On_More_Buttonxpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@title='Save and close']";
    }

    public static String concessionsAction_Save_Buttonxpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//..//div[@title='Save']";
    }

    public static String retrievedConcessionActionRecordNumberXPath() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[contains(text(),'- Record #')]";
    }

    public static String retrieveConcessionRecordNumberXPath() {
        return "//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[contains(text(),'- Record #')]";
    }

    public static String concessionsAction_OpenSsavedConcessionRecord_SelectField(String concessionActionRecordNumber) {
        return "//span[contains(text(),'" + concessionActionRecordNumber + "')]";
    }

    public static String concessionsAction_RefreshRecords_Buttonxpath() {
        return "//div[contains(text(),'Concession Actions')]//..//..//a[@title='Refresh']";
    }

    public static String concessions_Save_DropDownMorexpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@class='more options icon chevron down']";
    }

    public static String concessions_SaveAndClose_Buttonxpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@title='Save and close']";
    }

    public static String incidentManagement_VerifAndAddiDetails_Tabxpath() {
        return "//div[@class='tabpanel_move_content tabpanel_move_content_scroll']//div[contains(text(),'2.Verification and Additional Detail')]";
    }

    public static String concessions_OpenRecord_Selectxpath(String recordNumber2) {
        return "//span[contains(text(),'" + recordNumber2 + "')]";
    }

    //Risk And Impact Assessment xpaths
    public static String RiskAndImpactAssessment_Tab_ButtonXpath() {
        return "//div[contains(text(),'Risk and Impact Assessment')]";
    }

    public static String flowProcess_ButtonXpath() {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath() {
        return "//div[@id='control_8AC963E8-5EB7-492E-8371-DFAB7FE3B5AD']//div[@class='c-chk']";
        //return"//div[@id='control_8AC963E8-5EB7-492E-8371-DFAB7FE3B5AD']//div[@class='icheckbox icheck-item icheck[o7d9e]']";
    }

    public static String RiskAndImpactAssessment_DeclarationComment_InputXpath() {
        return "//div[@id='control_280FC061-EC2C-4A95-916F-6D47A94C4758']//textarea";
    }

    public static String RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath() {
        return "//div[@id='control_97D0FB6B-ABA3-47F8-A4C6-8E0DB2C5E71A']//li";
    }

    public static String RiskAndImpactAssessment_LeadInvestigator_TypeSearchxpath() {
        return "//ul[@class='select3-results jstree jstree-29 jstree-default jstree-loading']//..//input";
    }

    public static String anyActiveOptionDropdownXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]";
    }

    public static String RiskAndImpactAssessment_LeadInvestigator_xpath(String lead) {
        return "(//a[contains(text(),'"+lead+"')])[9]";
    }

    public static String RiskAndImpactAssessment_SaveAndContinueToStep3_ButtonXpath() {
        return "//div[@id='control_9E3EFB23-6979-4773-8B4C-E8B0D6F49952']//div[contains(text(),'Save and continue to Step 3')]";
    }

    public static String investigationTab() {
        return "//div[text()='3.Incident Investigation']";
    }

    public static String RiskAndImpactAssessment_Save_ButtonXpath() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='more options icon chevron down']";
        //return"//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@title='Save']";
    }

    public static String RiskAndImpactAssessment_SaveAndClose_ButtonXpath() {
        return "//div[@id='btnSaveClose_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@title='Save and close']";
    }

    public static String incidentManagement_InvestigationStatus_TitleXpath() {
        return "//div[@id='control_861FFF0E-9C05-40A9-8DB5-86FD61929844']";
    }

    public static String incidentManagement_InvestigationStatus_RetrieveXpath() {
        return "//div[@id='control_61A17BC2-21A9-463E-ADB9-536DCCAFCF4B']";
    }

    public static String RiskAndImpactAssessment_SubmitStep2_ButtonXpath() {
        return "//div[contains(text(),'Submit Step 2')]";
    }

    public static String RiskAndImpactAssessment_Refresh_ButtonXpath() {
        return "//a[@title='Refresh']";
    }

    //Capture Investigation team
    public static String investigationTeam_DueDate_textFieldXpath() {
        return "//div[@id='control_7E7E7BF6-08EF-4008-BEDA-5B7E422D739F']//input";
    }

    public static String investigationTeam_Scope_textFieldXpath() {
        return "//div[@id='control_669476AC-06EA-4FAE-9872-D47FB2469048']//textarea";
    }

    public static String investigationTeam_InvestigationTeam_DropDownXpath() {
        return "//span[contains(text(),'Investigation Team')]";
    }

    public static String investigationTeam_InvestigationTeam_ADD_ButtonXpath() {
        return "//div[@id='control_3006D33A-81F0-4C4F-A7F7-3289CCEEA80E']//div[@title='Add']";
    }

    public static String investigationTeam_Fullname_SelectFieldXpath() {
        return "//div[@id='control_74F3ABB3-D328-4733-BFC4-9D151C8478D3']//li";
    }

    public static String investigationTeam_Fullname_Xpath(String person) {
        return "//a[contains(text(),'" + person + "')]";
    }

    public static String investigationTeam_Role_TextFieldXpath() {
        return "//div[@id='control_9D9BAC87-D7A1-4552-A9B0-08F2ACCC4157']//input";
    }

    public static String investigationTeam_StartDate_TextFieldXpath() {
        return "//div[@id='control_B1A6F86B-56C1-4100-81F6-F9DE01644413']//input";
    }

    public static String investigationTeam_EndDate_TextFieldXpath() {
        return "//div[@id='control_0957E548-2540-4095-98A0-A8C30DB9FB72']//input";
    }

    public static String investigationTeam_Hours_TextFieldXpath() {
        return "//div[@id='control_CEE2D645-02D7-4931-9474-A2D4706B148B']//input";
    }

    public static String investigationTeam_InvestigationTeam_TitleXpath() {
        return "//div[contains(text(),'Investigation Team')]";
    }

    public static String investigationTeam_Save_textFieldXpath() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//..//div[@title='Save']";
    }

    public static String investigationTeam_Save_ButtonXpath() {
        return "//div[@id='control_3006D33A-81F0-4C4F-A7F7-3289CCEEA80E']//div[@title='Save']";
    }

    public static String incidentInvestigation_InvestigationDetail_Tabxpath() {
        return "//div[contains(text(),'Investigation Detail')]";
    }

    public static String IncidentMangementStatusAfterSavingXPath() {
        return "//div[@id='control_B1667955-9616-471C-BBEC-14F334A90E65']//li";
    }

    public static String incidentInvestigation_ProcessOrActivity_Layoutxpath() {
        return "//div[@id='control_CC6900A0-1399-4340-AB58-7D532D79BEC8']//li";
    }

    public static String incidentInvestigation_ProcessOrActivity_TickAllxpath() {
        return "//div[@id='control_CC6900A0-1399-4340-AB58-7D532D79BEC8']//b[@original-title='Select all']";
    }

    public static String incidentInvestigation_RiskSource_Layoutxpath() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//li";
    }

    public static String incidentInvestigation_RiskSource_TickAllxpath() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//b[@original-title='Select all']";
    }

    public static String incidentInvestigation_Risk_Layoutxpath() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//li";
    }

    public static String incidentInvestigation_Risk_TickAllxpath() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//b[@original-title='Select all']";
    }

    public static String incidentInvestigation_InvestigationType_Layoutxpath() {
        return "//div[@id='control_9033FCFD-1EC6-4C92-AFF0-0AAF4B37E191']//li";
    }

    public static String incidentInvestigation_InvestigationType_TickAllxpath(String type) {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'" + type + "')]";
    }

    public static String incidentInvestigation_Refresh_Buttonxpath() {
        return "//div[@id='control_E8AC341B-3D7B-4A87-8C02-91EDF4A9EB34']//div[contains(text(),'Refresh')]";
    }

    public static String incidentInvestigation_RelatedRisk_Panelxpath() {
        return "//span[contains(text(),'Related Risks')]";
    }

    public static String incidentInvestigation_RelatedRisk_Openxpath() {
        return "//div[contains(text(),'Mechanical -> Mobile equipment (Vehicles, forklifts etc.)')]";
    }

    public static String incidentInvestigation_RelatedRisk_OpenedRecordxpath() {
        return "//div[@id='form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//div[contains(text(),'- Record #')]";
    }

    public static String incidentInvestigation_RelatedJSAs_Panelxpath() {
        return "//span[contains(text(),'Related JSA')]";
    }

    public static String incidentInvestigation_BowtieRiskAssement_Panelxpath() {
        return "//span[contains(text(),'Related Bowties')]";
    }

    public static String incidentInvestigation_LearningAndApproval_tabxpath() {
        return "//li[@id='tab_4E355F73-BD7A-4459-A420-D5A01E939D21']//div[contains(text(),'Learnings & Approval ')]";
    }

    public static String incidentInvestigation_ManagementFindings_Add_Buttonxpath() {
        return "//div[@id='control_781A7295-A3B9-49BC-9CBA-5C18491C8649']//div[@title='Add']";
    }

    public static String incidentInvestigation_ManagementFindings_Description_TextFieldxpath() {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String incidentInvestigation_ManagementFindings_FindingOwner_Selectxpath() {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String incidentInvestigation_ManagementFindings_FindingOwner_xpath(String owner) {
        return "(//a[contains(text(),'"+owner+"')])[10]";
    }

    public static String incidentInvestigation_ManagementFindings_RiskSource_Selectxpath() {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//li";
    }

    public static String incidentInvestigation_ManagementFindings_RiskSource_TickAllxpath() {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-all']";
    }

    public static String incidentInvestigation_ManagementFindings_Classification_Selectxpath() {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//li";
    }

    public static String incidentInvestigation_ManagementFindings_Classification_xpath(String classification) {
        return "//a[contains(text(),'" + classification + "')]";
    }

    public static String incidentInvestigation_ManagementFindings_Risk_Selectxpath() {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']//li";
    }

    public static String incidentInvestigation_ManagementFindings_Risk_xpath(String risk) {
        return "(//a[contains(text(),'"+risk+"')])[2]";
    }

    public static String incidentInvestigation_ManagementFindings_Save_Selectxpath() {
        return "//div[@id='btnSave_form_0B2C6ED4-CC58-477E-931E-7949371422D0']//div[@title='Save']";
    }

    public static String incidentInvestigation_ManagementFindings_RecordNumber_Selectxpath() {
        return "//div[@id='form_0B2C6ED4-CC58-477E-931E-7949371422D0']//div[contains(text(),'- Record #')]";
    }

    public static String incidentInvestigation_ManagementFindings_Close_Selectxpath() {
        return "//div[@id='form_0B2C6ED4-CC58-477E-931E-7949371422D0']//i[2]";
    }

    public static String incidentInvestigation_ManagementFindings_ListedRecord_Listxpath(String retrieveRecordNumber) {
        return "//div[@id='grid']//span[@title='" + retrieveRecordNumber + "']";
    }

    public static String incidentInvestigation_RelatedJSAs_Refresh_Buttonxpath() {
        return "//div[@id='control_3CD0FC6D-FBE1-474A-9921-494B05186C6B']//span[@class='k-icon k-i-reload']";
    }

    public static String incidentInvestigation_BowtieRiskAssement_Refresh_Buttonxpath() {
        return "//div[@id='control_44411406-1DA6-493A-9A86-695A82FE16F9']//span[@class='k-icon k-i-reload']";
    }

    public static String incidentInvestigation_BowtieRiskAssement_Message_Buttonxpath() {
        return "//div[@id='control_44411406-1DA6-493A-9A86-695A82FE16F9']//span[@class='k-pager-info k-label']";
    }

    public static String incidentInvestigation_RelatedJSAs_Message_Buttonxpath() {
        return "//div[@id='control_3CD0FC6D-FBE1-474A-9921-494B05186C6B']//span[@class='k-pager-info k-label']";
    }

    public static String concessions_StatusDropDown_Buttonxpath() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//span[@class='select3-arrow']";
    }

    public static String concessions_Status_InProgress_Buttonxpath() {
        return "//ul[@class='select3-results jstree jstree-34 jstree-default jstree-loading']//a[contains(text(),'In Progress')]";
        //return"//ul[@class='select3-results jstree jstree-32 jstree-default jstree-loading']//a[contains(text(),'In Progress')]";
    }

    public static String concessions_AffectedParties_CheckBoxXpath() {
        return "//div[@id='control_569C771D-5F69-4957-BF21-E480351D417E']//div[@class='c-chk']";
    }

    public static String concessions_SignOff_CheckBoxXpath() {
        return "//div[@id='control_9A96ECD0-D256-4A86-8F4B-A903EAB67FA2']//div[@class='c-chk']";
        //return"//div[@id='control_57BBCC92-234A-429D-810D-7A33F29DD0CF']//div[@class='c-lbl']";
    }

    public static String incidentActions_SaveMoreOptions_Buttonxpath() {
        return "//div[@id='btnSave_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[@class='more options icon chevron down']";
    }

    public static String incidentActions_SaveAndClose_Buttonxpath() {
        return "//div[@id='btnSave_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[@title='Save and close']";
    }

    public static String incidentInvestigation_TabXpath() {
        return "//li[@id='tab_3A331B31-BF4C-4C42-ADB3-936999FEE515']//div[contains(text(),'3.Incident Investigation')]";
    }

    public static String LearningAndApproval_Findings_Panelxpath() {
        return "//span[contains(text(),'Investigation Findings')]";
    }

    public static String incidentInvestigation_learnings_TextFieldxpath() {
        return "//div[@id='control_0711AFDC-7A24-45F4-9A7D-4B27F4104D49']//textarea";
    }

    public static String incidentInvestigation_Conclusion_TextFieldxpath() {
        return "//div[@id='control_46CC5397-E89F-406E-ACC0-6AC270D0E371']//textarea";
    }

    public static String LearningAndApproval_declaration_CheckBoxXpath() {
        return "//div[@id='control_1F2CA953-B448-4A17-B1D0-383EB46D1349']//div[@class='c-chk']";
    }

    public static String incidentInvestigation_Comments_TextFieldxpath() {
        return "//div[@id='control_8D1AE953-65A5-4E95-A6D6-3237EA4A48B1']//textarea";
    }

    public static String incidentInvestigation_SaveAndContinue_Buttonxpath() {
        return "//div[@id='control_2CF4FD6E-B9AA-44D4-801C-2940E5495E06']//div[contains(text(),'Save and continue to Step 4')]";
    }

    public static String incidentInvestigation_Submit_Buttonxpath() {
        return "//div[@id='control_851B10A6-EC82-47C0-8F18-8C8CB97D7963']//div[contains(text(),'Submit Step 3')]";
    }

    public static String IncidentManagementId() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='recnum']//div[@class='record']";
    }

    public static String incidentmanagement_Refresh_Buttonxpath() {
        return "//span[@class='k-icon k-i-reload']";
    }

    public static String incident_xpath(String incidentRec) {
        return "//span[@title='" + incidentRec + "']";
    }

    public static String EquipAndTools_Assets_PanelArrowXPath() {
        return "//span[contains(text(),'Assets')]";
    }

    public static String EquipAndTools_AssetsAdd_ButtonXPath() {
        return "//div[@id='control_BC26F4F4-386E-42B1-A767-33E77CA09F6E']//div[@title='Add']";
    }

    public static String assets_Process_SelectXPath() {
        return "//div[@id='control_28914C65-06E3-4C73-91DB-D0461DB0C410']//li";
    }

    public static String process_Production_ExpandXPath() {
        return "//a[contains(text(),'Financial Management')]//..//i[@class='jstree-icon jstree-ocl']";
    }
    public static String process_Production_ExpandXPath2() {
        return "//a[contains(text(),'R-BusinessProcessMgnt/22')]";
    }

    public static String process_ProductionXPath() {
        return "//div//a[contains(text(),'Financial controls')]";
    }

    public static String assets_RTORequired_SelectXPath() {
        return "//div[@id='control_C29B7061-6896-4086-8BDE-1AEEE2F7ED4F']//li";
    }

    public static String assets_RTORequired_XPath() {
        return "//div//a[contains(text(),'01:00')]";
    }

    public static String assets_Quantity_XPath() {
        return "//div[@id='control_13675B86-8CC6-46D6-B54D-52838975DA99']//input[@type='number']";
    }

    public static String assets_Mandatory_SelectXPath() {
        return "//div[@id='control_985E4D2A-EE5B-4A61-A173-E1F1027864C8']//li";
    }

    public static String process_Mandatory_XPath() {
        return "//ul[@class='select3-results jstree jstree-33 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }

    public static String assets_Backup_SelectXPath() {
        return "//div[@id='control_E452C970-B034-44C2-B4EF-B20849E8B03F']//li";
    }

    public static String process_Backup_XPath() {
        return "//ul[@class='select3-results jstree jstree-34 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }

    public static String assets_Comments_XPath() {
        return "//div[@id='control_022B7F82-688D-4D84-9F2D-C21B02B429BD']//textarea";
    }

    public static String assets_Save_XPath() {
        return "//div[@id='btnSave_form_2000EE74-35E5-4218-90FC-47E8A0660483']//div[@title='Save']";
    }

    public static String assetId() {
        return "//div[@id='form_2000EE74-35E5-4218-90FC-47E8A0660483']//div[contains(text(),'- Record #')]";
    }

    public static String incidentActions_Close_Buttonxpath() {
        return "//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String incidentManagement_Refresh_Buttonxpath() {
        return "//a[@title='Refresh']";
    }

    public static String incidentManagement_xpath(String rec) {
        return "//span[@title='" + rec + "']";
    }

    public static String IncidentSignOff_Tabxpath() {
        return "//div[contains(text(),'4.Incident Sign Off')]";
    }

    public static String IncidentSignOff_ActionSummary_Panelxpath() {
        return "//div[@id='control_13FDC30A-C7B7-46B8-9C9B-B32DFFE1E2D4']//i[@class='toggle']";
    }

    public static String IncidentSignOff_ActionSummaryRecord_xpath() {
        return "//div[@id='control_78D0FDEF-5BB7-47F5-855E-269AC2CAACE4']//td[6]";
    }

    public static String IncidentSignOff_ActionSummaryRecordNumber_xpath() {
        return "//div[@id='form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[contains(text(),'- Record #')]";
    }

    public static String equipAndToolsId() {
        return "//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[@class='recnum']//div[@class='record']";
    }

    public static String assets_ProcessFlow_xpath() {
        return "//div[@id='btnProcessFlow_form_2000EE74-35E5-4218-90FC-47E8A0660483']//span";
    }

    public static String LearningAndApproval_Process_ButtonXpath() {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']//span";
    }

    public static String IncidentSignOff_Process_buttonxpath() {
        return "//div[@id='btnProcessFlow_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//span";
    }

    public static String concessions_Process_Buttonxpath() {
        return "//div[@id='btnProcessFlow_form_B3205F35-ED12-44E1-BD2F-995318C72096']//span";
    }

    public static String actionFeedbackTab() {
        return "//div[text()='Action Feedback'][@class='title']";
    }

    public static String concessionActionFeedbackAdd() {
        return "//div[text()='Action Feedback'][@class='link']/..//div[@id='btnAddNew']";
    }

    public static String actionFeedbackActiveForm() {
        return "//div[@id='form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83'][@class='form transition visible active']";
    }

    public static String actionFeedbackFlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']";
    }
    
    public static String pinProcessFlowConcessionActions(){
        return "//div[@id='divFlow_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//span[@original-title='Pin process flow']";
        
    }

    public static String actionFeedbackTextArea() {
        return "//div[@id='control_B9A4BEF3-B4D1-491D-881A-6021DDE86169']//textarea";
    }

    public static String actionFeedbackActionCompleteDropdown() {
        return "//div[@id='control_40EFC614-1BC1-4F14-934E-928BF0980FF2']";
    }

    public static String actionFeedbackSave() {
        return "//div[@id='btnSave_form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']";
    }

    public static String concessionActionFeedbackCloseButton() {
        return "//div[@id='form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String getConcessionActionFeedbackRecordID() {
        return "//div[@id='form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']//div[@class='recnum']//div[@class='record']";
    }

    public static String anySupervisorXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String activeAddPhaseXpath() {
        return "//div[text()='Add phase']//../..//div[@class='step active']";
    }

    public static String activeEditPhaseXpath() {
        return "//div[text()='Edit Phase']/..//..//div[@class='step active']";
    }
    
    public static String activeClosedPhaseXpath() {
        return "//div[text()='Closed']/..//..//div[@class='step active']";
    }


    public static String signOffTab() {
        return "//div[text()='Sign Off'][@class='title']";
    }

    public static String concessionSighOffAdd() {
        return "//div[text()='Action Sign Off'][@class='link']/..//div[@id='btnAddNew']";
    }

    public static String actionSignOffActiveForm() {
        return "//div[@id='form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05'][@class='form active transition visible']";
    }

    public static String actionSignOffFlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']";
    }

    public static String actionSignOffDropdown() {
        return "//div[@id='control_7EB577FB-3A6B-4CAF-B5E5-1D9E6D8040F0']";
    }

    public static String actionSignOffCommentTextArea() {
        return "//div[@id='control_55A6635D-1D32-4DB9-953C-3329A76E4F05']//textarea";
    }

    public static String actionOffSave() {
        return "//div[@id='btnSave_form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']";
    }

    public static String concessionActionsSighOffCloseButton() {
        return "//div[@id='form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String SignOffactiveEditPhaseXpath() {
        return "//div[text()='Edit phase']/..//..//div[@class='step active']";
    }

    public static String getConcessionActionSighOffRecordID() {
        return "//div[@id='form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']//div[@class='recnum']//div[@class='record']";
    }

    public static String activeConcessionActions() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E'][@class='form transition visible active']";
    }

    public static String SignOffactiveClosePhaseXpath() {
        return "//div[text()='Closed']/..//..//div[@class='step active']";
    }
    public static String allAffectedPartiesCheckbox(){
        return "//div[@id='control_569C771D-5F69-4957-BF21-E480351D417E']";
    }

    public static String IncidentSignOff_Admin1_Optionxpath() {
        return"//tr[6]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin1_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-45 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin1_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-45 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin1_Detailsxpath(){
        return "//tr[6]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin2_Optionxpath() {
        return"//tr[7]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin2_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-46 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin2_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-46 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin2_Detailsxpath(){
        return "//tr[7]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin3_Optionxpath() {
        return"//tr[8]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin3_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-47 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin3_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-47 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin3_Detailsxpath(){
        return "//tr[8]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentManagement_Save_Buttonxpath() {
        return"//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@title='Save']";
    }

    public static String IncidentInvestigationStatus() {
        return"//div[@id='control_61A17BC2-21A9-463E-ADB9-536DCCAFCF4B']//li";
    }

    public static String IncidentSignOff_Admin4_Optionxpath() {
        return"//tr[9]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin4_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-48 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin4_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-48 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin4_Detailsxpath(){
        return "//tr[9]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin5_Optionxpath() {
        return"//tr[10]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin5_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-49 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin5_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-49 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin5_Detailsxpath(){
        return "//tr[10]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin6_Optionxpath() {
        return"//tr[11]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin6_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-50 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin6_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-50 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin6_Detailsxpath(){
        return "//tr[11]//div[@name='txb_n1']//input[@language]";
    }

    public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }

    public static String incidentManagement_Reports_Iconxpath() {
        return"//div[@id='btnReports_form_B6196CB4-4610-463D-9D54-7B18E614025F']//span";
    }

    public static String incidentManagement_IncidentSummary_Buttonxpath() {
        return"//div[@id='divReport_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[contains(text(),'Reports')]//..//..//..//span[contains(text(),'Incident Summary')]";
    }
    
    public static String incidentManagement_IncidentFullSummary_Buttonxpath() {
        return"//div[@id='divReport_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[contains(text(),'Reports')]//..//..//..//span[contains(text(),'Incident Full Report')]";
    }
    
     public static String sighOffConcessionCheckbox(){
        return "//div[@id='control_9A96ECD0-D256-4A86-8F4B-A903EAB67FA2']";
    }
     
     public static String activeConcessionPage() {
        return "//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096'][@class='form active transition visible']";
    }
     
     public static String statusCompleted(){
         return "//li[text()='Completed']";
     }
     
     public static String savebutton(){
         return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']";
     }

}
