/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Learnings Approval SignOff",
        createNewBrowserInstance = false
)

public class FR28_CapturLearningsApprovalSignOffMainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    String path;

    public FR28_CapturLearningsApprovalSignOffMainScenario() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();

    }

    public TestResult executeTest() {
        if (!SignOff()) {
            return narrator.testFailed("Failed to Capture Learnings Approval SignOff - " + error);
        }

        return narrator.finalizeTest("Successfully completed Capture Learnings Approval SignOff.");
    }

    public boolean SignOff() {

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to wait for the investigation details tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to click on the investigation details tab ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.InvestigationDueDateXPath(), date)) {
            error = "Failed to enter investigation due date";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.investigationScope(), getData("Investigation Scope"))) {
            error = "Failed to enter investigation scope";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to wait for the investigation details sub tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.ProcessActivityDropdownXPath())) {
            error = "Failed to click on the Process/Activity dropdown.";
            return false;
        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(getData("Process Activity 1")))) {
//            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 1");
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Process Activity 2")))) {
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskSourceDropdownXPath())) {
            error = "Failed to click on the Risk Source dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(Isometrics_PageObjects.MechanicalSelectionXPath())) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.MechanicalSelectionXPath())) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
//        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2")))) {
//            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2")))) {
            error = "Failed to click on the Risk Source option checkbox - " + getData("Risk Source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskDropdownXPath1())) {
            error = "Failed to click Risk Source close dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskDropdownXPath())) {
            error = "Failed to click on the Risk dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk")))) {
            error = "Failed to click on the Risk option checkbox - " + getData("Risk");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationTypeDropdownXPath1())) {
            error = "Failed to close on Risk dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationTypeDropdownXPath())) {
            error = "Failed to click on the Investigation Type dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Investigation Type")))) {
            error = "Failed to click on the Investigation Type option - " + getData("Investigation Type");
            return false;
        }

        //Full Investigation 
        String faileddefenses = getData("Failed defenses");
        String faileddefensesDetails = getData("Failed defenses details");
        String teamActions = getData("Team actions");
        String teamActionsDetails = getData("Team actions details");
        String task = getData("Task");
        String taskDetails = getData("Task details");
        String organisationalfactors = getData("Organisational factors");
        String organisationalfactorsDetails = getData("Organisational factors details");

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.fullInvestigationTab())) {
            error = "Failed to click on the Full Investigation tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropDownCheckXpath(faileddefenses))) {
            error = "Failed to click on the Full Investigation - Absent - failed defenses checkbox" + faileddefenses;
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.absentDetails(), faileddefensesDetails)) {
            error = "Failed to click on the Full Investigation - Absent - failed defenses details";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropDownCheckXpath(teamActions))) {
            error = "Failed to click on the Full Investigation - Individual - team actions checkbox" + teamActions;
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.teamDetails(), teamActionsDetails)) {
            error = "Failed to click on the Full Investigation - Individual - team actions details";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropDownCheckXpath(task))) {
            error = "Failed to click on the Full Investigation - Task - environmental factors checkbox" + task;
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.taskDetails(), taskDetails)) {
            error = "Failed to click on the Full Investigation Task - environmental factors details";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropDownCheckXpath(organisationalfactors))) {
            error = "Failed to click on the Full Investigation - Organisational factors - checkbox" + organisationalfactors;
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.organisationalDetails(), organisationalfactorsDetails)) {
            error = "Failed to click on the Full Investigation Task - environmental factors details";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.saveIncidentManagementFullInvestigation())) {
            error = "Failed to click on the Full Investigation - save " + organisationalfactors;
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
         if (!SeleniumDriverInstance.scrollToElement(Isometrics_PageObjects.fullInvestigationTab())) {
            error = "Failed to scroll on the Full Investigation tab ";
            return false;
        }


        narrator.stepPassed("Full Investigation");
        narrator.stepPassedWithScreenShot("Successfulled saved Full Investigation ");

//        }
        return true;
    }

}
