/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Witness Statements Main Scenario HYPER",
        createNewBrowserInstance = false
)

public class FR3_Capture_Witness_Statements_MainScenarioHYPER extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Witness_Statements_MainScenarioHYPER()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!ValidatePanelsAreDisplayed())
        {
            return narrator.testFailed("Failed to validate that the Persons Involved, Witness Statement and Equipment Involved panels are displayed - " + error);
        }
        if (!witnessStatement())
        {
            return narrator.testFailed("Failed to fill in incident report - " + error);
        }
        return narrator.finalizeTest("Successfully captured the details for Witness Statements");
    }

    public boolean ValidatePanelsAreDisplayed()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.PersonsInvolvedXpath()))
        {
            error = "Failed to validate that the Persons Involved panel is dispayed";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.PersonsInvolvedXpath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Persons Involved")))
            {
                narrator.stepPassed("Successfully validated that the Persons Involved panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.WitnessStatementsXpath()))
        {
            error = "Failed to validate that the Witness Statement panel is dispayed";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.WitnessStatementsXpath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Witness Statements")))
            {
                narrator.stepPassed("Successfully validated that the Witness Statement panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.EquipmentInvolvedTabXpath()))
        {
            error = "Failed to validate that the Equipment Involved panel is dispayed";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.EquipmentInvolvedTabXpath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Equipment Involved")))
            {
                narrator.stepPassed("Successfully validated that the Equipment Involved panel is displayed: " + text);
            }
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Persons Involved, Witness Statement and Equipment Involved panels are displayed");
        return true;
    }

    public boolean witnessStatement()
    {

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to wait for witness Statements button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to click witness Statements button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.ValidateGridIsNotEditableXPath()))
        {
            error = "Failed to validate that the editable grid is displayed";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Witness Statement non-editable grid is displayed");
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsAddXpath()))
        {
            error = "Failed to click Witness Statements add button";
        }

        narrator.stepPassedWithScreenShot("Successfully validated that a new module is displayed in the Add Phase");

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40))
            {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessNameXpath(), getData("witness Name")))
        {
            error = "Failed to enter witness name ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSurnameXpath(), getData("witness Surname")))
        {
            error = "Failed to enter witness Surname";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSummaryXpath(), getData("witness Sunnary")))
        {
            error = "Failed to enter witness Sunnary";
            return false;
        }
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic()))
//        {
//            error = "Failed to click the Upload image";
//            return false;
//        }
//        String pathofImages = System.getProperty("user.dir") + "\\images";
//
//        String path = new File(pathofImages).getAbsolutePath();
//        System.out.println("path " + pathofImages);
//
//        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path))
//        {
//            error = "Failed to click image 1";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.selectIdentificationType())
//        {
//            error = "Failed to press Enter ";
//            return false;
//        }
//        if(!sikuliDriverUtility.MouseMoveElement(VerificationAndAdditionalPageObject.dvt2Pic())){
//            error = "failed to move mouse.";
//            return false;
//        }
//        
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic()))
//        {
//            error = "Failed to click the witness dvt 2 image";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic()))
//        {
//            error = "Failed to click the witness open window button";
//            return false;
//        }

        //Add support document
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.linkalt())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.linkalt())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.LinkURL(), "www.inspiredtesting.com/" )){
            error = "Failed to enter 'www.inspiredtesting.com/' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : https://www.inspiredtesting.com/");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.urlTitle2())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.urlTitle2(), "Inspired Testing")){
            error = "Failed to enter 'Inspired Testing' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : 'Inspired Testing'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.urlAddButton2())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.urlAddButton2())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded 'Inspired Testing' document using 'https://www.inspiredtesting.com/' Link.");
        
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        narrator.stepPassed(
                "Successfully entered witness statement " + getData("witness Name") + ", " + getData("witness Surname") + ", " + getData("witness Sunnary"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessSaveCloseButtonXpath()))
        {
            error = "Failed to click witness save and close button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableWitness(), 20))
        {

            error = "Failed to click save Witness details";
            return false;
        }

        narrator.stepPassedWithScreenShot("Ssaved Witness details");

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to wait for witness Statements close tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to click witness Statements close tab";
            return false;
        }

        return true;
    }
}
