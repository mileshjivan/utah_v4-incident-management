/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.Keys;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Log Injured Person",
        createNewBrowserInstance = false
)
public class InjuredPersons extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public InjuredPersons() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {

        if (!injuuredPersons()) {
            return narrator.testFailed("Failed to enter Injured Persons due - " + error);
        }

        if (!fillInInjuryOrIllnessDetails()) {
            return narrator.testFailed("Failed to enter Injured Persons details - " + error);
        }

        if (!ReturnToWorkManagement()) {
            return narrator.testFailed("Failed to fill in the Return to work mangement details, as well as the Status, Origin of Case and Link to incident record details - " + error);
        }

        if (!DoctorsNotesDetails()) {
            return narrator.testFailed("Failed to enter Doctor's notes details - " + error);
        }

        if (!SuitableDuties()) {
            return narrator.testFailed("Failed to enter the Suitable Duties details - " + error);
        }

        if (!ReturnToWorkMangementSubHeading()) {
            return narrator.testFailed("Failed to enter the Return to Work Management details - " + error);
        }

        if (!RehabilitationProvider()) {
            return narrator.testFailed("Failed to enter the Rehabilitation Provider details - " + error);
        }

        if (!Agreements()) {
            return narrator.testFailed("Failed to enter the Agreements details - " + error);
        }

        return narrator.finalizeTest("Successfully logging An Incident");
    }

    public boolean superVisor() {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.Safety())) {
            error = "Failed to click Safety supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Safety supervisor")))) {
            error = "Failed to select Safety supervisor ";
            return false;
        }
        narrator.stepPassedWithScreenShot("successfully selected Safety supervisor :" + getData("Safety supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentXpath())) {
            error = "Failed to click Environment button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentDropDownXpath())) {
            error = "Failed to click Environment dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(testData.getData("Environmental supervisor")))) {
            error = "Failed to select Environment supervisor ";
            return false;
        }
        narrator.stepPassedWithScreenShot("successfully selected  Environment supervisor :" + testData.getData("Environmental supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualitytXpath())) {
            error = "Failed to click Quality button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualityDropDownXpath())) {
            error = "Failed to click Quality dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(testData.getData("Quality supervisor")))) {
            error = "Failed to select Quality supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Quality supervisor :" + testData.getData("Quality supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyXpath())) {
            error = "Failed to click railway Safety button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyDropDownXpath())) {
            error = "Failed to click railway Safety dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(testData.getData("Railway safety supervisor")))) {
            error = "Failed to select railway Safety supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Railway safety supervisor :" + testData.getData("Railway safety supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusXpath())) {
            error = "Failed to click Social Sustainability button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusDropDownXpath())) {
            error = "Failed to click Social supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(testData.getData("Social supervisor")))) {
            error = "Failed to select supervisor supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Social supervisor :" + testData.getData("Social supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneXpath())) {
            error = "Failed to click Occupational hygiene button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneDropDownXpath())) {
            error = "Failed to Occupational hygiene supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(testData.getData("Occupational hygiene supervisor")))) {
            error = "Failed to select Occupational hygiene supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Occupational hygiene supervisor :" + testData.getData("Occupational hygiene supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceXpath())) {
            error = "Failed to click Compliance button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceDropDownXpath())) {
            error = "Failed to Compliance supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(testData.getData("Compliance supervisor")))) {
            error = "Failed to select Compliance supervisor ";
            return false;
        }
        narrator.stepPassedWithScreenShot("successfully selected Compliance supervisor :" + testData.getData("Compliance supervisor"));

        return true;
    }

    public boolean injuuredPersons() {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to wait for the safety tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to click the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.addInjuredPersonsButtonXPath())) {
            error = "Failed to click the Add button ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 4)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 500)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }
        
        SeleniumDriverInstance.pause(10000);
                
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuredPersonsFlow())) {
            error = "Fail to click Injured persons process flow";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.employeeTypeDropdownXPath(), 50)){
            error = "Failed to wait for the employee type dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.employeeTypeDropdownXPath())) {
            error = "Failed to wait for the employee type dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.employeeTypeDropdownXPath())) {
            error = "Failed to click the employee type dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(getData("Employee")))) {
            error = "Failed to click the employee type element: " + testData.getData("Employee");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.fullNameDropdownXPath())) {
            error = "Failed to click the full name dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(testData.getData("Full Name")))) {
            error = "Failed to click the full name element: " + testData.getData("Full Name");
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuryOrIlnessDropdownXPath())) {
            error = "Failed to click the injury or illness dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(testData.getData("Yes")))) {
            error = "Failed to click the injury or illness element: " + testData.getData("Yes");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.drugAlcoholTestRequiredDropdownXPath())) {
            error = "Failed to click the drugs and alcohol test dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Yes")))) {
            error = "Failed to click the drugs and alcohol test element: " + testData.getData("Yes");
            return false;
        }

        return true;
    }

    public boolean fillInInjuryOrIllnessDetails() {

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.jobProfileTextFieldXPath(), testData.getData("Job Profile"))) {
            error = "Failed to fill in the Job profile: " + testData.getData("Job Profile");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.positionStartDatePickerXPath(), date)) {
            error = "Failed to fill in the Position start date: " + date;
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.usualJobTasksXPath(), testData.getData("Job Task"))) {
            error = "Failed to fill in the Job task: " + testData.getData("Job Task");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuryOrIllnessClassificationXPath())) {
            error = "Failed to click the Injury Classification ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.treeElementXPath(testData.getData("Classification")))) {
            error = "Failed to select the Injury item: " + testData.getData("Classification");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Treatment")))) {
            error = "Failed to fselect the Medical Treatment Case item: " + testData.getData("Treatment");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.descriptionXPath(), testData.getData("Description"))) {
            error = "Failed to fill in the Description field: " + testData.getData("Description");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.activityAtTheTimeDropdownXPath())) {
            error = "Failed to click the Activity at the time dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Activity")))) {
            error = "Failed to select the Activity at the time item: " + testData.getData("Activity");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsDropdownXPath())) {
            error = "Failed to click the Body Parts dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Body Part 1") + " "), 100))
        {
            error = "Failed to scroll to the following item: " + testData.getData("Body Part 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Body Part 1") + " "))) {
            error = "Failed to select the following item: " + testData.getData("Body Part 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(testData.getData("Body Part 2") + " "))) {
            error = "Failed to select the following item: " + testData.getData("Body Part 2");
            return false;
        }

        SeleniumDriverInstance.pause(500);
        // Select option on Nature Of Injury dropdown by clicking on tree items untill it leads to the final item
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.natureOfInjuryLabelXPath())) {
            error = "Failed to click the nature of injury label";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.natureOfInjuryDropdownXPath())) {
            error = "Failed to click on the Nature of Injury dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Classification")))) {
            error = "Failed to select the following item: " + testData.getData("Classification");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 1") + " "))) {
            error = "Failed to select the following item: " + testData.getData("Nature 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 2")))) {
            error = "Failed to select the following item: " + testData.getData("Nature 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Nature 3")))) {
            error = "Failed to select the following item: " + testData.getData("Nature 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.mechanismDropdownXPath())) {
            error = "Failed to click on the Mechanism dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Mechanism 1") + " "))) {
            error = "Failed to select the following item: " + testData.getData("Mechanism 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Mechanism 2")))) {
            error = "Failed to select the following item: " + testData.getData("Mechanism 2");
            return false;
        }

        // Fill in the Follow Up Description text area and click the Follow Up Required checkbox, Follow Up Details textarea appears
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.treatmentProvidedDescriptionXPath(), testData.getData("Follow Up Description"))) {
            error = "Failed to fill in the Follow Up Description field: " + testData.getData("Follow Up Description");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.FollowUpRequiredCheckBoxXPath())) {
            error = "Failed to click on the Follow Up Required checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.followUpDetailsXPath())) {
            error = "Failed to wait for the Follow Up Details text area  ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.followUpDetailsXPath(), getData("Follow Up Details"))) {
            error = "Failed to fill in the Follow Up Details text area: " + getData("Follow Up Details");
            return false;
        }

        //Check Additional Treatment Required Checkbox, Treatment away from home checkbox appears
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.AdditionalTreatmentRequiredCheckBoxXPath())) {
            error = "Failed to click on the Additional Treatment Required checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.TreatmentAwayFromWorkpaceCheckBoxXPath())) {
            error = "Failed to wait for the Treatment Away From Home checkbox to appear ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.TreatmentAwayFromWorkpaceCheckBoxXPath())) {
            error = "Failed to click on the Treatment Away From Home checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.TreatedInEmergencyRoomCheckBoxXPath())) {
            error = "Failed to click on the Treated In Emergency Room checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.TreatedInpatientOverNightCheckBoxXPath())) {
            error = "Failed to click on the Treated Inpatient Over Night checkbox ";
            return false;
        }

        // Fill in Treatment Facility Details text area
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.TreatmentFacililtyDetailsXPath(), getData("Treatment Facility Details"))) {
            error = "Failed to fill in the Treatment Facility Details text area: " + getData("Treatment Facility Details");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsThisInjuryRecordableCheckBoxXPath())) {
            error = "Failed to click on the Is This Injury Recordable checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath())) {
            error = "Failed to wait for the Is It Reportable checkbox to appear ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath())) {
            error = "Failed to click on the Is It Reportable checkbox";
            return false;
        }

        // Reportable to Dropdown appears after checking Is It Reportable dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReportableToDropdownXPath())) {
            error = "Failed to click the Reportable to Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Regulatory authority")))) {
            error = "Failed to select the Reportable to item: " + testData.getData("Regulatory authority");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.InjuryClaimCheckBoxXPath())) {
            error = "Failed to click on the Injury Claim checkbox";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveAndContinueButtonXPath())) {
            error = "Failed to click on the Save And Continue button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath())) {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" ")) {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved"))) {
                narrator.stepPassed("Successfully saved the injured Persons record: " + text);
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.injuredPersonseditPhaseActive(),2)) {
            error = "Failed for edit phase";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered the Incident Management/Injured Persons form");
        return true;
    }
   

    public boolean ReturnToWorkManagement() 
    {

        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.ReturnToWorkManagementXPath(), 100))
        {
            error = "Failed to scroll to the Return To Work Mangement heading ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReturnToWorkManagementAddButtonXPath())) {
            error = "Failed to click the Add button ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.StatusDropdownXPath())) {
            error = "Failed to click the Status Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Status")))) {
            error = "Failed to select the Reportable to item: " + testData.getData("Status");
            return false;
        }

        String[] origin = getData("Origin Of Case").split(",");

        for (int i = 0; i <= origin.length - 1; i++) {
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDropdownXPath())) {
                error = "Failed to click the Origin Of Case Dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(origin[i]))) {
                error = "Failed to select the following item: " + origin[i];
                return false;
            }

            if (origin[i].equals("External medical")) {
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ExternalCaseDropdownXPath())) {
                    error = "Failed to click the External Case Dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("External Case")))) {
                    error = "Failed to select the following item item: " + getData("External Case");
                    return false;
                }
                narrator.stepPassed("Successfully validated the item on the External Case dropdown: " + getData("External Case"));
            } else if (origin[i].equals("Injury")) {
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(origin[i]))) {
                    error = "Failed to click the Origin Of Case Dropdown";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Injury 1") + " "))) {
                    error = "Failed to select the following item item: " + getData("Injury 1");
                    return false;
                }
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.OriginOfCaseValidateItemDropdownXPath(origin[i]))) {
                error = "Failed to wait for the following item on the dropdown: " + origin[i];
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseValidateItemDropdownXPath(origin[i]))) {
                error = "Failed clicked on the dropdown: ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDeselectButtonOnDropdownXPath())) {
                error = "Failed to click the deselct button";
                return false;
            }
            narrator.stepPassed("Successfully selected this item on the Origin Of Case dropdown: " + origin[i]);
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDropdownXPath())) {
            error = "Failed clicked on the dropdown: ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDeselectButtonOnDropdownXPath())) {
            error = "Failed to click the deselct button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDropdownXPath())) {
            error = "Failed clicked on the dropdown: ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(getData("Classification")))) {
            error = "Failed to click the Origin Of Case Dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Injury 2")))) {
            error = "Failed to select the following item item: " + getData("Injury 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.LinkToIncidentRecordDropdownXPath())) {
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Link To Incident Record")))) {
            error = "Failed towait for the following item: " + getData("Link To Incident Record");
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Link To Incident Record")), 100))
        {
            error = "Failed to move to the following item: " + getData("Link To Incident Record");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Link To Incident Record")))) {
            error = "Failed to select the following item: " + getData("Link To Incident Record");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully filled in the Return to work mangement details, as well as the Status, Origin of Case and Link to incident record details");
        return true;
    }

    public boolean DoctorsNotesDetails() {
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.MedicalPractionerDropdownXPath())) {
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Medical Practionar")))) {
            error = "Failed to select the following item: " + getData("Medical Practionar");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DoctorsNotesTextAreaXPath(), getData("Doctors Notes"))) {
            error = "Failed to fill in the Doctor's Notes text area";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateIssuedXPath(), date)) {
            error = "Failed to enter text into the Date Issued text field";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.FitnessForWorkDropdownXPath())) {
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Fitness")))) {
            error = "Failed to select the following item: " + getData("Fitness");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateFromXPath(), date)) {
            error = "Failed to enter text into the Date From text field ";
            return false;
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.DATE, 1);
            Date currentDatePlusOne = cal.getTime();

            if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateToXPath(), dateFormat.format(currentDatePlusOne))) {
                error = "Failed to enter text into the Date To text field";
                return false;
            }
            SeleniumDriverInstance.pressKey(Keys.ENTER);
        } catch (Exception e) {
            error = "Failed to get the current date - " + e.getMessage();
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic())) {
            error = "Failed to click the Regulatory Authority  Upload image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic())) {
            error = "Failed to click the Regulatory Authority  dvt 2 image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic())) {
            error = "Failed to click the Regulatory Authority open window button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully filled in the Doctor's Notes Details");

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.DoctorsNoteDetailsHeadingXPath())) {
            error = "Failed to click on the Doctor's Notes Heading";
            return false;
        }

        return true;
    }

    public boolean SuitableDuties()
    {
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.SuitableDuitiesHeadingXPath(), 100))
        {
            error = "Failed to move to the Suitable Duties heading ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SuitableDuitiesHeadingXPath())) {
            error = "Failed to click on the Suitable Duties heading ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsASuitableOfferOfEmploymentAttachedDropdownXPath())) {
            error = "Failed to click on the Offer Of Employment dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Yes")))) {
            error = "Failed to select the following item: " + getData("Yes");
            return false;
        }

        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.ActionsAndSupportinhInformationHeadingXPath(), 100))
        {
            error = "Failed to move to the Suitable Duties heading ";
            return false;
        }
        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic())) {
            error = "Failed to click the Upload image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic())) {
            error = "Failed to click the dvt 2 image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic())) {
            error = "Failed to click the Opem window button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully filled in the Suitable Duties Details");
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SuitableDuitiesHeadingXPath())) {
            error = "Failed to click on the Suitable Duties heading ";
            return false;
        }

        return true;
    }

    public boolean ReturnToWorkMangementSubHeading() {

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReturnToWorkArrangementHeadingXPath())) {
            error = "Failed to click on the Return To Work Arrangement heading ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DutieOrTasksToBeUndertakenTextAreaXPath(), getData("Work Mangement"))) {
            error = "Failed to enter text into the Duties or tasks to be undertaken text area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.WorkplaceSupportsTextAreaXPath(), getData("Work Mangement"))) {
            error = "Failed to enter text into the Workplace supports, aids or modifications to be provided text area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.SpecificDutiesOrTasksTextAreaXPath(), getData("Work Mangement"))) {
            error = "Failed to enter text into the Specific duties or tasks to be avoided text area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.ReturnToWorkArrangementTextAreaXPath(), getData("Work Mangement"))) {
            error = "Failed to enter text into the Return to Work Mangement text area";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully filled in the Return To Work Arrangements Details");
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReturnToWorkArrangementHeadingXPath())) {
            error = "Failed to click on the Return To Work Arrangement heading ";
            return false;
        }

        return true;
    }

    public boolean RehabilitationProvider() {
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.RehabilitationsProviderHeadingXPath())) {
            error = "Failed to click on the Rehabilitation Provider heading ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.ProviderNameTextFieldXPath(), getData("Work Mangement"))) {
            error = "Failed to enter text into the Provider Name text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.ContactNumberTextFieldXPath(), getData("Provider Number"))) {
            error = "Failed to enter text into the Contact Number text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.EmailAddressTextFieldXPath(), getData("Provider Email"))) {
            error = "Failed to enter text into the Provider Email text field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully filled in the Rehabilitations Provider Details");
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.RehabilitationsProviderHeadingXPath())) {
            error = "Failed to click on the Rehabilitation Provider heading ";
            return false;
        }

        return true;
    }

    public boolean Agreements() {
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.AgreementsHeadingXPath())) {
            error = "Failed to click on the Agreements heading ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.WorkersAgreementNameXPath(), getData("Agreements"))) {
            error = "Failed to enter text into the Workers Agreement name text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.WorkersAgreementDateOfAgreementXPath(), date)) {
            error = "Failed to enter the date into the Workers Agreement Date Of Agreement text field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.SupervisorsAgreementNameXPath(), getData("Agreements"))) {
            error = "Failed to enter text into the Supervisors Agreement name text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.SupervisorsAgreementDateOfAgreementXPath(), date)) {
            error = "Failed to enter the date into the Supervisors Agreement Date Of Agreement text field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.ReturnToWorkCoordinatorsAgreementNameXPath(), getData("Agreements"))) {
            error = "Failed to enter text into the Return To Work Coordinators Agreement name text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.ReturnToWorkCoordinatorsDateOfAgreementXPath(), date)) {
            error = "Failed to enter the date into the Return To Work Coordinators Agreement Date Of Agreement text field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.TreatingMedicalPractionersAgreemenrNameXPath(), getData("Agreements"))) {
            error = "Failed to enter text into the Treating Medical Practioners Agreement name text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.TreatingMedicalPractionersAgreemenrDateOfAgreementXPath(), date)) {
            error = "Failed to enter the date into the Treating Medical Practioners Agreement Date Of Agreement text field";
            return false;
        }

//        WebElement element = SeleniumDriverInstance.Driver.findElement(By.id("fileUpload"));
//        element.sendKeys("C:myfile.txt");
        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic())) {
            error = "Failed to click the Upload image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic())) {
            error = "Failed to click the dvt 2 image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic())) {
            error = "Failed to click the Opem window button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveUploadedReportButtonXPath())) {
            error = "Failed to click on the Save Uploaded Report Button ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath())) {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" ")) {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved"))) {
                narrator.stepPassed("Successfully saved the injured Persons record: " + text);
            }
        }

        narrator.stepPassedWithScreenShot("Succesfully filled in the Agreements details");
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.CloseFormButtonXPath())) {
            error = "Failed to click on the close form Button ";
            return false;
        }

        return true;
    }
}
