/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Update Investigation Details Alternate Scenario 1",
        createNewBrowserInstance = false
)
public class FR19_Update_Investigation_Details_AlternateScenario1 extends BaseClass
{

    String date;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber2;

    public FR19_Update_Investigation_Details_AlternateScenario1()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        recordNumber2 = "";
    }

    public TestResult executeTest()
    {
        if (!FillInInvestigationPlanning())
        {
            return narrator.testFailed("Failed to check the concession Status - " + error);
        }
        if (!AddInvestigationDetails())
        {
            return narrator.testFailed("Failed to add the investigation details - " + error);
        }
        return narrator.finalizeTest("Successfully validated panels based on Investigation type - " + getData("Investigation Type"));
    }

    public boolean FillInInvestigationPlanning()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.supporting_Information_XPath()))
        {
            error = "Failed to wait for the supporting information panel";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.supporting_Information_XPath()))
        {
            error = "Failed to wait for the supporting information panel";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.InvestigationDueDateXPath()))
        {
            error = "Failed to wait for the investigation due date text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.InvestigationDueDateXPath(), date))
        {
            error = "Failed to enter text into the investigation due date text field";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.InvestigationScopeTextAreaXPath()))
        {
            error = "Failed to wait for the investigation scope text area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.InvestigationScopeTextAreaXPath(), getData("Scope")))
        {
            error = "Failed to enter text into the investigation scope text area";
            return false;
        }

//        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait2(), 2))
//        {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait(), 400))
//            {
//                error = "Webside took too long to load wait reached the time out";
//                return false;
//            }
//        }
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath(), 3))
//        {
//            error = "Failed to click on the Save And Continue button";
//            return false;
//        } else
//        {
//            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());
//
//            if (text.equals(" "))
//            {
//                error = "String cannot be empty";
//                return false;
//            } else if (text.equals(testData.getData("Record Saved")))
//            {
//                narrator.stepPassedWithScreenShot("Successfully saved the Data Collection item: " + text);
//            }
//        }
        return true;
    }

    public boolean AddInvestigationDetails()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.InvestigationDetailTabXPath()))
        {
            error = "Failed to wait for the investigation details sub tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath()))
        {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.ProcessActivityDropdownXPath()))
        {
            error = "Failed to click on the Process/Activity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(getData("Process Activity 1"))))
        {
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Process Activity 2"))))
        {
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath()))
        {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskSourceDropdownXPath()))
        {
            error = "Failed to click on the Risk Source dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(Isometrics_PageObjects.MechanicalSelectionXPath()))
        {
            error = "Failed to move to the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.MechanicalSelectionXPath()))
        {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2"))))
        {
            error = "Failed to move to the Risk Source option checkbox - " + getData("Risk Source 2");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2"))))
        {
            error = "Failed to click on the Risk Source option checkbox - " + getData("Risk Source 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath()))
        {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskDropdownXPath()))
        {
            error = "Failed to click on the Risk dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk"))))
        {
            error = "Failed to click on the Risk option checkbox - " + getData("Risk");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath()))
        {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationTypeDropdownXPath()))
        {
            error = "Failed to click on the Investigation Type dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Investigation Type"))))
        {
            error = "Failed to click on the Investigation Type option - " + getData("Investigation Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath()))
        {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Full Investigation tabs are displayed");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.CriticalControlsLinkedToThisBusinessProcessXPath()))
        {
            error = "Failed to wait for the Critical controls linked to this Process/Activity panel.";
            return false;
        }
        narrator.stepPassed("Critical controls linked to this Process/Activity panel is displayed");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.WhyAnalysisPanelXPath()))
        {
            error = "Failed to wait for the Why Analysis panel.";
            return false;
        }
        narrator.stepPassed("Why Analysis panel is displayed");
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.RelatedRisksXPath()))
        {
            error = "Failed to wait for the Related Risks panel.";
            return false;
        }
        narrator.stepPassed("Related Risks panel is displayed");
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.RelatedBowtiesXPath()))
        {
            error = "Failed to wait for the Related Bowties panel.";
            return false;
        }
        narrator.stepPassed("Related Bowties panel is displayed");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.RelatedJSAPanelXPath()))
        {
            error = "Failed to wait for the Related JSA's panel.";
            return false;
        }
        narrator.stepPassed("Related JSA's panel is displayed");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.RelatedObligationsPanelXPath()))
        {
            error = "Failed to wait for the Related Obligations panel.";
            return false;
        }
        narrator.stepPassed("Related Obligations panel is displayed");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.RelatedPoliciesAndProceduresPanelXPath()))
        {
            error = "Failed to wait for the Related Policies and Procedures panel.";
            return false;
        }
        narrator.stepPassed("Related Policies and Procedures panel is displayed");
        
narrator.stepPassedWithScreenShot("Successfully validated that all panels are displayed");
        return true;
    }
}
