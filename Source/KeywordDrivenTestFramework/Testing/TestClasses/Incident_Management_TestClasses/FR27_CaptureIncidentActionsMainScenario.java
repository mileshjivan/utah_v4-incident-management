/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.EnvironmentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MonitoringAirPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Incident Action",
        createNewBrowserInstance = false
)

public class FR27_CaptureIncidentActionsMainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    String path;

    public FR27_CaptureIncidentActionsMainScenario() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();

    }

    public TestResult executeTest() {
        if (!incidentAction()) {
            return narrator.testFailed("Failed to Incident Actions details - " + error);
        }

        return narrator.finalizeTest("Successfully Incident Actions record is saved.");
    }

    public boolean incidentAction() {

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to wait for the investigation details tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to click on the investigation details tab ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.InvestigationDueDateXPath(), date)) {
            error = "Failed to enter investigation due date";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.investigationScope(), getData("Investigation Scope"))) {
            error = "Failed to enter investigation scope";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.actionTab())) {
            error = "Failed to click on - Incident Managament - action tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.incidentActionAdd())) {
            error = "Failed to click on - Incident Managament - Incident Actions Add ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.incidentActionProcessButton())) {
            error = "Failed to wait on - Incident Actions - Process Flow button ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.incidentActionProcessButton())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.incidentActionProcessButton())) {
                error = "Failed to wait on - Incident Actions - Process Flow button ";
                return false;
            }
            pause(8000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.incidentActionProcessButton())) {
                error = "Failed to click on - Incident Actions - Process Flow button";
                return false;
            }
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.actionDescription(), getData("Action description"))) {
            error = "Failed to enter on - Incident Actions - Action description ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.departmentResponsibleDropdown())) {
            error = "Failed to click on - Incident Actions - Department Responsible dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropdwonOptionXpath(getData("Department responsible")))) {
            error = "Failed to click on - Incident Actions - Department Responsible ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.responsiblePersonDropdown())) {
            error = "Failed to click on - Incident Actions - Responsible person dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.responsiblePerson(getData("Responsible person")))) {
            error = "Failed to click on - Incident Actions - Responsible person ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.actionDueDate(), date)) {
            error = "Failed to enter on - Incident Actions - Action due date ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.incidentActionsSaveButton())) {
            error = "Failed to click on - Incident Actions - Save";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.activeToBeInitiatedXpath())) {
            error = "Failed to save on - Incident Actionss record";
            return false;
        }

        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(Isometrics_PageObjects.incidentActionId()).split("#");
        setRecordId(Actionid[1]);

        narrator.stepPassed("Control Incident Actions Record Number: " + Actionid[1]);
        narrator.stepPassedWithScreenShot("Successfulled saved Incident Actions ");

//        }
        return true;
    }

}
