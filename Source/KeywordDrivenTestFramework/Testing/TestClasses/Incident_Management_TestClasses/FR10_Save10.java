/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "FR10 Save and close",
        createNewBrowserInstance = false
)
public class FR10_Save10 extends BaseClass {

    String error = "";

    public FR10_Save10() {

    }

    public TestResult executeTest() {

        if (!captureDetailsAndsaveStep2()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed injured person ");
    }

    public boolean captureDetailsAndsaveStep2() {

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.saveXpath())) {
            error = "Failed to click to Save and close ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(1500);

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.injuredPersonProcessButtonXpath())) {
            error = "Failed to wait - injured persons details process flow  ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuredPersonProcessButtonXpath())) {
            error = "Failed to click - injured persons details process flow  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.injuryEditphaseXpath(), 3)) {
            error = "Failed to move injured persons details to edit phase";
            return false;
        }

        narrator.stepPassed("Successfully saved injured persons ");

        return true;
    }
}
