/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Data Collection",
        createNewBrowserInstance = false
)
public class FR18_Capture_Data_Collection_MainScenario extends BaseClass
{

    String date;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber2;

    public FR18_Capture_Data_Collection_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        recordNumber2 = "";
    }

    public TestResult executeTest()
    {
        if (!NavigateToDataCollection())
        {
            return narrator.testFailed("Failed to check the concession Status - " + error);
        }
        return narrator.finalizeTest("Successfully created a new Data Collection record");
    }

    public boolean NavigateToDataCollection()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.incident_Investigation_Tab_Xpath()))
        {
            error = "Failed to wait for the Incident Investigation sub tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.incident_Investigation_Tab_Xpath()))
        {
            error = "Failed to click on the Incident Investigation sub tab";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.investigation_Planning_Tab_XPath()))
        {
            error = "Failed to wait for the Investigation planning sub tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.investigation_Planning_Tab_XPath()))
        {
            error = "Failed to click on the Investigation planning sub tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.supporting_Information_XPath()))
        {
            error = "Failed to wait for the supporting information panel";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.supporting_Information_XPath()))
        {
            error = "Failed to wait for the supporting information panel";
            return false;
        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.process_Flow_Button_XPath()))
//        {
//            error = "Failed to wait for the Proess Flow button";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.process_Flow_Button_XPath()))
//        {
//            error = "Failed to click on the Proess Flow button";
//            return false;
//        }
        //Data Collection editable grid 
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.data_Collection_Editable_Grid_XPath()))
        {
            error = "Failed to wait for the Data Collection editable grid";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.add_Data_Collection_Button_XPath()))
        {
            error = "Failed to click on the Add button on the Data Collection grid";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.file_Name_Data_Collection_Field_XPath()))
        {
            error = "Failed to wait for the file name text field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Data Collection editable grid successfully opens on a new line in the Add Phase");
        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.file_Name_Data_Collection_Field_XPath(), getData("File Name")))
        {
            error = "Failed to enter text into the File Name text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.description_Data_Collection_Field_XPath(), getData("Description")))
        {
            error = "Failed to enter text into the Description text field";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.save_New_Data_Collection_Item_XPath()))
        {
            error = "Failed to click on the Save button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait2(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait(), 400))
            {
                error = "Webside took too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath(), 3))
        {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved")))
            {
                narrator.stepPassedWithScreenShot("Successfully saved the Data Collection item: " + text);
            }
        }
        return true;
    }
}
