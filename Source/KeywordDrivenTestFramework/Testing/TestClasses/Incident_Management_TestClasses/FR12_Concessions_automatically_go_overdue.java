/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author sjonck
 */
@KeywordAnnotation(
        Keyword = "Concessions automatically go overdue",
        createNewBrowserInstance = false
)
public class FR12_Concessions_automatically_go_overdue extends BaseClass
{

    String date;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber2;
    String concessionRecordNumber;
    String concessionActionRecordNumber;

    public FR12_Concessions_automatically_go_overdue()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        recordNumber2 = "";
    }

    public TestResult executeTest()
    {
        if (!CheckConcessionEndDate())
        {
            return narrator.testFailed("Failed to check the concession Status - " + error);
        }
        if (!ValidateOverdueFilter())
        {
            return narrator.testFailed("Failed to validate that the Overdue filter is applied - " + error);
        }
        return narrator.finalizeTest("Completed Capturing Concessions automatically go overdue");
    }

    public boolean CheckConcessionEndDate()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concession_Status_XPath()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.VerificationAndAdditionalTab()))
            {
                error = "Failed to wait for the Verification And Additional Tab ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.VerificationAndAdditionalTab()))
            {
                error = "Failed to click on the Verification And Additional Tab ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
            {
                error = "Failed to wait for Quality tab.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
            {
                error = "Failed to click Quality tab.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath()))
            {
                error = "Failed to wait for Quality concession expand button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath()))
            {
                error = "Failed to click Quality concession expand button.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concession_Status_XPath()))
            {
                error = "Failed to wait for the Status column";
                return false;
            }
        }
        narrator.stepPassed("Successfully opened an Incident Record");
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
            {
                error = "Failed to wait for Quality tab.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
            {
                error = "Failed to click Quality tab.";
                return false;
            }
        }
        narrator.stepPassed("Successfully validated that the Quality tab is displayed");

        try
        {
            String status = SeleniumDriverInstance.retrieveTextByXpath(Isometrics_PageObjects.concession_Status_XPath());

            if (status.equals(getData("Status1")) || status.equals(getData("Status2")))
            {
                narrator.stepPassed("Successfully validated status: Expected - " + getData("Status1") + " or " + getData("Status2") + " Result: " + status);
            } else
            {
                error = "Status should be " + getData("Status1") + " or " + getData("Status2");
                return false;
            }

            if (status.equals(" "))
            {
                error = "Status cannot be empty";
                return false;
            }
            narrator.stepPassed("Status needs to be changed" + status);
        } catch (Exception e)
        {
            error = "Failed to retrive text from the table " + e;
            return false;
        }

        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.data_Item_Concessions_XPath(getData("Description"))))
        {
            error = "Failed to wait for data item";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.data_Item_Concessions_XPath(getData("Description"))))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
            {
                error = "Failed to wait for Quality tab.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
            {
                error = "Failed to click Quality tab.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.data_Item_Concessions_XPath(getData("Description"))))
            {
                error = "Failed to click on the data item";
                return false;
            }
        }
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait2(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concessions_Loading_VisibleXPath(), 10))
        {
            if (!SeleniumDriverInstance.waitForElementsByXpath(Isometrics_PageObjects.concessions_Loading_HiddenXPath()))
            {
                error = "Failed to wait for Loading form to dissapear";
                return false;
            }
        }

        int counter = 1;
        while (SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concessions_Loading_VisibleXPath()))
        {
            pause(1000);
            if (counter >= 100)
            {
                error = "Took to long to wait for page to load";
                return false;
            }
            counter++;
        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementsByXpath(Isometrics_PageObjects.concessions_Loading_HiddenXPath()))
        {
            error = "Failed to wait for Loading form to dissapear";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_EndDate_InputAreaxpath()))
        {
            error = "Failed to wait for concession End date Input Field.";
            return false;
        }
        
        
        //Validation Concession End Date
        try
        {
            String concessionEndDate = getData("ConcessionEndDate");
            Date d = new Date();

            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(concessionEndDate);
            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            // Date 1 occurs after Date 2
//            if (date1.compareTo(date2) > 0)
//            {   
//            } 
            //Date 1 occurs before Date 2
            if (date1.compareTo(date2) < 0)
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concession_Status_Dropdown_XPath()))
                {
                    error = "Failed to wait for concession status dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.concession_Status_Dropdown_XPath()))
                {
                    error = "Failed to click on the concession status dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Status3"))))
                {
                    error = "Failed to select the Status: " + getData("Status3");
                    return false;
                }

            }
        } catch (Exception e){
            error = "Failed to validate that the concession end date is older than the current date " + e;
            return false;
        }

        //Save and close
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.chevron_Down_ButtonXPath()))
        {
            error = "Failed to wait for the Save & Close button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.chevron_Down_ButtonXPath()))
        {
            error = "Failed to click on the Save & Close button.";
            return false;
        }
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.save_And_Close_XPath()))
        {
            error = "Failed to wait for Save and Close button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.save_And_Close_XPath()))
        {
            error = "Failed to click the Save and Close button.";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait2(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        return true;
    }

    public boolean ValidateOverdueFilter()
    {
        int counter = 1;
        while (SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.verification_And_Additional_Loading_VisibleXPath()))
        {
            pause(1000);
            if (counter >= 100)
            {
                error = "Took to long to wait for page to load";
                return false;
            }
            counter++;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.VerificationAndAdditionalTab()))
        {
            error = "Failed to wait for the Verification And Additional Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.VerificationAndAdditionalTab()))
        {
            error = "Failed to click on the Verification And Additional Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
        {
            error = "Failed to wait for Quality tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityTab_xpath()))
        {
            error = "Failed to click Quality tab.";
            return false;
        }

        counter = 1;
        while (SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concessions_Loading_VisibleXPath()))
        {
            pause(1000);
            if (counter >= 100)
            {
                error = "Took too long to wait for page to load";
                return false;
            }
            counter++;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath()))
        {
            error = "Failed to wait for Quality concession expand button.";
            return false;
        }

//        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.concessions_QualityConcession_Expandxpath()))
//        {
//            error = "Failed to click Quality concession expand button.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concession_Status_XPath()))
        {
            error = "Failed to wait for the Status column";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.non_Editable_Grid_XPath()))
        {
            error = "Failed to wait for the non-editable grid.";
            return false;
        }
        narrator.stepPassed("Successfully validated that the Concession non-editable grid is displayed");
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.search_Button_ConcessionXPath()))
        {
            error = "Failed to click the Search button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.status_Filter_XPath()))
        {
            error = "Failed to wait for the Status filter dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.status_Filter_XPath()))
        {
            error = "Failed to click on the Status filter dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Status3"))))
        {
            error = "Failed to click on the Status option checkbox - " + getData("Status3");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.search_Button_On_Filter_XPath()))
        {
            error = "Failed to click on the Search button on the filter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.table_Loading_XPath()))
        {
            error = "Failed to wait for the Search button on the filter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.concession_Status_XPath()))
        {
            error = "Failed to wait for the status";
            return false;
        }
        try
        {
            String status = SeleniumDriverInstance.retrieveTextByXpath(Isometrics_PageObjects.concession_Status_XPath());

            if (status.equals(getData("Status3")))
            {
                narrator.stepPassed("Successfully validated status: Expected - " + getData("Status3") + " Result: " + status);
            } else
            {
                error = "Status should be " + getData("Status1") + " or " + getData("Status2");
                return false;
            }

            if (status.equals(" "))
            {
                error = "Status cannot be empty";
                return false;
            }
            narrator.stepPassedWithScreenShot("Status needs to be changed" + status);
        } catch (Exception e)
        {
            error = "Failed to retrive text from the table " + e;
            return false;
        }
        return true;
    }
}
