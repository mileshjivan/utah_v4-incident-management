/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 Third party",
        createNewBrowserInstance = false
)

public class FR10_ThirdPartyManufacturer extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR10_ThirdPartyManufacturer() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!compensationClaim()) {
            return narrator.testFailed("Failed fill Is this a compensation claim " + error);
        }

        return narrator.finalizeTest("Completed  Incident and Worker's Injury Details  ");
    }

    public boolean compensationClaim() {
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.IncidentTabXpath(), 2)) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.IncidentTabOpneXpath())) {
                error = "Failed to wait for injury Claim - incident and Worker's Injury Details ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.IncidentTabOpneXpath())) {
                error = "Failed to click injury Claim - incident and Worker's Injury Details ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.anyOptionCheckXpath(getData("What is your injury")))) {
            error = "Failed to wait for injury Claim - Worker's Personal Details - What is your injury / condition and which parts of your body are affected?  " + getData("What is your injury");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anyOptionCheckXpath(getData("What is your injury")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - What is your injury / condition and which parts of your body are affected?  " + getData("What is your injury");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.whatHappenedXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What happened and how were you injured ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.whatHappenedXpath(), getData("what happened"))) {
            error = "Failed to enter - Incident and Worker's Injury - What happened and how were you injured ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.youWereInjuredXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What task/s were you doing when you were injured ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.youWereInjuredXpath(), getData("you were injured"))) {
            error = "Failed to enter - Incident and Worker's Injury - What task/s were you doing when you were injured ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.responsibleEmployerXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.responsibleEmployerXpath(), getData("Responsible employer"))) {
            error = "Failed to enter - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("incident circumstances apply")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - Which of the following incident circumstances apply?";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("incident circumstances apply")))) {
            error = "Failed to click Incident and Worker's Injury Details - Which of the following incident circumstances apply?";
            return false;
        }
        
//           if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.accidentWasReportedXpath(), getData("accident was reported to"))) {
//            error = "Failed to enter - Incident and Worker's Injury - Responsible employer of workplace  ";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.involvedVehiclesXpath(), getData("involved vehicles"))) {
//            error = "Failed to enter - Incident and Worker's Injury - involved vehiclese  ";
//            return false;
//        }
//        
//        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.StateXpath(), getData("State"))) {
//            error = "Failed to enter - Incident and Worker's Injury - State  ";
//            return false;
//        }


        String thirdParty = getData("Third Party Manufacturer");

        if (thirdParty.equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.thirdPartyDropdown())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.thirdPartyDropdown())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }
            pause(2000);
            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(thirdParty))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + thirdParty;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(thirdParty))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + thirdParty;
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.thirdPartyDiplayedXpath())) {
                error = "Failed to display injury Claim form - Incident and Worker's Injury Details - Third party";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.thirdPartyDiplayedXpath(), 2)) {
                error = "Failed to display injury Claim form - Incident and Worker's Injury Details - Third party";
                return false;
            }
            pause(2000);
            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Third party")))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + getData("Third party");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Third party")))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + getData("Third party");
                return false;
            }


            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details and third party displayed.");

        } else if (thirdParty.equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.thirdPartyDropdown())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.thirdPartyDropdown())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(thirdParty))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + thirdParty;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(thirdParty))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + thirdParty;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.thirdPartyDiplayedXpath(), 2)) {
                error = "Display injury Claim form - Incident and Worker's Injury Details - Third party";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details no fields are triggered. ");
        }

        return true;
    }

}
