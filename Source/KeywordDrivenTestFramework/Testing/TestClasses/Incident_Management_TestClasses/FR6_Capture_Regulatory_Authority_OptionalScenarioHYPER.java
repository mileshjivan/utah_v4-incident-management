/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Regulatory Authority Optional Scenario HYPER",
        createNewBrowserInstance = false
)

public class FR6_Capture_Regulatory_Authority_OptionalScenarioHYPER extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_Capture_Regulatory_Authority_OptionalScenarioHYPER()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!regulatoryAuthority())
        {
            return narrator.testFailed("Failed to validate that the document is uploaded to Regulatoy Authority and the record is saved - " + error);
        }

        return narrator.finalizeTest("Successfully validated that the document is uploaded to Regulatoy Authority and the record is saved");
    }

    public boolean regulatoryAuthority()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab()))
        {
            error = "Failed to wait for the safety tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab()))
        {
            error = "Failed to click the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.isReportableToRegulatoryAuthorityCheckboxXpath()))
        {
            error = "Failed to wait for the reportable to regulatory authority checkbox";
            return false;
        }

        if (!SeleniumDriverInstance.checkIfCheckboxIsChecked(VerificationAndAdditionalPageObject.isReportableToRegulatoryAuthorityCheckboxXpath()))
        {
            error = "Failed to check if checkbox is checked";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyRegulatoryAuthorityHeadingXpath()))
        {
            error = "Failed to wait for the Safety Regulatory Authority heading";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyRegulatoryAuthorityHeadingXpath()))
        {
            error = "Failed to wait for the Safety Regulatory Authority heading";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.regulatoryAuthorityAddButtonXpath()))
        {
            error = "Failed to wait for the Add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.regulatoryAuthorityAddButtonXpath()))
        {
            error = "Failed to click the Add button";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 60))
            {
                error = " save took long - reached the time out ";
                return false;
            }
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40))
            {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.regulatoryAuthority()))
        {
            error = "Failed to click regulatory authority dropdown";
            return false;
        }

        SeleniumDriverInstance.pause(9500);
        
       
        if (!SeleniumDriverInstance.clickElementsbyXpath(VerificationAndAdditionalPageObject.regulatoryAuthorityInput1(getData("Regulatory authority")), 1))
        {
            error = "Failed to enter regulatory authority " + getData("Regulatory authority");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.contactPersonXpath(), getData("Contact person")))
        {
            error = "Failed to enter contact person";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.contactNumberXpath(), getData("Contact number")))
        {
            error = "Failed to enter contact number";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.contactEmailXpath(), getData("Contact email")))
        {
            error = "Failed to enter contact email";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.dateXpath(), startDate))
        {
            error = "Failed to enter date ";
            return false;
        }
        String time = new SimpleDateFormat("HH:mm").format(new Date());

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.timeXpath(), time))
        {
            error = "Failed to enter time ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.communicationSummaryXpath(), getData("Communication summary")))
        {
            error = "Failed to enter Communication summary ";
            return false;
        }

//        String pathofImages = System.getProperty("user.dir") + "\\images";
//
//        String path = new File(pathofImages).getAbsolutePath();
//        System.out.println("path " + pathofImages);
//
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic()))
//        {
//            error = "Failed to click the Regulatory Authority  Upload image";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path))
//        {
//            error = "Failed to click image 1";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.selectIdentificationType())
//        {
//            error = "Failed to click Add more images ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic()))
//        {
//            error = "Failed to click the Regulatory Authority  dvt 2 image";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic()))
//        {
//            error = "Failed to click the Regulatory Authority open window button";
//            return false;
//        }
//Add support document
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.fr6link())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.fr6link())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.LinkURL(), "www.inspiredtesting.com/" )){
            error = "Failed to enter 'www.inspiredtesting.com/' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : https://www.inspiredtesting.com/");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.urlTitle(), "Inspired Testing")){
            error = "Failed to enter 'Inspired Testing' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : 'Inspired Testing'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded 'Inspired Testing' document using 'https://www.inspiredtesting.com/' Link.");
        
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.regularyAuthorityFlow()))
        {
            error = "Failed to click Regulatory Authority flow";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.regularyAuthorityFlow()))
        {
            error = "Failed to click Regulatory Authority flow";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.regularyAuthoritySave()))
        {
            error = "Failed to click Regulatory Authority save ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.SaveRegulatoryWait(), 60))
            {
                error = " save took long - reached the time out ";
                return false;
            }
        }

        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.regularyAuthorityeditPhaseActive(), 7))
        {
            error = "Failed to move Regulatory Authority Edit Phase";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully saved Regulatory Authority details and uploaded a document");
        return true;
    }

}
