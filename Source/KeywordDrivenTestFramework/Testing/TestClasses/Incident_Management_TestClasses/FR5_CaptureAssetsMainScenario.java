/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.Keys;

/**
 *
 * @author Syotsi
 */
@KeywordAnnotation(
        Keyword = "FR5 Asset",
        createNewBrowserInstance = false
)

public class FR5_CaptureAssetsMainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String id = FR4_Capture_Equipment_And_Tools.assetId;

    public FR5_CaptureAssetsMainScenario() {

    }

    public TestResult executeTest() {
        if (!assets()) {
            return narrator.testFailed("Failed to Capture Assets- " + error);
        }

        return narrator.finalizeTest("Successfully Captured Assets");
    }

    public boolean assets() {

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.EquipAndTools_Assets_PanelArrowXPath())) {
            error = "Failed to click Assets Panel Arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.EquipAndTools_AssetsAdd_ButtonXPath())) {
            error = "Failed to click Assets Add button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_Process_SelectXPath())) {
            error = "Failed to wait for Assets Process Select Field.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.assets_Process_SelectXPath())) {
            error = "Failed to click Assets Process Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.process_Production_ExpandXPath2())) {
            error = "Failed to wait for Assets Process Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.process_Production_ExpandXPath2())) {
            error = "Failed to click Assets Process Select Field.";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.process_ProductionXPath())) {
//            error = "Failed to wait for Assets Process Select Field.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.process_ProductionXPath())) {
//            error = "Failed to click Assets Process Select Field.";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_RTORequired_SelectXPath())) {
            error = "Failed to wait for Assets Process Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.assets_RTORequired_SelectXPath())) {
            error = "Failed to click Assets Process Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_RTORequired_XPath())) {
            error = "Failed to wait for Assets Process Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.assets_RTORequired_XPath())) {
            error = "Failed to click Assets Process Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_Quantity_XPath())) {
            error = "Failed to wait for Quantity Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.assets_Quantity_XPath(), testData.getData("Quantity"))) {
            error = "Failed to Enter text into Quantity.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_Mandatory_SelectXPath())) {
            error = "Failed to wait for Assets Mandatory Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.assets_Mandatory_SelectXPath())) {
            error = "Failed to click Assets Mandatory Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.process_Mandatory_XPath())) {
            error = "Failed to wait for Assets Mandatory.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.process_Mandatory_XPath())) {
            error = "Failed to click Assets Mandatory.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_Backup_SelectXPath())) {
            error = "Failed to wait for Assets Backup Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.assets_Backup_SelectXPath())) {
            error = "Failed to click Assets Backup Select Field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.process_Backup_XPath())) {
            error = "Failed to wait for Assets Backup.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.process_Backup_XPath())) {
            error = "Failed to click Assets Backup.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_Comments_XPath())) {
            error = "Failed to wait for Comment Field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.assets_Comments_XPath(), testData.getData("Comments"))) {
            error = "Failed to Enter text into Comment field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_ProcessFlow_xpath())) {
            error = "Failed to wait for Process Flow button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.assets_ProcessFlow_xpath())) {
            error = "Failed to click Process Flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured Asset details.");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assets_Save_XPath())) {
            error = "Failed to wait for Assets Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.assets_Save_XPath())) {
            error = "Failed to click Assets Save button.";
            return false;
        }

        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assetId())) {
            error = "Failed to wait for Assets Record number.";
            return false;
        }

        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.assetId()).split("#");
        String AssetRec = Actionid[1];
        narrator.stepPassed("Asset Record Number: " + AssetRec);

        return true;
    }

}
