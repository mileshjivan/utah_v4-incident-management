/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.Keys;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Return To Work Management Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR8_Capture_Return_To_Work_Management_AlternateScenario1 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public FR8_Capture_Return_To_Work_Management_AlternateScenario1()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest()
    {

        if (!ReturnToWorkManagement())
        {
            return narrator.testFailed("Failed to validate that 'Test to be conducted' field is displayed and 'Management who referred the person?' field is not displayed - " + error);
        }

        
        
        return narrator.finalizeTest("Successfully saved an Work Management record");
    }

    public boolean ReturnToWorkManagement()
    {

        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.ReturnToWorkManagementXPath(), 500))
        {
            error = "Failed to scroll to the Return To Work Mangement heading ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReturnToWorkManagementAddButtonXPath()))
        {
            error = "Failed to click the Add button ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.StatusDropdownXPath()))
        {
            error = "Failed to click the Status Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Status"))))
        {
            error = "Failed to select the Reportable to item: " + testData.getData("Status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDropdownXPath()))
        {
            error = "Failed to click on the dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Origin Of Case"))))
        {
            error = "Failed to click the Origin Of Case Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SelectTestToBeConductedXPath(getData("Test To Be Conducted 1"))))
        {
            error = "Failed to click on the dropdown  element - " + getData("Test To Be Conducted 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SelectTestToBeConductedXPath(getData("Test To Be Conducted 2"))))
        {
            error = "Failed to click on the dropdown element - " + getData("Test To Be Conducted 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementsByXpath(InjuredPersonsPageObjects.ManagementWhoReferredThePersonXPath()))
        {
            error = "Failed to validate that 'Management who referred the person' field is not displayed";
            return false;
        }

        narrator.stepPassedWithScreenShot("'Test to be conducted' field is displayed");
        return true;
    }

    

    
}
