/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 Main 8",
        createNewBrowserInstance = false
)

public class FR10_AdditionalDetails8 extends BaseClass {

    String error = "";
    String date;
    
    public FR10_AdditionalDetails8() {
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!compensationClaim()) {
            return narrator.testFailed("Failed fill Did you have any other employment at the time you were injured " + error);
        }

        return narrator.finalizeTest("Completed Did you have any other employment at the time you were injured ");
    }

    public boolean compensationClaim() {
        if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.workerEmploymentDetailsClosedTab(), 2)) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.workerEmploymentDetailsTab())) {
                error = "Failed to wait for Worker's Employment Details  Tab ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workerEmploymentDetailsTab())) {
                error = "Failed to click Worker's Employment Details  Tab ";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.nameOrgPayingInjuredXpath())) {
            error = "Failed to enter - Worker's Employment Details  - Name of organisation paying your wages when you were injured ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.nameOrgPayingInjuredXpath(), getData("Name Org paying injured"))) {
            error = "Failed to enter - Worker's Employment Details  - Name of organisation paying your wages when you were injured ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.workPlaceAddressXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Street address of your usual workplace  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.workPlaceAddressXpath(), getData("Workplace Address"))) {
            error = "Failed to enter - Worker's Employment Details  - Street address of your usual workplace  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.suburbXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Suburb ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.suburbXpath(), getData("Suburb"))) {
            error = "Failed to enter - Worker's Employment Details  - Suburb ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.stateXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - State ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.stateXpath(), getData("Address state"))) {
            error = "Failed to enter - Worker's Employment Details  - State ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.postCodeXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Postcode ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.postCodeXpath(), getData("Postcode"))) {
            error = "Failed to enter - Worker's Employment Details  - Postcode ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.nameofEmployerXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Name of employer contact ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.nameofEmployerXpath(), getData("Name of Employer"))) {
            error = "Failed to enter - Worker's Employment Details  - Name of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.NumOfEmployerXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.NumOfEmployerXpath(), getData("Number of Contact"))) {
            error = "Failed to enter - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.usualOccupationXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.usualOccupationXpath(), getData("Usual occupation"))) {
            error = "Failed to enter - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("apply to you")))) {
            error = "Failed to wait for Worker's Employment Details - Which of the following incident circumstances apply? " + getData("apply to you");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("apply to you")))) {
            error = "Failed to click Worker's Employment Details - Which of the following incident circumstances apply? " + getData("apply to you");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.startedWorkDateXpath())) {
            error = "Failed to wait for - Worker's Employment Details - When did you start working for this employer? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.startedWorkDateXpath(),date)) {
            error = "Failed to enter - Worker's Employment Details - When did you start working for this employer? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyContainsDropDownCheckXpath(getData("Indicate if any apply to you")))) {
            error = "Failed to wait for - Worker's Employment Details - Please indicate if any of the following apply to you  " + getData("Indicate if any apply to you");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyContainsDropDownCheckXpath(getData("Indicate if any apply to you")))) {
            error = "Failed to click - Worker's Employment Details - Please indicate if any of the following apply to you  " + getData("Indicate if any apply to you");
            return false;
        }
        String additionalDetails = getData("Additional details");

        if (additionalDetails.equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.moveToElementByXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to scroll - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to click - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(additionalDetails))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(additionalDetails))) {
                error = "Failed to click Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.addDetailsXpath(), 2)) {
                error = "Failed to diplay - Worker's Employment Details - Additional details ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.addDetailsXpath(),getData("Additional details text"))) {
                error = "Failed to enter - Worker's Employment Details - Additional details";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Did you have any other employment at the time you were injured and additional details field is displayed.");
            
        } else if (additionalDetails.equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to scroll - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employmentTimInjuredDropdownXpath())) {
                error = "Failed to click - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(additionalDetails))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(additionalDetails))) {
                error = "Failed to click Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.addDetailsXpath(), 2)) {
                error = "Diplay - Worker's Employment Details - Additional details ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled did you have any other employment at the time you were injured no fields are triggered. ");
        }
        return true;
    }

}
