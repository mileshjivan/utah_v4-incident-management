/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;

/**
 *
 * @author sjonck
 */
@KeywordAnnotation(
        Keyword = "Upload a supporting document",
        createNewBrowserInstance = false
)

public class FR1_Optional_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Optional_Scenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!loggingAnIncident())
        {
            return narrator.testFailed("Failed to log an incident - " + error);
        }
        if (!UploadDocument())
        {
            return narrator.testFailed("Failed to upload a document - " + error);
        }

        return narrator.finalizeTest("Successfully uploaded a supporting document");
    }

    public boolean loggingAnIncident()
    {

        SikuliDriverUtility sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame ";
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame ";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.environmentalHealth()))
        {
            error = "Failed to wait for Environmental Health Safety";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.environmentalHealth()))
        {
            error = "Failed to click Environmental Health Safety";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath()))
        {
            error = "Failed to click Incident Managment";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 60))
            {
                error = " save took long - reached the time out ";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.AddNewBtn()))
        {
            error = "Failed to wait for add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.AddNewBtn()))
        {
            error = "Failed to click add button";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.incidenttitleXpath(), getData("Incident title")))
        {
            error = "Failed to enter  Incident title";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.IncidentDescription(), getData("Incident Description")))
        {
            error = "Failed to enter  Incident Description";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentOccured()))
        {
            error = "Failed to click Section where incident occurred dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.clickGlobalCompanyXpath()))
        {
            error = "Failed to click global Company dropdown";
            return false;
        }
        String occured = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.occuredText())).getText();

        narrator.stepPassed("Successfully selected an item for the Section where incident occurred dropdown: " + occured);

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.specificLocationXpath(), getData("Specific Location")))
        {
            error = "Failed to enter to Specific Location ";
            return false;
        }

        narrator.stepPassed("Successfully entered the Specific location: " + getData("Specific Location"));
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.ProjectLink()))
        {
            error = "Failed to click Project Link";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Project()))
        {
            error = "Failed to click project dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionXpath(getData("Project"))))
        {
            error = "Failed to click project " + getData("Project");
            return false;
        }
        narrator.stepPassed("Successfully selected Project :" + SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.projectText())).getText());
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.RiskDisciple()))
        {
            error = "Failed to click Impack type dropdown ";
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.incidenttypeAll()))
        {
            error = "Failed to selected all Impack type  ";
            return false;
        }
        String impactType = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.impactTypeText())).getText();
        if (impactType.equalsIgnoreCase(""))
        {
            error = "Failed to selected all Impack type  ";
            return false;
        }
        narrator.stepPassed("Successfully selected Impact type :" + impactType);
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentTypeXpath()))
        {
            error = "Failed to click incident type dropdown";
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentTypeSelectAllXpath()))
        {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();

        startDate = sdf.format(cal.getTime());
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.dateOcXpath(), startDate))
        {
            error = "Failed to enter  Date of occurrence";
            return false;
        }
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.timeXpath(), time))
        {
            error = "Failed to enter  Time of occurrence";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.reportedDateXpath(), startDate))
        {
            error = "Failed to enter  Reported date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.reportedTimeXpath(), time))
        {
            error = "Failed to enter  Reported time";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.shiftXpath()))
        {
            error = "Failed to click shift dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionXpath(getData("Shift"))))
        {
            error = "Failed to click shift ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.partInvolvedXpath()))
        {
            error = "Failed to click External parties involved check box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionCheckXpath(getData("External parties"))))
        {
            error = "Failed to click External parties ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.ActionTaken(), getData("Immediate action taken")))
        {
            error = "Failed to enter  Immediate action taken";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.Reported()))
        {
            error = "Failed to click Reported by check box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.SuperVisor()))
        {
            error = "Failed to click Reported by dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.sighOffActionXpath(getData("Reported by"))))
        {
            error = "Failed to click Reported by projecte User";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.IncidentOwnerXpath()))
        {
            error = "Failed to click to Incident Owner dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.sighOffActionXpath(getData("Incident Owner"))))
        {
            error = "Failed to select Incident Owner";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered the Incident Management details");
        return true;
    }

    public boolean UploadDocument()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for the Supporting Documents tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click the Supporting Documents tab";
            return false;
        }
        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic()))
        {
            error = "Failed to click the Upload image";
            return false;
        }
        String pathofImages = System.getProperty("user.dir") + "\\images";

        String path = new File(pathofImages).getAbsolutePath();
        System.out.println("path " + pathofImages);

        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path))
        {
            error = "Failed to click image 1";
            return false;
        }
        if (!SeleniumDriverInstance.selectIdentificationType())
        {
            error = "Failed to press Enter ";
            return false;
        }
        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvtPic()))
        {
            error = "Failed to click dvt ";
            return false;
        }
        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic()))
        {
            error = "Failed to click open ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.SaveButtonXPath()))
        {
            error = "Failed to wait for the Save Supporting Documents button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.SaveButtonXPath()))
        {
            error = "Failed to click the Save Supporting Documents button";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath(), 3))
        {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved")))
            {
                narrator.stepPassed("Successfully saved the uploaded document: " + text);
            }
        }
        narrator.stepPassedWithScreenShot("Successfully uploaded supporting documents ");
        return true;
    }
}
