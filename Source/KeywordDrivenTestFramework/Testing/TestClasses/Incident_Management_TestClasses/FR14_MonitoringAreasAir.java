/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.EnvironmentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MonitoringAirPageObjects;
import java.util.Set;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR14 Monitoring Air",
        createNewBrowserInstance = false
)

public class FR14_MonitoringAreasAir extends BaseClass {

    String error = "";
    String monitorAreas = "";

    public FR14_MonitoringAreasAir() {

    }

    public TestResult executeTest() {
        if (!monitorAreaAir()) {
            return narrator.testFailed("Failed fill Monitoring areas - " + monitorAreas + " " + error);
        }

        return narrator.finalizeTest("Succesfully open a new window of " + monitorAreas + " ");
    }

    public boolean monitorAreaAir() {

        String businessUnit = getData("Business unit");
        String airQualityType = getData("Air Quality Type");
        String month = getData("Month");
        String year = getData("Year");

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.airMonitorFlowProcessButton())) {
            error = "Failed to wait Air flow process flow ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.airMonitorFlowProcessButton())) {
            error = "Failed to click Air flow process flow ";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.activeAddPhaseXpath(), 3)) {
//            error = "Failed to be in Add phase ";
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.businessUnitDropdownXpath())) {
            error = "Failed to click on business Unit dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(businessUnit))) {
            error = "Failed to click on business Unit " + businessUnit;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.airQualityTypeDropdownXpath())) {
            error = "Failed to click on air quality type dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(airQualityType))) {
            error = "Failed to click on air quality type " + airQualityType;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.monthDropdownXpath())) {
            error = "Failed to click on month dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(month))) {
            error = "Failed to click on month " + month;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.yearDropdownXpath())) {
            error = "Failed to click on year dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(year))) {
            error = "Failed to click on year " + year;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.airSaveButton())) {
            error = "Failed to click on save button ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.activeEditPhaseXpath(), 3)) {
            error = "Failed to wait for air save block layout";
            return false;
        }

        String[] airRecord = SeleniumDriverInstance.retrieveTextByXpath(MonitoringAirPageObjects.getAirMonitorRecord()).split("#");

        narrator.stepPassedWithScreenShot("Successfully saved Air Quality Monitoring " + airRecord[1]);

        Set<String> set = SeleniumDriverInstance.Driver.getWindowHandles();
        String[] win = set.stream().toArray(String[]::new);
        SeleniumDriverInstance.Driver.switchTo().window(win[1]).close();
        SeleniumDriverInstance.Driver.switchTo().window(win[0]);

        SeleniumDriverInstance.switchToDefaultContent();

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame of " + monitorAreas;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.refButton())) {
            error = "Failed to click on refreach button ";
            return false;
        }
        
        pause(5000);
        
        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]), 5000)) {
            error = "Failed to save record in Incident Management edit page " + airRecord[1];
            return false;
        }
        
           if (!SeleniumDriverInstance.scrollToElement(EnvironmentPageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]))) {
            error = "Failed to scrol record in Incident Management edit page " + airRecord[1];
            return false;
        }

        return true;
    }

}
