/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Up load images",
        createNewBrowserInstance = false
)
public class IncidentAddPhotos extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public IncidentAddPhotos() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() throws IOException {

        if (!addPhotos()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed uploading images");
    }

    public boolean addPhotos() throws IOException {

        String pathofImages = System.getProperty("user.dir") + "\\images";

        String path = new File(pathofImages).getAbsolutePath();
        System.out.println("path " + pathofImages);

        if (getData("More images").equalsIgnoreCase("True")) {
//           
            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.addMoreImagesRow1())) {
                error = "Failed to click Add more images 1";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.addMoreImagesRow2())) {
                error = "Failed to click Add more images 2";
                return false;
            }

            Runtime.getRuntime().exec("C:/Users/rnagel/Documents/isometrix/Scripts/uploadImage.exe");
            
            pause(3000);
            //default content
            if (!SeleniumDriverInstance.switchToDefaultContent()) {
                    error = "Failed to switch to default content ";
                    return false;
            }
            //iframe
            if (!SeleniumDriverInstance.switchToFrameByXpath(IncidentPageObjects.iframeXpath())) {
                    error = "Failed to switch to iframe ";
                    return false;
            }

//            if (!SeleniumDriverInstance.selectIdentificationType()) {
//                error = "Failed to click Add more images ";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvtPic())) {
//                error = "Failed to click dvt ";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic())) {
//                error = "Failed to click open ";
//                return false;
//            }
//
//            //image 2
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.image2Pic())) {
//                error = "Failed to click image 2";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvt2Pic())) {
//                error = "Failed to click dvt ";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic())) {
//                error = "Failed to click open ";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Add images ");
//            //image 3
//            if (!SeleniumDriverInstance.scrollToElement(IncidentPageObjects.scrollXpath())) {
//                error = "Failed to scroll to Incident Owner";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.image3Pic())) {
//                error = "Failed to click image 2";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvt3Pic())) {
//                error = "Failed to click dvt ";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic())) {
//                error = "Failed to click open ";
//                return false;
//            }
//
            narrator.stepPassedWithScreenShot("Add image 3 and entered incident owner");

            SeleniumDriverInstance.pause(2000);

            narrator.stepPassedWithScreenShot("Added 3 more images");

        } else {
            
            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.addMoreImagesRow1())) {
                error = "Failed to click Add more images 1";
                return false;
            }
            
            if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.image())) {
                error = "Failed to click image";
                return false;
            }
            
            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.image())) {
                error = "Failed to click image";
                return false;
            }
            
            Runtime.getRuntime().exec("C:/Users/rnagel/Documents/isometrix/Scripts/uploadImage.exe");  
            
            pause(3000);
            //default content
            if (!SeleniumDriverInstance.switchToDefaultContent()) {
                    error = "Failed to switch to default content ";
                    return false;
            }
            //iframe
            if (!SeleniumDriverInstance.switchToFrameByXpath(IncidentPageObjects.iframeXpath())) {
                    error = "Failed to switch to iframe ";
                    return false;
            }

//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.imagePic1())) {
//                error = "Failed to click image 1";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path)) {
//                error = "Failed to click image 1";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.selectIdentificationType()) {
//                error = "Failed to click Add more images ";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvtPic())) {
//                error = "Failed to click dvt ";
//                return false;
//            }
//
//            if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic())) {
//                error = "Failed to click open ";
//                return false;
//            }
            SeleniumDriverInstance.pause(1500);
            narrator.stepPassedWithScreenShot("Successfully added one image ");

        }

        return true;
    }
}
