/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Main scenario",
        createNewBrowserInstance = false
)
public class FR1_CaptureIncident_UC_INC_01_01_MainScenario extends BaseClass {

    String error = "";

    public FR1_CaptureIncident_UC_INC_01_01_MainScenario() {

    }

    public TestResult executeTest() {

        if (!loggingAnIncident()) {
            return narrator.testFailed("Failed to logging An Incident due - " + error);
        }

        return narrator.finalizeTest("Successfully logging An Incident");
    }

    public boolean loggingAnIncident() {
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.environmentalHealth())) {
            error = "Failed to wait for Environmental Health Safety";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.environmentalHealth())) {
            error = "Failed to click Environmental Health Safety";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath())) {
            error = "Failed to click Incident Managment";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.IncidentManagemetLoadingXpath(), 5)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsIncidentPageObjects.IncidentManagemetLoadingDoneXpath(), 100)) {
                error = " save took long - reached the time out 1";
                return false;
            }
            System.out.println(" \n Found");
        } else {
            System.out.println("\n not Found");
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.AddNewBtn(), 40)) {
            error = "Failed to wait for add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.AddNewBtn())) {
            error = "Failed to click add button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.incidentReportTab(), 2)) {
            error = "Failed to validate - 1.Incident Report tab  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.supportDocumentTab(), 2)) {
            error = "Failed to validate - Supporting Documents tab  ";
            return false;
        }

        narrator.stepPassedWithScreenShot(" Sucessfully validated 1.Incident Report and Supporting Documents tab");

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.incidenttitleXpath(), getData("Incident title"))) {
            error = "Failed to enter  Incident title";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.IncidentDescription(), getData("Incident Description"))) {
            error = "Failed to enter  Incident Description";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.IncidentOccured())) {
            error = "Failed to wait for Section where incident occurred dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentOccured())) {
            error = "Failed to click Section where incident occurred dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.clickGlobalCompanyXpath(), 300000)) {
            error = "Failed to wait for global Company dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.clickGlobalCompanyXpath())) {
            error = "Failed to click global Company dropdown";
            return false;
        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.southAfricaXpath())) {
//            error = "Failed to click South Africa dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.victorySiteXpath())) {
//            error = "Failed to click Victory Site ";
//            return false;
//        }

        String occured = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.occuredText())).getText();

        narrator.stepPassed("Successfully selected Section where incident occurred :" + occured);

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.specificLocationXpath(), getData("Specific Location"))) {
            error = "Failed to enter to Specific Location ";
            return false;
        }

        narrator.stepPassed("Successfully eneterd Specific location :" + getData("Specific Location"));

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.RiskDisciple())) {
            error = "Failed to wait for click Impack type dropdown ";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.RiskDisciple())) {
            error = "Failed to click Impack type dropdown ";
            return false;
        }
        SeleniumDriverInstance.pause(1500);

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.incidenttypeAll(), 300000)) {
            error = "Failed to wait for selected all Impack type  ";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.incidenttypeAll())) {
            error = "Failed to selected all Impack type  ";
            return false;
        }
        String impactType = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.impactTypeText())).getText();
        if (impactType.equalsIgnoreCase("")) {
            error = "Failed to selected all Impack type  ";
            return false;
        }
        narrator.stepPassed("Successfully selected Impact type :" + impactType);

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentTypeXpath())) {
            error = "Failed to click incident type dropdown";
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentTypeSelectAllXpath())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();

        startDate = sdf.format(cal.getTime());
        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.dateOcXpath(), startDate)) {
            error = "Failed to enter  Date of occurrence";
            return false;
        }

        String time = new SimpleDateFormat("HH:mm").format(new Date());

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.timeXpath(), time)) {
            error = "Failed to enter  Time of occurrence";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.reportedDateXpath(), startDate)) {
            error = "Failed to enter  Reported date";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.reportedTimeXpath(), time)) {
            error = "Failed to enter  Reported time";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.shiftXpath())) {
            error = "Failed to click shift dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionXpath(getData("Shift")))) {
            error = "Failed to click shift ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.ActionTaken(), getData("Immediate action taken"))) {
            error = "Failed to enter  Immediate action taken";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(IncidentPageObjects.IncidentOwnerXpath())) {
            error = "Failed to scroll to Incident Owner dropdown";
            return false;
        }
        
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.IncidentOwnerXpath())) {
            error = "Failed to wait for Incident Owner dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.IncidentOwnerXpath())) {
            error = "Failed to click Incident Owner dropdown";
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.incident_Select(getData("Incident Owner")))) {
            error = "Failed to wait for Incident Owner";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incident_Select(getData("Incident Owner")))) {
            error = "Failed to select Incident Owner " + getData("Incident Owner");
            return false;
        }

        narrator.stepPassed("Successfully selected incident Type  :" + SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.incidentTypeTextXpath())).getText());

        return true;
    }
}
