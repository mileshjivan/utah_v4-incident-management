/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Verification and Additional detail",
        createNewBrowserInstance = false
)
public class VerificationAndAdditional extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public VerificationAndAdditional() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        
       

        if (!superVisor()) {
            return narrator.testFailed("Failed to enter Supervisor due - " + error);
        }

        if (!safety()) {
            return narrator.testFailed("Failed to - " + error);
        }

        if (!addIncidentManagement()) {
            return narrator.testFailed("Failed to - " + error);
        }

        if (!personsInvolved()) {
            return narrator.testFailed("Failed to enter persons Involved due - " + error);
        }

        if (!witnessStatement()) {
            return narrator.testFailed("Failed to enter witness statement due - " + error);
        }

        if (!equipment()) {
            return narrator.testFailed("Failed to enter Equipment and Tools due - " + error);
        }

        return narrator.finalizeTest("Successfully logging An Incident");
    }

    public boolean superVisor() {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.Safety())) {
            error = "Failed to click Safety supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Safety supervisor")))) {
            error = "Failed to select Safety supervisor ";
            return false;
        }
        narrator.stepPassedWithScreenShot("successfully selected Safety supervisor :" + getData("Safety supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentXpath())) {
            error = "Failed to click Environment button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentDropDownXpath())) {
            error = "Failed to click Environment dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Environmental supervisor")))) {
            error = "Failed to select Environment supervisor ";
            return false;
        }
        narrator.stepPassedWithScreenShot("successfully selected  Environment supervisor :" + getData("Environmental supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualitytXpath())) {
            error = "Failed to click Quality button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualityDropDownXpath())) {
            error = "Failed to click Quality dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Quality supervisor")))) {
            error = "Failed to select Quality supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Quality supervisor :" + getData("Quality supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyXpath())) {
            error = "Failed to click railway Safety button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyDropDownXpath())) {
            error = "Failed to click railway Safety dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Railway safety supervisor")))) {
            error = "Failed to select railway Safety supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Railway safety supervisor :" + getData("Railway safety supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusXpath())) {
            error = "Failed to click Social Sustainability button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusDropDownXpath())) {
            error = "Failed to click Social supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Social supervisor")))) {
            error = "Failed to select supervisor supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Social supervisor :" + getData("Social supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneXpath())) {
            error = "Failed to click Occupational hygiene button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneDropDownXpath())) {
            error = "Failed to Occupational hygiene supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Occupational hygiene supervisor")))) {
            error = "Failed to select Occupational hygiene supervisor ";
            return false;
        }

        narrator.stepPassedWithScreenShot("successfully selected Occupational hygiene supervisor :" + getData("Occupational hygiene supervisor"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceXpath())) {
            error = "Failed to click Compliance button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceDropDownXpath())) {
            error = "Failed to Compliance supervisor dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Compliance supervisor")))) {
            error = "Failed to select Compliance supervisor ";
            return false;
        }
        narrator.stepPassedWithScreenShot("successfully selected Compliance supervisor :" + getData("Compliance supervisor"));

        return true;
    }

    public boolean personsInvolved() {

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.personInvolvedXpath())) {
            error = "Failed to click persons Involved tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.personInvolvedAddXpath())) {
            error = "Failed to click persons Involved add button";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.personInvolvedNameXpath(), getData("Involved Full Name"))) {
            error = "Failed to enter persons Involved Full name";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.personInvolvedDecXpath(), getData("Involved Description"))) {
            error = "Failed to enter persons Involved Involved Description";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.personInvolvedXpath())) {
            error = "Failed to click persons Involved close tab";
            return false;
        }

        return true;
    }

    public boolean witnessStatement() {

//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.saveVerAndAddXpath())) {
//            error = "Failed to click save button 2.Verification and Additional Detail"
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to wait for witness Statements button";
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to click witness Statements button";
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsAddXpath())) {
            error = "Failed to click witness Statements add button";
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessNameXpath(), getData("witness Name"))) {
            error = "Failed to enter witness name ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSurnameXpath(), getData("witness Surname"))) {
            error = "Failed to enter witness Surname";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSummaryXpath(), getData("witness Sunnary"))) {
            error = "Failed to enter witness Sunnary";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic())) {
            error = "Failed to click the witness Upload image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic())) {
            error = "Failed to click the witness dvt 2 image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic())) {
            error = "Failed to click the witness open window button";
            return false;
        }

        narrator.stepPassedWithScreenShot(
                "Successfully entered witness statement " + getData("witness Name") + ", " + getData("witness Surname") + ", " + getData("witness Sunnary"));

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessSaveCloseButtonXpath())) {
            error = "Failed to click witness save and close button";
                        return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableWitness(), 20)) {

            error = "Failed to click save Witness details";
            return false;
        }

        narrator.stepPassedWithScreenShot("Ssaved Witness details");
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to wait for witness Statements close tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to click witness Statements close tab";
            return false;
        }

        return true;
    }

    public boolean equipment() {

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath())) {
            error = "Failed to wait for Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath())) {
            error = "Failed to click Equipment and Tools tab ";
                        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentAddXpath())) {
            error = "Failed to click Equipment and Tools Add button";
                        return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.assetTypeDropDownXpath())) {
            error = "Failed to click Equipment and Tools - Asset Type ";
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(getData("Asset Type")))) {
            error = "Failed to click Equipment and Tools Asset Type option - " + getData("Asset Type");
        }

        String[] asset = getData("Asset").split(",");

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.assetDropDownXpath())) {
            error = "Failed to click asset dropdown";
            return false;
        }

        for (int k = 0; k < asset.length; k++) {

            if (k + 1 == asset.length) {
                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionXpath(asset[k]))) {
                    error = "Failed to click asset - " + asset[k];
                    return false;
                }
                narrator.stepPassed("SuccessFully asset : " + asset[k]);
            } else {

                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveDropdownXpath(asset[k]))) {
                    error = "Failed to click asset - " + asset[k];
                    return false;
                }
                narrator.stepPassed("Successfully selected asset type : " + asset[k]);
            }

        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.equipmentDescXpath(), getData("Equipment description"))) {
            error = "Failed to enter Equipment and Tools - Equipment description - " + getData("Equipment description");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.descrOfDamageXpath(), getData("Description of damage"))) {
            error = "Failed to enter Equipment and Tools - Description of damage - " + getData("Description of damage");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.damageCostXpath(), getData("Damage cost"))) {
            error = "Failed to enter Equipment and Tools - Damage cost - " + getData("Damage cost");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.equipmentDescXpath())) {
            error = "Failed to click Equipment and Tools - Asset Type ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.operatorEmTypeDropdownpath())) {
            error = "Failed to click Equipment and Tools - Operator employment type ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator employment type")))) {
            error = "Failed to select Equipment and Tools - Operator employment type - " + getData("Operator employment type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.operatorDropdownpath())) {
            error = "Failed to click Equipment and Tools - Operator  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator")))) {
            error = "Failed to select Equipment and Tools - Operator  - " + getData("Operator");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.motorVehiTabXpath())) {
            error = "Failed to click Equipment and Tools - Motor Vehicle Accident Information tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.timeOfDayDropdownpath())) {
            error = "Failed to click Equipment and Tools - time Of Day dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Time of Day")))) {
            error = "Failed to select Equipment and Tools - time Of Day - " + getData("Time of Day");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.weatherCondDropdownpath())) {
            error = "Failed to click Equipment and Tools - Weather Conditions dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Weather Conditions")))) {
            error = "Failed to select Equipment and Tools - Weather Conditions - " + getData("Weather Conditions");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.roadConditionsCondDropdownpath())) {
            error = "Failed to click Equipment and Tools - Road Conditions dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Road Conditions")))) {
            error = "Failed to select Equipment and Tools - Road Conditions - " + getData("Road Conditions");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.roadTypeDropdownpath())) {
            error = "Failed to click Equipment and Tools - Road Type dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Road Type")))) {
            error = "Failed to select Equipment and Tools - Road Type - " + getData("Road Type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.InsuranceCoverDropdownpath())) {
            error = "Failed to click Equipment and Tools - Insurance covered by Company dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Insurance covered by Company")))) {
            error = "Failed to select Equipment and Tools - Insurance covered by Company- " + getData("Insurance covered by Company");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.deductibleAmountXpath(), getData("Deductible Amount"))) {
            error = "Failed to enter Equipment and Tools - Deductible Amount - " + getData("Deductible Amount");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.saveEquipToolsXpath())) {
            error = "Failed to click  Equipment and Tools  save button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully enter Equipment and Tools details");

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.closeEquipToolXpath())) {
            error = "Failed to click  Equipment and Tools  close button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByJavascript(VerificationAndAdditionalPageObject.closeEquipToolXpath())) {
            error = "Failed to click  Equipment and Tools  close button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableEquipAndTool(), 20)) {
            error = "Failed to click save Equipment and Tools details";
            return false;
        }

        narrator.stepPassedWithScreenShot("Ssaved Equipment and Tools details");

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableEquipAndTool(), 20)) {
            error = "Failed to save Equipment and Tools details";
            return false;
        }

        narrator.stepPassedWithScreenShot("Ssaved Equipment and Tools details");
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath())) {
            error = "Failed to wait for Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath())) {
            error = "Failed to click Equipment and Tools tab ";
            return false;
        }

        return true;
    }

    public boolean safety() {

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab())) {

            error = "Failed to wait for the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to click the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.isReportableToRegulatoryAuthorityCheckboxXpath())) {
            error = "Failed to wait for the reportable to regulatory authority checkbox";
            return false;
        }

        if (!SeleniumDriverInstance.checkIfCheckboxIsChecked(VerificationAndAdditionalPageObject.isReportableToRegulatoryAuthorityCheckboxXpath())) {
            error = "Failed to check if checkbox is checked";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyRegulatoryAuthorityHeadingXpath())) {
            error = "Failed to wait for the Safety Regulatory Authority heading";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyRegulatoryAuthorityHeadingXpath())) {
            error = "Failed to wait for the Safety Regulatory Authority heading";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.regulatoryAuthorityAddButtonXpath())) {
            error = "Failed to wait for the Add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.regulatoryAuthorityAddButtonXpath())) {
            error = "Failed to click the Add button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 60)) {
                error = " save took long - reached the time out ";
                return false;
            }
        }

        return true;
    }

    public boolean addIncidentManagement() {

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 60)) {
                error = " save took long - reached the time out ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.regulatoryAuthority())) {
            error = "Failed to click regulatory authority dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.regulatoryAuthorityInput(getData("Regulatory authority")))) {
            error = "Failed to enter regulatory authority " + getData("Regulatory authority");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.contactPersonXpath(), getData("Contact person"))) {
            error = "Failed to enter contact person";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.contactNumberXpath(), getData("Contact number"))) {
            error = "Failed to enter contact number";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.contactEmailXpath(), getData("Contact email"))) {
            error = "Failed to enter contact email";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.dateXpath(), startDate)) {
            error = "Failed to enter date ";
            return false;
        }
        String time = new SimpleDateFormat("HH:mm").format(new Date());

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.timeXpath(), time)) {
            error = "Failed to enter time ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.communicationSummaryXpath(), getData("Communication summary"))) {
            error = "Failed to enter Communication summary ";
            return false;
        }

        String pathofImages = System.getProperty("user.dir") + "\\images";

        String path = new File(pathofImages).getAbsolutePath();
        System.out.println("path " + pathofImages);

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic())) {
            error = "Failed to click the Regulatory Authority  Upload image";
            return false;
        }

        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path)) {
            error = "Failed to click image 1";
            return false;
        }

        if (!SeleniumDriverInstance.selectIdentificationType()) {
                error = "Failed to click Add more images ";
                return false;
            }
        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic())) {
            error = "Failed to click the Regulatory Authority  dvt 2 image";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic())) {
            error = "Failed to click the Regulatory Authority open window button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully enter Regulatory Authority details");

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveAndCloseXpath())) {
            error = "Failed to click Regulatory Authority  save and close";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.saveAndCloseXpath())) {
            error = "Failed to click Regulatory Authority  save and close";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to wait for the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to click the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitRegulatoryTable())) {
            error = "Failed to save Regulatory Authority";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully saved Regulatory Authority details");

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitRegulatoryTable())) {
            error = "Failed to save Regulatory Authority";
            return false;
        }

        return true;
    }

}
