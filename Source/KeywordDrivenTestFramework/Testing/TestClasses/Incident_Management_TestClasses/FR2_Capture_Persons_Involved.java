/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *g
 * @author sjonck
 */
@KeywordAnnotation(
        Keyword = "Persons Involved",
        createNewBrowserInstance = false
)

public class FR2_Capture_Persons_Involved extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Persons_Involved()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!ValidatePanelsAreDisplayed())
        {
            return narrator.testFailed("Failed to validate that the Persons Involved, Witness Statement and Equipment Involved panels are displayed - " + error);
        }
        if (!PersonsInvolvedEditableGrid())
        {
            return narrator.testFailed("Failed to validate that the Persons Involved editable grid is displayed - " + error);
        }
        if (!PersonsInvolved())
        {
            return narrator.testFailed("Failed to add a person involved - " + error);
        }
        return narrator.finalizeTest("Successfully captured the details for persons involved");
    }

    public boolean ValidatePanelsAreDisplayed()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.PersonsInvolvedXpath()))
        {
            error = " Failed to validate that the Persons Involved panel is dispayed";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.PersonsInvolvedXpath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Persons Involved")))
            {
                narrator.stepPassed("Successfully validated that the Persons Involved panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.WitnessStatementsXpath()))
        {
            error = "Failed to validate that the Witness Statement panel is dispayed";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.WitnessStatementsXpath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Witness Statements")))
            {
                narrator.stepPassed("Successfully validated that the Witness Statement panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.EquipmentInvolvedTabXpath()))
        {
            error = "Failed to validate that the Equipment Involved panel is dispayed";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.EquipmentInvolvedTabXpath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Equipment Involved")))
            {
                narrator.stepPassed("Successfully validated that the Equipment Involved panel is displayed: " + text);
            }
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Persons Involved, Witness Statement and Equipment Involved panels are displayed");
        return true;
    }

    public boolean PersonsInvolvedEditableGrid()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.PersonsInvolvedXpath()))
        {
            error = "Failed to wait for the Persons Involved panel ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.PersonsInvolvedXpath()))
        {
            error = "Failed to click the Persons Involved panel ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.ValidateGridIsEditableXPath()))
        {
            error = "Failed to validate that the editable grid is displayed";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Persons Involved editable grid is displayed");
        return true;
    }

    public boolean PersonsInvolved()
    {
      
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.personInvolvedAddXpath()))
        {
            error = "Failed to click persons Involved add button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.ANewRowIsDisplayedXPath()))
        {
            error = "Failed to validate that a new row is displayed";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated that a new is displayed in the Add Phase");
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.personInvolvedNameXpath(), getData("Involved Full Name")))
        {
            error = "Failed to enter persons Involved Full name";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.personInvolvedDecXpath(), getData("Involved Description")))
        {
            error = "Failed to enter persons Involved Involved Description";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.SaveXPath()))
        {
            error = "Failed to click the save button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait2(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath()))
        {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved")))
            {
                narrator.stepPassed("Successfully saved the Persons Involved record: " + text);
            }
        }

        return true;
    }
}
