/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 support any children under",
        createNewBrowserInstance = false
)

public class FR10_SupportAnyChildren extends BaseClass {

    String error = "";
    String date;
    public FR10_SupportAnyChildren() {
         date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!returnToWork()) {
            return narrator.testFailed("Failed fill support any children under " + error);
        }

        return narrator.finalizeTest("Completed support any children under ");
    }

    public boolean returnToWork() {

           if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.workersTabCheckOpenXpath(), 2)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workersTabXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details  ";
                return false;
            }
        }

        String partner = getData("Support Any Children");

        if (partner.equalsIgnoreCase("Yes")) {

            if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.fullTimeStudentsXpath())) {
                error = "Failed to scroll injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.fullTimeStudentsXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }
             
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(partner))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner?  " + partner;
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.pleaseProvideTheDateOfBirthXpath())) {
                error = "Failed to wait for field on injury Claim - Worker's Personal Details - Please provide the date of birth  " + partner;
                return false;
            }
            
              if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.pleaseProvideTheDateOfBirthXpath(),date)) {
                error = "Failed to enter for field on injury Claim - Worker's Personal Details - Please provide the date of birth  " + partner;
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Do you support any children under the age of 18 or full-time students? Fields are displayed.");

        } else if (partner.equalsIgnoreCase("No")) {

             if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.fullTimeStudentsXpath())) {
                error = "Failed to scroll injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }

             if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.fullTimeStudentsXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }
           
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(partner))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner?  " + partner;
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled o Do you support any children under the age of 18 or full-time students? no fields are triggered.");
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.returnWorkDetailsTab())) {
            error = "Failed to click Treatment and Return To Work Details close Tab  ";
            return false;
        }

        return true;
    }

}
