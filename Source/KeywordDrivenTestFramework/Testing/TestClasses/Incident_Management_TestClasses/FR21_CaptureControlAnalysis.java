/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.EnvironmentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MonitoringAirPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Control Analysis",
        createNewBrowserInstance = false
)

public class FR21_CaptureControlAnalysis extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    String path;

    public FR21_CaptureControlAnalysis() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();

    }

    public TestResult executeTest() {
        if (!captureControlAnalysis()) {
            return narrator.testFailed("Failed to capture control analysis details - " + error);
        }

        return narrator.finalizeTest("Successfully ontrol Actions record is saved.");
    }

    public boolean captureControlAnalysis() {

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to wait for the investigation details tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to click on the investigation details tab ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.InvestigationDueDateXPath(), date)) {
            error = "Failed to enter investigation due date";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.investigationScope(), getData("Investigation Scope"))) {
            error = "Failed to enter investigation scope";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to wait for the investigation details sub tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.ProcessActivityDropdownXPath())) {
            error = "Failed to click on the Process/Activity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(getData("Process Activity 1")))) {
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Process Activity 2")))) {
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskSourceDropdownXPath())) {
            error = "Failed to click on the Risk Source dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(Isometrics_PageObjects.MechanicalSelectionXPath())) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.MechanicalSelectionXPath())) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2")))) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2")))) {
            error = "Failed to click on the Risk Source option checkbox - " + getData("Risk Source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskDropdownXPath1())) {
            error = "Failed to click Risk Source close dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskDropdownXPath())) {
            error = "Failed to click on the Risk dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk")))) {
            error = "Failed to click on the Risk option checkbox - " + getData("Risk");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationTypeDropdownXPath1())) {
            error = "Failed to close on Risk dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationTypeDropdownXPath())) {
            error = "Failed to click on the Investigation Type dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Investigation Type")))) {
            error = "Failed to click on the Investigation Type option - " + getData("Investigation Type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalisisTab())) {
            error = "Failed to click on - Critical controls linked to this Process/Activity - panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalisisAddButon())) {
            error = "Failed to click on - Critical controls linked to this Process/Activity - add button ";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 3)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.controlFlowProcess(), 5000)) {
            error = "Failed to wait on - Control Analysis - process button ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlFlowProcess())) {
            error = "Failed to click on - Control Analysis - process button ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.activeAddPhaseXpath(), 7)) {
            error = "Failed to wait on - Control Analysis - add phase ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlDropdown())) {
            error = "Failed to click on - Control Analysis - Control Dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropdwonOptionXpath(getData("Control")))) {
            error = "Failed to selet on - Control Analysis - Control ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.controlDescription(), getData("Control description"))) {
            error = "Failed to enter - Control Analysis - Control description ";
            return false;
        }

        String controlAnalysis = getData("Control analysis");

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalysisDropdown())) {
            error = "Failed to click on - Control Analysis Dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropdwonOptionXpath(controlAnalysis))) {
            error = "Failed to selet on - Control Analysis ";
            return false;
        }

        if (controlAnalysis.equalsIgnoreCase("Effective")) {

            if (SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.relatedLinkXpath(), 3)) {
                error = "Failed - Related Link panel is displayed  ";
                return false;
            }

        } else {

            if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.relatedLinkXpath(), 3)) {
                error = "Failed - Related Link panel was displayed  ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.causeSelectionDropdown())) {
                error = "Failed to click on - Control Analysis - Cause selection - dropdwon ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropdwonOptionXpath(getData("Cause selection")))) {
                error = "Failed to selet on - Control Analysis - Cause selection ";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.description(), getData("Description"))) {
                error = "Failed to enetr - Control Analysis - Description ";
                return false;
            }

        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlFollowProDropdown())) {
            error = "Failed to click on - Control Analysis - Did the control follow procedure - dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropdwonOptionXpath(getData("Did the control follow procedure")))) {
            error = "Failed to selet on - Control Analysis -Did the control follow procedure ";
            return false;
        }

        //Set True in the testpack to add Support documents
        String supportingDocuments = getData("Supporting Documents");

        if (supportingDocuments.equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.supportDocumentsPanel())) {
                error = "Failed to click on - Control Analysis  - Support documents panel ";
                return false;
            }

            if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic())) {
                error = "Failed to click the witness Upload image";
                return false;
            }

            if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path)) {
                error = "Failed to click image 1";
                return false;
            }

            if (!SeleniumDriverInstance.selectIdentificationType()) {
                error = "Failed to click Add more images ";
                return false;
            }

            if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic())) {
                error = "Failed to click the witness dvt 2 image";
                return false;
            }

            if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic())) {
                error = "Failed to click the witness open window button";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalysisSaveButoon())) {
            error = "Failed to click on - Control Analysis - Save - Button ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.activeEditPhaseXpath(), 9)) {
            error = "Failed to save - Control Analysis";
            return false;
        }

        String[] id = SeleniumDriverInstance.retrieveTextByXpath(Isometrics_PageObjects.controlAnalysisId()).split("#");

        narrator.stepPassed("Record Number: " + id[1]);
        narrator.stepPassedWithScreenShot("Successfulled saved control Analysis");

        if (!controlAnalysis.equalsIgnoreCase("Effective")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalysisActionTab())) {
                error = "Failed to click on - Control Analysis - Actions panel ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalysisActionAddButton())) {
                error = "Failed to click on - Control Analysis Actions - add button ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.controlAnalysisActionProcessButton(), 15)) {
                error = "Failed to wait on - Control Analysis Actions win ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalysisActionProcessButton())) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - process flow button";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.actionDescription(), getData("Action description"))) {
                error = "Failed to enter on - Control Analysis  - Control Analysis Actions - Action description ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.departmentResponsibleDropdown())) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Department Responsible dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.anyDropdwonOptionXpath(getData("Department responsible")))) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Department Responsible ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.responsiblePersonDropdown())) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Responsible person dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.responsiblePerson(getData("Responsible person")))) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Responsible person ";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.actionDueDate(), date)) {
                error = "Failed to enter on - Control Analysis  - Control Analysis Actions - Action due date ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.controlAnalysisActionSaveButton())) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Save";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.activeToBeInitiatedXpath(), 20)) {
                error = "Failed to save on - Control Analysis  - Control Analysis Actions record";
                return false;
            }

            String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(Isometrics_PageObjects.controlAnalysisActionId()).split("#");

            narrator.stepPassed("Control Analysis Actions  Record Number: " + Actionid[1]);
            narrator.stepPassedWithScreenShot("Successfulled saved Control Analysis Actions ");
        }

//        }
        return true;
    }

}
