/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.sikuli.basics.Debug.error;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Log Injured Person",
        createNewBrowserInstance = false
)
public class IncidentInjuredPersons extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public IncidentInjuredPersons()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest()
    {
        if (!injuuredPersons())
        {
            return narrator.testFailed("Failed to enter Injured Persons details - " + error);
        }
        return narrator.finalizeTest("Successfully saved an Work Management record");
    }

    public boolean injuuredPersons()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.addDetailsXpath()))
        {
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }    
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.addDetailsXpath()))
        {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 5))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400))
            {
                error = "Website too long to load wait reached the time out";
                return false;
            }
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab()))
        {
            error = "Failed to wait for the safety tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab()))
        {
            error = "Failed to click the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.addInjuredPersonsButtonXPath()))
        {
            error = "Failed to click the Add button ";
            return false;
        }
//        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait2(), 5000))
//        {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//
//        }
//        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 5000))
//        {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 5000))
//            {
//                error = "Website too long to load wait reached the time out";
//                return false;
//            }
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.employeeTypeDropdownXPath()))
        {
            error = "Failed to wait for the employee type dropdown ";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.employeeTypeDropdownXPath()))
        {
            error = "Failed to click the employee type dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(getData("Employee"))))
        {
            error = "Failed to click the employee type element: " + testData.getData("Employee");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.fullNameDropdownXPath()))
        {
            error = "Failed to click the full name dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(testData.getData("Full Name"))))
        {
            error = "Failed to click the full name element: " + testData.getData("Full Name");
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuryOrIlnessDropdownXPath()))
        {
            error = "Failed to click the injury or illness dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(testData.getData("Yes"))))
        {
            error = "Failed to click the injury or illness element: " + testData.getData("Yes");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.drugAlcoholTestRequiredDropdownXPath()))
        {
            error = "Failed to click the drugs and alcohol test dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Yes"))))
        {
            error = "Failed to click the drugs and alcohol test element: " + testData.getData("Yes");
            return false;
        }

        return true;
    }

}
