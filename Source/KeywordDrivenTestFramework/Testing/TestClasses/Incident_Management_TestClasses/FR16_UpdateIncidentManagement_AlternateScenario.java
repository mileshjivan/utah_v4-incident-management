/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author sjonck
 */
@KeywordAnnotation(
        Keyword = "Update Incident Management Alternate Scenario",
        createNewBrowserInstance = false
)
public class FR16_UpdateIncidentManagement_AlternateScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR16_UpdateIncidentManagement_AlternateScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!UpdateIncidentManagement()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Updating Incident Management - Alternate Scenario");
    }

    public boolean UpdateIncidentManagement() {
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Tab_ButtonXpath())) {
            error = "Failed to wait for Risk And Impact Assessment tab button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Tab_ButtonXpath())) {
            error = "Failed to click Risk And Impact Assessment tab button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath())) {
            error = "Failed to wait for I declare that I am authorised to ... check box.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath())) {
            error = "Failed to click I declare that I am authorised to ... check box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationComment_InputXpath())) {
            error = "Failed to wait for Declaration Comment text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_DeclarationComment_InputXpath(), testData.getData("DeclarationComments"))) {
            error = "Failed to enter text into Declaration Comment text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath())) {
            error = "Failed to wait for Lead inverstigator Select dropdawon list.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath())) {
            error = "Failed to click for Lead inverstigator Select dropdawon list.";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_TypeSearchxpath())) {
//            error = "Failed to wait for EnterText into Lead inverstigator type search.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_TypeSearchxpath(), testData.getData("LeadInvestigator"))) {
//            error = "Failed to EnterText into Lead inverstigator type search.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_xpath(testData.getData("LeadInvestigator")))) {
            error = "Failed to wait for Lead inverstigator: " + testData.getData("LeadInvestigator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_LeadInvestigator_xpath(testData.getData("LeadInvestigator")))) {
            error = "Failed to click Lead inverstigator: " + testData.getData("LeadInvestigator");
            return false;
        }

        pause(500);
        narrator.stepPassedWithScreenShot("Successfully Captured Risk and Impact Assessment.");
        narrator.stepPassed("Declaration comment: " + testData.getData("DeclarationComments"));
        narrator.stepPassed("lead investigator: " + testData.getData("LeadInvestigator"));

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_SubmitStep2_ButtonXpath())) {
            error = "Failed to wait for Submit Step 3 button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_SubmitStep2_ButtonXpath())) {
            error = "Failed to click Submit Step 3 button.";
            return false;
        }

        pause(7000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
            error = "Failed to wait for Refresh button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
            error = "Failed to click Refresh button.";
            return false;
        }

        pause(8000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
            error = "Failed to wait for Refresh button to load again.";
            return false;
        }
//        
//        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
//            error = "Failed to click Save Save dropdown button.";
//            return false;
//        }
//
//        pause(7000);

        return true;
    }

}
