/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 Previous Related Injuries",
        createNewBrowserInstance = false
)

public class FR10_PreviousRelatedInjuries extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR10_PreviousRelatedInjuries() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!compensationClaim()) {
            return narrator.testFailed("Failed fill Is this a compensation claim " + error);
        }

        return narrator.finalizeTest("Completed Is this a compensation claim ");
    }

    public boolean compensationClaim() {
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.IncidentTabXpath(), 2)) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.IncidentTabOpneXpath())) {
                error = "Failed to wait for injury Claim - incident and Worker's Injury Details ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.IncidentTabOpneXpath())) {
                error = "Failed to click injury Claim - incident and Worker's Injury Details ";
                return false;
            }
        }
        if(getData("Third Party Manufacturer").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.additionalDescriptionXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - Additional description  ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.additionalDescriptionXpath(), getData("Additional description"))) {
                error = "Failed to enter - Incident and Worker's Injury - Additional description  ";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryDateXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What was the date the injury  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.injuryDateXpath(), date)) {
            error = "Failed to enter - Incident and Worker's Injury - What was the date the injury  ";
            return false;
        }
        
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryTimeXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What was the Time the injury  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.injuryTimeXpath(), time)) {
            error = "Failed to enter - Incident and Worker's Injury - What was the Time the injury  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryConditionsXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - When did you first notice the injury / condition?  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.injuryConditionsXpath(), getData("Injury Conditions"))) {
            error = "Failed to enter - Incident and Worker's Injury - When did you first notice the injury / condition?  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.stoppedWorkingDateXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.stoppedWorkingDateXpath(), date)) {
            error = "Failed to enter - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.stoppedWorkingTimeXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.stoppedWorkingTimeXpath(), time)) {
            error = "Failed to enter - Incident and Worker's Injury - If you stopped work, what was the date? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.reportDateXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - When did you report the injury / condition to your employer ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.reportDateXpath(), date)) {
            error = "Failed to enter - Incident and Worker's Injury - When did you report the injury / condition to your employer ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Injury reported to")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - TInjury reported to " + getData("Injury reported to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyDropDownCheckXpath(getData("Injury reported to")))) {
            error = "Failed to click Incident and Worker's Injury Details - TInjury reported to " + getData("Injury reported to");
            return false;
        }

        String PreviousRelatedInjuries = getData("Previous Related Injuries");
        if (PreviousRelatedInjuries.equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            
            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(PreviousRelatedInjuries))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Previous Related Injuries " + PreviousRelatedInjuries;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(PreviousRelatedInjuries))) {
                error = "Failed to click Incident and Worker's Injury Details - Previous Related Injuries " + PreviousRelatedInjuries;
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.prevoiusRelatedInjuriesTable())) {
                error = "Failed to display injury Claim form - Incident and Worker's Injury Details - Previous Related Injuries table";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.prevoiusRelatedInjuriesTable(), 2)) {
                error = "Failed to display injury Claim form - Incident and Worker's Injury Details - Previous Related Injuries table";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details and Previous Related Injuries viewing grid is displayed showing");

        } else if (PreviousRelatedInjuries.equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
                error = "Failed to scroll injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.prevoiusInjuryDropdownXpath())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(PreviousRelatedInjuries))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + PreviousRelatedInjuries;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(PreviousRelatedInjuries))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + PreviousRelatedInjuries;
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details no fields - Previous Related Injuries table are triggered. ");
        }

        
        return true;
    }

}
