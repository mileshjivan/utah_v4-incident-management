/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author sjonck
 */
@KeywordAnnotation(
        Keyword = "FR3 Alternate scenario HYPER",
        createNewBrowserInstance = false
)

public class FR3_CaptureWitnessStatementsAlternateScenarioHYPER extends BaseClass {
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String pathofImages, path;
    
    public FR3_CaptureWitnessStatementsAlternateScenarioHYPER() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();
    }
    
    public TestResult executeTest() {
        
        if (!superVisor()) {
            return narrator.testFailed("Failed to fill in incident report - " + error);
        }
        if (!witnessStatement()) {
            return narrator.testFailed("Failed to enter witness statement due - " + error);
        }
        
        return narrator.finalizeTest("Successfully completed witness statement");
    }
    
    public boolean superVisor() {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.Safety())) {
            error = "Failed to click Safety supervisor dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Safety supervisor")))) {
            error = "Failed to select Safety supervisor ";
            return false;
        }
        narrator.stepPassed("Successfully selected Safety supervisor: " + getData("Safety supervisor"));
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentXpath())) {
            error = "Failed to click Environment button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentDropDownXpath())) {
            error = "Failed to click Environment dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Environmental supervisor")))) {
            error = "Failed to select Environment supervisor ";
            return false;
        }
        narrator.stepPassed("Successfully selected  Environment supervisor: " + getData("Environmental supervisor"));
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualitytXpath())) {
            error = "Failed to click Quality button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualityDropDownXpath())) {
            error = "Failed to click Quality dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Quality supervisor")))) {
            error = "Failed to select Quality supervisor ";
            return false;
        }
        
        narrator.stepPassed("Successfully selected Quality supervisor: " + getData("Quality supervisor"));
        
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyXpath())) {
//            error = "Failed to click railway Safety button";
//            return false;
//        }
        
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyDropDownXpath())) {
//            error = "Failed to click railway Safety dropdown";
//            return false;
//        }
//        
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Railway safety supervisor")))) {
//            error = "Failed to select railway Safety supervisor ";
//            return false;
//        }
////        
//        narrator.stepPassed("Successfully selected Railway safety supervisor: " + getData("Railway safety supervisor"));
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusXpath())) {
            error = "Failed to click Social Sustainability button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusDropDownXpath())) {
            error = "Failed to click Social supervisor dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Social supervisor")))) {
            error = "Failed to select supervisor supervisor ";
            return false;
        }
        
        narrator.stepPassed("Successfully selected Social supervisor: " + getData("Social supervisor"));
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneXpath())) {
            error = "Failed to click Occupational hygiene button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneDropDownXpath())) {
            error = "Failed to Occupational hygiene supervisor dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Occupational hygiene supervisor")))) {
            error = "Failed to select Occupational hygiene supervisor ";
            return false;
        }
        
        narrator.stepPassed("Successfully selected Occupational hygiene supervisor: " + getData("Occupational hygiene supervisor"));
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceXpath())) {
            error = "Failed to click Compliance button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceDropDownXpath())) {
            error = "Failed to Compliance supervisor dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Compliance supervisor")))) {
            error = "Failed to select Compliance supervisor ";
            return false;
        }
        
        narrator.stepPassed("Successfully selected Compliance supervisor: " + getData("Compliance supervisor"));
        return true;
    }
    
    public boolean witnessStatement() {

//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.saveVerAndAddXpath())) {
//            error = "Failed to click save button 2.Verification and Additional Detail"
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to wait for witness Statements button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to click witness Statements button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsAddXpath())) {
            error = "Failed to click witness Statements add button";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 60)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 60)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }
        
        SeleniumDriverInstance.pause(18000);
        
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementFlow(), 40)) {
            error = "Failed to wait witness Statements flow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementFlow())) {
            error = "Failed to click witness Statements flow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessNameXpath(), getData("witness Name"))) {
            error = "Failed to enter witness name ";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSurnameXpath(), getData("witness Surname"))) {
            error = "Failed to enter witness Surname";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSummaryXpath(), getData("witness Sunnary"))) {
            error = "Failed to enter witness Sunnary";
            return false;
        }
        pause(2000);
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic())) {
//            error = "Failed to click the witness Upload image";
//            return false;
//        }
//        
//        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path)) {
//            error = "Failed to click image 1";
//            return false;
//        }
//        
//        if (!SeleniumDriverInstance.selectIdentificationType()) {
//            error = "Failed to click Add more images ";
//            return false;
//        }
//        
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic())) {
//            error = "Failed to click the witness dvt 2 image";
//            return false;
//        }
//        
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic())) {
//            error = "Failed to click the witness open window button";
//            return false;
//        }
        //Add support document
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.linkalt())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.linkalt())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.LinkURL(), "www.inspiredtesting.com/" )){
            error = "Failed to enter 'www.inspiredtesting.com/' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : https://www.inspiredtesting.com/");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.urlTitle2())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.urlTitle2(), "Inspired Testing")){
            error = "Failed to enter 'Inspired Testing' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : 'Inspired Testing'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.urlAddButton2())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.urlAddButton2())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded 'Inspired Testing' document using 'https://www.inspiredtesting.com/' Link.");
        
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        narrator.stepPassedWithScreenShot("Successfully entered witness statement " + getData("witness Name") + ", " + getData("witness Surname") + ", " + getData("witness Sunnary"));
        
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessSaveCloseButtonXpath())) {
            error = "Failed to click witness save and close button";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableWitness())) {
            error = "Failed to click save Witness details";
            return false;
        }

        narrator.stepPassedWithScreenShot("Saved Witness details");
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to click witness Statements close tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath())) {
            error = "Failed to click witness Statements close tab";
            return false;
        }
        return true;
    }
    
}
