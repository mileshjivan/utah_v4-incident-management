/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Save and close",
        createNewBrowserInstance = false
)
public class FR7_SaveAndClose extends BaseClass {
    
    String error = "";
    
    public FR7_SaveAndClose() {
        
    }
    
    public TestResult executeTest() {
        
        if (!captureDetailsAndsaveStep2()) {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed injured person ");
    }
    
    public boolean captureDetailsAndsaveStep2() {
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.saveAndColse())) {
            error = "Failed to click to Save and close ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.saveAndColse())) {
            error = "Failed to click to Save and close ";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to wait for the safety tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to click the safety tab";
            return false;
        }
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.injuredPersonswait(), 20)) {

            error = "Failed to wait for save injured persons details";
            return false;
        }   
        narrator.stepPassed("Successfully saved injured persons ");
        
        return true;
    }
}
