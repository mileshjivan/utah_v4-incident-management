/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR7 Injury claim",
        createNewBrowserInstance = false
)

public class FR7_InjuryClaim extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_InjuryClaim() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!injuryClaim()) {
            return narrator.testFailed("Failed fill Injured Persons - Injury claim " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Injury claim ");
    }

    public boolean injuryClaim() {

        if (getData("Injury claim").equalsIgnoreCase("True")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.InjuryClaimCheckBoxXPath())) {
                error = "Failed to wait for the Injury Claim checkbox";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.InjuryClaimCheckBoxXPath())) {
                error = "Failed to click on the Injury Claim checkbox";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryClaimTab(), 2)) {
                error = "Failed to display injury Claim Tab ";
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(InjuryClaimPageObject.injuryClaimTab())) {
                error = "Failed to display injury Claim Tab ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully displayed - injury Claim Tab");

        } else if (getData("Injury claim").equalsIgnoreCase("false")) {

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.injuryClaimTab(), 2)) {
                error = "display injury Claim Tab ";
                return false;
            }
            narrator.stepPassedWithScreenShot("injury Claim Tab - no tabs are triggered ");
        }

        return true;
    }

}
