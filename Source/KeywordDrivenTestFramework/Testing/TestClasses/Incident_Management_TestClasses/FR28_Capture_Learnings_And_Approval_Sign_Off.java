/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "Capture Learnings and Approval Sign Off",
        createNewBrowserInstance = false
)
public class FR28_Capture_Learnings_And_Approval_Sign_Off extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR28_Capture_Learnings_And_Approval_Sign_Off() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!CaptureLearningsAndApprovalSignOff()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Capturing Learnings and Approval Sign Off - Main Scenario");
    }

    public boolean CaptureLearningsAndApprovalSignOff() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
            error = "Failed to wait for Incident Action Save more option dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
                error = "Failed to wait for Incident Action Save more option dropdown.";
                return false;
            }
            pause(5000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentActions_SaveMoreOptions_Buttonxpath())) {
                error = "Failed to click Incident Action Save more option dropdown.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentActions_SaveAndClose_Buttonxpath())) {
            error = "Failed to wait for Incident Action Save and close.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentActions_SaveAndClose_Buttonxpath())) {
            error = "Failed to click Incident Action Save and close.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentManagementId())) {
            error = "Failed to wait for Incident Management Record number.";
            return false;
        }

        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.IncidentManagementId()).split("#");

        narrator.stepPassed("Incident Management Record Number: " + Actionid[1]);

        String incidentManagementRec = Actionid[1];

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentActions_Close_Buttonxpath())) {
            error = "Failed to wait for Incident Action close.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentActions_Close_Buttonxpath())) {
            error = "Failed to click Incident Action Save close.";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentManagement_Refresh_Buttonxpath())) {
            error = "Failed to wait for Incident Management refresh.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentManagement_Refresh_Buttonxpath())) {
            error = "Failed to click Incident Management refresh.";
            return false;
        }

        pause(9000);

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(MainScenario_PageObjects.incidentManagement_xpath(incidentManagementRec),500)) {
            error = "Failed to wait for Incident Management Record.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentManagement_xpath(incidentManagementRec))) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentManagement_xpath(incidentManagementRec))) {
                error = "Failed to wait for Incident Management Record.";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentManagement_xpath(incidentManagementRec))) {
                error = "Failed to click Incident Management Record.";
                return false;
            }
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
            error = "Failed to wait for 3.Incident Investigation.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
                error = "Failed to wait for 3.Incident Investigation.";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_TabXpath())) {
                error = "Failed to click 3.Incident Investigation";
                return false;
            }
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_LearningAndApproval_tabxpath())) {
            error = "Failed to wait for Learning & Approval tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_LearningAndApproval_tabxpath())) {
            error = "Failed to click Learning & Approval tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.LearningAndApproval_Findings_Panelxpath())) {
            error = "Failed to wait for Investigation Findings close panel.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.LearningAndApproval_Findings_Panelxpath())) {
            error = "Failed to click Investigation Findings close panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("FR28-Capture Learnings & Approval Sign Off");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_learnings_TextFieldxpath())) {
            error = "Failed to wait for Incident Investigation Learnings to be shared... Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.incidentInvestigation_learnings_TextFieldxpath(), testData.getData("Learnings"))) {
            error = "Failed to EnterText into Incident Investigation Learnings to be shared... Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Conclusion_TextFieldxpath())) {
            error = "Failed to wait for Incident Investigation Conclusion Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.incidentInvestigation_Conclusion_TextFieldxpath(), testData.getData("Conclusion"))) {
            error = "Failed to EnterText into Incident Investigation Conclusion Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.LearningAndApproval_declaration_CheckBoxXpath())) {
            error = "Failed to wait for I Have included all required .... checkbox.";
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.LearningAndApproval_declaration_CheckBoxXpath())) {
            error = "Failed to click I Have included all required .... checkbox.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Learning & Approval Declaration checkbox.");
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Comments_TextFieldxpath(), 300000)) {
            error = "Failed to wait for Incident Investigation Comments Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.incidentInvestigation_Comments_TextFieldxpath(), testData.getData("Comments"))) {
            error = "Failed to EnterText into Incident Investigation Comments Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.LearningAndApproval_Process_ButtonXpath())) {
            error = "Failed to wait for Process button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.LearningAndApproval_Process_ButtonXpath())) {
            error = "Failed to click Process button.";
            return false;
        }

        String button = getData("Submit Step 3");

        if (button.equalsIgnoreCase("False")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_SaveAndContinue_Buttonxpath())) {
                error = "Failed to wait for Incident Investigation Save And Continue to step 4 button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_SaveAndContinue_Buttonxpath())) {
                error = "Failed to Click Incident Investigation Save And Continue to step 4 button.";
                return false;
            }

        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Submit_Buttonxpath())) {
                error = "Failed to wait for Incident Investigation Submit step 3 button.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_Submit_Buttonxpath())) {
                error = "Failed to click Incident Investigation Submit step 3 button.";
                return false;
            }

        }
        pause(10000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.IncidentManagementId())) {
            error = "Failed to wait for Incident Management Record number.";
            return false;
        }

        return true;
    }

}
