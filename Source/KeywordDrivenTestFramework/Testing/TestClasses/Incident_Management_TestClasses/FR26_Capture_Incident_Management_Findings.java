/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "Capture Incident Management Findings",
        createNewBrowserInstance = false
)
public class FR26_Capture_Incident_Management_Findings extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber;

    public FR26_Capture_Incident_Management_Findings() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!CaptureIncidentManagementFindings()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Capturing Incident Management Findings - Main Scenario");
    }

    public boolean CaptureIncidentManagementFindings() {
        pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_LearningAndApproval_tabxpath(), 300000)) {
            error = "Failed to wait for Learning & Approval tab.";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_LearningAndApproval_tabxpath())) {
            error = "Failed to click Learning & Approval tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Add_Buttonxpath())) {
            error = "Failed to wait for Incident Management Findings Add button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Add_Buttonxpath())) {
            error = "Failed to click Incident Management Findings Add button.";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Description_TextFieldxpath())) {
            error = "Failed to wait for Incident Management Findings Add button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Description_TextFieldxpath(), testData.getData("Finding"))) {
            error = "Failed to EnterText into Incident Management Findings Description Textfield.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_Selectxpath())) {
            error = "Failed to wait for Incident Management Finding Owner Select field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_Selectxpath())) {
            error = "Failed to click Incident Management Finding Owner Select field.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_xpath(testData.getData("FindingOwner")))) {
            error = "Failed to wait for Incident Management Finding Owner.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_xpath(testData.getData("FindingOwner")))) {
            error = "Failed to click Incident Management Finding Owner.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_Selectxpath())) {
            error = "Failed to wait for Incident Management Risk Source Select field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_Selectxpath())) {
            error = "Failed to click Incident Management Risk Source Select field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_TickAllxpath())) {
            error = "Failed to wait for Incident Management tick all Risk Source.";
            return false;
        }
        
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_TickAllxpath())) {
            error = "Failed to click Incident Management tick all Risk Source.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Classification_Selectxpath())) {
            error = "Failed to wait for Incident Management Finding Classification Select field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Classification_Selectxpath())) {
            error = "Failed to click Incident Management Finding Classification Select field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Classification_xpath(testData.getData("Classification")))) {
            error = "Failed to wait for Incident Management Finding Classification.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Classification_xpath(testData.getData("Classification")))) {
            error = "Failed to click Incident Management Finding Classification.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Risk_Selectxpath())) {
            error = "Failed to wait for Incident Management Risk Select field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Risk_Selectxpath())) {
            error = "Failed to click Incident Management Risk Select field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Risk_xpath(testData.getData("Risk")))) {
            error = "Failed to wait for Incident Management Risk.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Risk_xpath(testData.getData("Risk")))) {
            error = "Failed to click Incident Management Risk.";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully captured Incident Management Finding Details");

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Save_Selectxpath())) {
            error = "Failed to wait for Incident Management Save button field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Save_Selectxpath())) {
            error = "Failed to click Incident Management Save button field.";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_RecordNumber_Selectxpath())) {
            error = "Failed to wait for Incident Management Record number field.";
            return false;
        }

        String retrieveRecordNumber = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_RecordNumber_Selectxpath());
        narrator.stepPassed("Saved Incident Management Findings " + retrieveRecordNumber);
        recordNumber = retrieveRecordNumber.substring(10);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Close_Selectxpath())) {
            error = "Failed to wait for Incident Management Close button field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_Close_Selectxpath())) {
            error = "Failed to click Incident Management Close button field.";
            return false;
        }

        pause(7000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_ManagementFindings_ListedRecord_Listxpath(recordNumber))) {
            error = "Failed to wait for Listed Incident Management Finding Record #"+retrieveRecordNumber;
            return false;
        }

        return true;
    }

}
