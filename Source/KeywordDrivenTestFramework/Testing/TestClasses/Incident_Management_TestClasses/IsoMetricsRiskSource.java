/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.GmailPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsRiskAssessmentScopePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsRiskSourcePageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Risk source",
        createNewBrowserInstance = false
)
public class IsoMetricsRiskSource extends BaseClass {

    String error = "";

    public IsoMetricsRiskSource() {

    }

    public TestResult executeTest() {

        if (!navigateToRiskAssessment()) {
            return narrator.testFailed("Failed to navigate Risk Assessment scope Page - " + error);
        }

        return narrator.finalizeTest("Successfully Saved Audit record ");
    }

    public boolean navigateToRiskAssessment() {

        if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsRiskSourcePageObjects.riskSourceXpath(getData("Risk source")), 10)) {
            error = "Failed to wait risk source check box";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.riskSourceXpath(getData("Risk source")))) {
            error = "Failed to click risk source";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsRiskSourcePageObjects.riskSourceCheck(), 1000)) {
            error = "Failed to wait Risk source checkbox";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.laoding(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.laoding2(), 200)) {
                error = "Website too long to load ";
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.riskSourceCheck())) {
            error = "Failed to check Risk source ";
            return false;
        }
        String riskAssRef = getData("Risk assessment reference");

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsRiskSourcePageObjects.riskAssRefXpath(), riskAssRef)) {
            error = "Failed to enter Risk assessment reference " + riskAssRef;
            return false;
        }

        narrator.stepPassed("Successfully entered Risk assessment reference :" + riskAssRef);

        String riskDescription = getData("Risk description");

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsRiskSourcePageObjects.riskDesciptionXpath(), riskDescription)) {
            error = "Failed to enter Risk description " + riskDescription;
            return false;
        }

        narrator.stepPassed("Successfully entered Risk description :" + riskDescription);

        String subProcesses = getData("Sub processes");

        if (!SeleniumDriverInstance.clickElementsbyXpath(IsometricsRiskSourcePageObjects.rmBusinessUnitXpath(subProcesses), 0)) {
            error = "Failed to click dropdown Sub processes 1" + subProcesses;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementsbyXpath(IsometricsRiskSourcePageObjects.rmBusinessUnitCheckXpath(subProcesses), 1)) {
            error = "Failed to click select sub processes 2" + subProcesses;
            return false;
        }

        narrator.stepPassed("Successfully select Sub processes :" + subProcesses);

        String riskOwner = getData("Risk owner");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.riskOwnerDropdownXpath())) {
            error = "Failed to click dropdown risk owner " + riskOwner;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.riskOwnerXpath(riskOwner))) {
            error = "Failed to click risk owner " + riskOwner;
            return false;
        }

        narrator.stepPassed("Successfully selected risk owner :" + riskOwner);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.riskMatrix())) {
            error = "Failed to click - Supporting Information: Risk Matrix ";
            return false;
        }

        //Impact - Inherent Risk
        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.inherenrRiskButtonXpath())) {
            error = "Failed to click - Inherent Risk ";
            return false;
        }

        int numOfInherentRisk = Integer.parseInt(getData("Num of Inherent Risk"));

        for (int m = 1; m <= numOfInherentRisk; m++) {

            String impactType = getData("Impact type " + m);
            String[] impact = getData("impact " + m).split(",");
            String rating = getData("likeLihood rating " + m);

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.addImpactRiskAddButton())) {
                error = "Failed to click - Inherent Risk add button ";
                return false;
            }

            if (m == 1) {
                SeleniumDriverInstance.pause(4000);
            }
            
            if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsRiskSourcePageObjects.impactTypeDropdownXpath(m), 2)) {
                if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.addImpactRiskAddButton())) {
                    error = "Failed to click - Inherent Risk add button ";
                    return false;
                }
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.impactTypeDropdownXpath(m))) {
                error = "Failed to click - Inherent Risk -Impact type dropdown " + m;
                return false;
            }
            SeleniumDriverInstance.pause(2500);

            if (!SeleniumDriverInstance.clickElementsbyXpath(IsometricsRiskSourcePageObjects.selectFromDropdown(impactType), 1)) {
                error = "Failed to select Impack type " + impactType;
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.impactDropdownXpath(m))) {
                error = "Failed to click - Inherent Risk -Impact  dropdown " + m;
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.selectFromDropdownInside(impact[0]))) {
                error = "Failed to click - Inherent Risk - Impact Rom " + m + " " + impact[0];
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.selectFromDropdown(impact[1]))) {
                error = "Failed to click - Inherent Risk - Impact Rom " + m + " " + impact[1];
                return false;
            }
            SeleniumDriverInstance.pause(2000);

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.likeliHoodRatingDropdownXpath(m))) {
                error = "Failed to click - Inherent Risk - likeLihood rating   dropdown " + m;
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.selectFromDropdown(rating))) {
                error = "Failed to click - Inherent Risk - likeLihood rating   dropdown " + m;
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.inherentSaveButtonXpath())) {
            error = "Failed to click - Inherent Risk - Save button ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskSourcePageObjects.editButtonOfImpactTypeXpath(1))) {
            error = "Failed to click - Edit button - Fines -> R501  000 to R1 000 000 + ";
            return false;
        }

        return true;
    }

}
