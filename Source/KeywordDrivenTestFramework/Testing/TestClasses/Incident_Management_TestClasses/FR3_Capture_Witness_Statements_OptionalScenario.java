/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Witness Statements Optional Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Witness_Statements_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Witness_Statements_OptionalScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!witnessStatement())
        {
            return narrator.testFailed("Failed to fill in incident report - " + error);
        }
        if (!UploadDocument())
        {
            return narrator.testFailed("Failed to upload a document - " + error);
        }
        return narrator.finalizeTest("Successfully uploaded a document for Witness Statements");
    }

    public boolean witnessStatement()
    {

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to wait for witness Statements button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to click witness Statements button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.ValidateGridIsNotEditableXPath()))
        {
            error = "Failed to validate that the editable grid is displayed";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Witness Statement non-editable grid is displayed");
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsAddXpath()))
        {
            error = "Failed to click Witness Statements add button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated that a new module is displayed in the Add Phase");

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40))
            {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessNameXpath(), getData("witness Name")))
        {
            error = "Failed to enter witness name ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSurnameXpath(), getData("witness Surname")))
        {
            error = "Failed to enter witness Surname";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.witnessSummaryXpath(), getData("witness Sunnary")))
        {
            error = "Failed to enter witness Sunnary";
            return false;
        }
        
//        sikuliDriverUtility.pause(1500);
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic()))
//        {
//            error = "Failed to click the Upload image";
//            return false;
//        }
//        String pathofImages = System.getProperty("user.dir") + "\\images";
//
//        String path = new File(pathofImages).getAbsolutePath();
//        System.out.println("path " + pathofImages);
//
//        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path))
//        {
//            error = "Failed to click image 1";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.selectIdentificationType())
//        {
//            error = "Failed to press Enter ";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.dvt2Pic()))
//        {
//            error = "Failed to click the witness dvt 2 image";
//            return false;
//        }
//
//        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.openPic()))
//        {
//            error = "Failed to click the witness open window button";
//            return false;
//        }
       
        narrator.stepPassed(
                "Successfully entered witness statement " + getData("witness Name") + ", " + getData("witness Surname") + ", " + getData("witness Sunnary"));

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.witnessSaveButtonXpath()))
        {
            error = "Failed to click witness save button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath()))
        {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved")))
            {
                narrator.stepPassedWithScreenShot("Successfully saved the Wittness record: " + text);
            }
        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableWitness(), 20))
//        {
//
//            error = "Failed to click save Witness details";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.CloseWitnessStatementsXPath()))
        {
            error = "Failed to click Witness Statements close button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementByJavascript(MainScenario_PageObjects.CloseWitnessStatementsXPath()))
        {
            error = "Failed to click Witness Statements close button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to wait for witness Statements close tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.witnessStatementsXpath()))
        {
            error = "Failed to click witness Statements close tab";
            return false;
        }

        return true;
    }

    public boolean UploadDocument()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for the Supporting Documents tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click the Supporting Documents tab";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(VerificationAndAdditionalPageObject.uploadPic()))
        {
            error = "Failed to click the Upload image";
            return false;
        }
        String pathofImages = System.getProperty("user.dir") + "\\images";

        String path = new File(pathofImages).getAbsolutePath();
        System.out.println("path " + pathofImages);

        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fileNamePic1(), path))
        {
            error = "Failed to click image 1";
            return false;
        }

        if (!SeleniumDriverInstance.selectIdentificationType())
        {
            error = "Failed to press Enter ";
            return false;
        }
        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.dvtPic()))
        {
            error = "Failed to click dvt ";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.openPic()))
        {
            error = "Failed to click open ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document uploaded");
        
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(VerificationAndAdditionalPageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.SaveButtonXPath()))
        {
            error = "Failed to wait for the Save Supporting Documents button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.SaveButtonXPath()))
        {
            error = "Failed to click the Save Supporting Documents button";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath(), 3))
        {
            error = "Failed to click on the Save And Continue button";
            return false;
        } else
        {
            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());

            if (text.equals(" "))
            {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Record Saved")))
            {
                narrator.stepPassed("Successfully saved the uploaded document: " + text);
            }
        }

        narrator.stepPassedWithScreenShot("Successfully uploaded supporting documents ");
        return true;
    }
}
