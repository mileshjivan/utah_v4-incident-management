/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Super Visor",
        createNewBrowserInstance = false
)

public class IsometrixSuperVisor extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public IsometrixSuperVisor()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!superVisor())
        {
            return narrator.testFailed("Failed to fill in incident report - " + error);
        }
        return narrator.finalizeTest("Successfully captured super visors");
    }

    public boolean superVisor()
    {
        if(!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.addDetailsXpath())){
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.addDetailsXpath()))
        {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.Safety(), 99999))
        {
            error = "Failed to wait for Safety supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.Safety()))
        {
            error = "Failed to click Safety supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Safety supervisor"))))
        {
            error = "Failed to wait for select Safety supervisor ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Safety supervisor"))))
        {
            error = "Failed to select Safety supervisor ";
            return false;
        }
        narrator.stepPassed("Successfully selected Safety supervisor: " + getData("Safety supervisor"));
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.EnvironmentXpath()))
        {
            error = "Failed to wait for Environment button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentXpath()))
        {
            error = "Failed to click Environment button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.EnvironmentDropDownXpath()))
        {
            error = "Failed to wait for Environment dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EnvironmentDropDownXpath()))
        {
            error = "Failed to click Environment dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Environmental supervisor"))))
        {
            error = "Failed to wait for select Environment supervisor ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Environmental supervisor"))))
        {
            error = "Failed to select Environment supervisor ";
            return false;
        }
        narrator.stepPassed("Successfully selected  Environment supervisor: " + getData("Environmental supervisor"));
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.QualitytXpath()))
        {
            error = "Failed to wait for Quality button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualitytXpath()))
        {
            error = "Failed to click Quality button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.QualityDropDownXpath()))
        {
            error = "Failed to wait for Quality dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.QualityDropDownXpath()))
        {
            error = "Failed to click Quality dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Quality supervisor"))))
        {
            error = "Failed to wait for select Quality supervisor ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Quality supervisor"))))
        {
            error = "Failed to select Quality supervisor ";
            return false;
        }

        narrator.stepPassed("Successfully selected Quality supervisor: " + getData("Quality supervisor"));

//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyXpath()))
//        {
//            error = "Failed to click railway Safety button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.railwaySafetyDropDownXpath()))
//        {
//            error = "Failed to click railway Safety dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Railway safety supervisor"))))
//        {
//            error = "Failed to select railway Safety supervisor ";
//            return false;
//        }
//
//        narrator.stepPassed("Successfully selected Railway safety supervisor: " + getData("Railway safety supervisor"));
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.socialSusXpath()))
        {
            error = "Failed to wait for Social Sustainability button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusXpath()))
        {
            error = "Failed to click Social Sustainability button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.socialSusDropDownXpath()))
        {
            error = "Failed to wait for Social supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.socialSusDropDownXpath()))
        {
            error = "Failed to click Social supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Social supervisor"))))
        {
            error = "Failed to wait for select supervisor supervisor ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Social supervisor"))))
        {
            error = "Failed to select supervisor supervisor ";
            return false;
        }

        narrator.stepPassed("Successfully selected Social supervisor: " + getData("Social supervisor"));
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.occHygieneXpath()))
        {
            error = "Failed to wait for Occupational hygiene button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneXpath()))
        {
            error = "Failed to click Occupational hygiene button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.occHygieneDropDownXpath()))
        {
            error = "Failed to wait for Occupational hygiene supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.occHygieneDropDownXpath()))
        {
            error = "Failed to click Occupational hygiene supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Occupational hygiene supervisor"))))
        {
            error = "Failed to wait for select Occupational hygiene supervisor ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Occupational hygiene supervisor"))))
        {
            error = "Failed to select Occupational hygiene supervisor ";
            return false;
        }

        narrator.stepPassed("Successfully selected Occupational hygiene supervisor: " + getData("Occupational hygiene supervisor"));
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.complianceXpath()))
        {
            error = "Failed to wait for Compliance button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceXpath()))
        {
            error = "Failed to click Compliance button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.complianceDropDownXpath()))
        {
            error = "Failed to wait for Compliance supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.complianceDropDownXpath()))
        {
            error = "Failed to click Compliance supervisor dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Compliance supervisor"))))
        {
            error = "Failed to wait for select Compliance supervisor ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anySupervisorXpath(getData("Compliance supervisor"))))
        {
            error = "Failed to select Compliance supervisor ";
            return false;
        }

        narrator.stepPassed("Successfully selected Compliance supervisor: " + getData("Compliance supervisor"));
        return true;
    }
}
