/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.EnvironmentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MonitoringAirPageObjects;
import java.util.Set;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR14 Monitoring Water",
        createNewBrowserInstance = false
)

public class FR15_MonitoringAreasWater extends BaseClass {

    String error = "";
    String monitorAreas = "";

    public FR15_MonitoringAreasWater() {

    }

    public TestResult executeTest() {
        if (!monitorAreaWater()) {
            return narrator.testFailed("Failed fill Monitoring areas - " + monitorAreas + " " + error);
        }

        return narrator.finalizeTest("Succesfully open a new window of " + monitorAreas + " ");
    }

    public boolean monitorAreaWater() {

        String businessUnit = getData("Business unit");
        String[] waterType = getData("Water Type").split(",");
        String measuremenType = getData("Measurement Type");
        String year = getData("Year");
        String month = getData("Month");

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.waterFlowProcessXpath())) {
            error = "Failed to wait water flow process flow ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.waterFlowProcessXpath())) {
            error = "Failed to click water flow process flow ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.activeAddPhaseXpath(), 3)) {
            error = "Failed to be in Add phase ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.waterBusinessUnitDropdownXpath())) {
            error = "Failed to click on business Unit dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(businessUnit))) {
            error = "Failed to click on business Unit " + businessUnit;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.waterTypeDropdownXpath())) {
            error = "Failed to click on water type dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.anyActiveDropdownXpath(waterType[0]))) {
            error = "Failed to click on water type " + waterType[0];
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(waterType[1]), 6)) {
            error = "Failed to wait on water type " + waterType[1];
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(waterType[1]))) {
            error = "Failed to click on water type " + waterType[1];
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.waterMeasurementTypeDropdownXpath())) {
            error = "Failed to click on measurement type dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(measuremenType))) {
            error = "Failed to click on measurement type " + measuremenType;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.waterMonthDropdownXpath())) {
            error = "Failed to click on moth dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(month))) {
            error = "Failed to click on month " + month;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.waterYearDropdownXpath())) {
            error = "Failed to click on year dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.discharge_to())){
            error = "Failed to wait for Discharge To input filed.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(MonitoringAirPageObjects.discharge_to(), "Inspired Testing")){
            error = "Failed to enter text into Discharge To input filed.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.TypeOfMonitoringAreaOption(year))) {
            error = "Failed to click on year " + year;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.waterSaveXpath())) {
            error = "Failed to click on save button ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MonitoringAirPageObjects.activeEditPhaseXpath(), 3)) {
            error = "Failed to wait for air save block layout";
            return false;
        }

        String[] airRecord = SeleniumDriverInstance.retrieveTextByXpath(MonitoringAirPageObjects.getAirMonitorRecord()).split("#");

        narrator.stepPassedWithScreenShot("Successfully saved Water Transaction Monitoring " + airRecord[1]);

        Set<String> set = SeleniumDriverInstance.Driver.getWindowHandles();
        String[] win = set.stream().toArray(String[]::new);
        SeleniumDriverInstance.Driver.switchTo().window(win[1]).close();
        SeleniumDriverInstance.Driver.switchTo().window(win[0]);

        SeleniumDriverInstance.switchToDefaultContent();

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame of " + monitorAreas;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MonitoringAirPageObjects.refButtonWater())) {
            error = "Failed to click on refreach button ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]), 14)) {
            error = "Failed to save record in Incident Management edit page " + airRecord[1];
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(EnvironmentPageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]))) {
            error = "Failed to scrol record in Incident Management edit page " + airRecord[1];
            return false;
        }

        return true;
    }

}
