/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Injury Illness on duty",
        createNewBrowserInstance = false
)

public class FR7_IllnessOnDuty extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_IllnessOnDuty() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!illness()) {
            return narrator.testFailed("Failed fill Injured Persons illness - " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons illness ");
    }

    public boolean illness() {

        if (getData("Injury/Illness on duty?").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.injuryOrIlnessDropdownXPath())) {
                error = "Failed to wait for the injury or illness dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuryOrIlnessDropdownXPath())) {
                error = "Failed to click the injury or illness dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.anySupervisorXpath(getData("Injury/Illness on duty?")))) {
                error = "Failed to wait for the injury or illness element: ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(getData("Injury/Illness on duty?")))) {
                error = "Failed to click the injury or illness element: ";
                return false;
            }

            if (getData("Drug and alcohol test required").equalsIgnoreCase("Yes")) {
                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.jobProfileTextFieldXPath())) {
                    error = "Failed to wait for the Job profile: " + testData.getData("Job Profile");
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.jobProfileTextFieldXPath(), testData.getData("Job Profile"))) {
                    error = "Failed to fill in the Job profile: " + testData.getData("Job Profile");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.positionStartDatePickerXPath())) {
                    error = "Failed to wait for the Position start date: " + date;
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.positionStartDatePickerXPath(), date)) {
                    error = "Failed to fill in the Position start date: " + date;
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.usualJobTasksXPath())) {
                    error = "Failed to wait for the Job task: " + testData.getData("Job Task");
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.usualJobTasksXPath(), testData.getData("Job Task"))) {
                    error = "Failed to fill in the Job task: " + testData.getData("Job Task");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.injuryOrIllnessClassificationXPath())) {
                    error = "Failed to wait for the Injury Classification ";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuryOrIllnessClassificationXPath())) {
                    error = "Failed to click the Injury Classification ";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.treeElementXPath(testData.getData("Classification")))) {
                    error = "Failed to wait for the Injury item: " + testData.getData("Classification");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.treeElementXPath(testData.getData("Classification")))) {
                    error = "Failed to select the Injury item: " + testData.getData("Classification");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Treatment")))) {
                    error = "Failed to wait for the Medical Treatment Case item: " + testData.getData("Treatment");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Treatment")))) {
                    error = "Failed to select the Medical Treatment Case item: " + testData.getData("Treatment");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.descriptionXPath())) {
                    error = "Failed to wait for the Description field: " + testData.getData("Description");
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.descriptionXPath(), testData.getData("Description"))) {
                    error = "Failed to fill in the Description field: " + testData.getData("Description");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.activityAtTheTimeDropdownXPath())) {
                    error = "Failed to wait for the Activity at the time dropdown";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.activityAtTheTimeDropdownXPath())) {
                    error = "Failed to click the Activity at the time dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Activity")))) {
                    error = "Failed to wait for the Activity at the time item: " + testData.getData("Activity");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Activity")))) {
                    error = "Failed to select the Activity at the time item: " + testData.getData("Activity");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.bodyPartsDropdownXPath())) {
                    error = "Failed to wait for the Body Parts dropdown";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsDropdownXPath())) {
                    error = "Failed to click the Body Parts dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Body Part 1") + " "))) {
                    error = "Failed to wait for the following item: " + testData.getData("Body Part 1");
                    return false;
                }
                if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Body Part 1") + " "))) {
                    error = "Failed to scroll to the following item: " + testData.getData("Body Part 1");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Body Part 1") + " "))) {
                    error = "Failed to select the following item: " + testData.getData("Body Part 1");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(testData.getData("Body Part 2") + " "))) {
                    error = "Failed to wait for the following item: " + testData.getData("Body Part 2");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(testData.getData("Body Part 2") + " "))) {
                    error = "Failed to select the following item: " + testData.getData("Body Part 2");
                    return false;
                }
                SeleniumDriverInstance.pause(500);
                
                // Select option on Nature Of Injury dropdown by clicking on tree items untill it leads to the final item
                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.natureOfInjuryLabelXPath())) {
                    error = "Failed to wait for the nature of injury label";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.natureOfInjuryLabelXPath())) {
                    error = "Failed to click the nature of injury label";
                    return false;
                }

                SeleniumDriverInstance.pause(1500);
                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.natureOfInjuryDropdownXPath())) {
                    error = "Failed to wait for the Nature of Injury dropdown";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.natureOfInjuryDropdownXPath())) {
                    error = "Failed to click on the Nature of Injury dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Classification")))) {
                    error = "Failed to wait for the following item: " + testData.getData("Classification");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Classification")))) {
                    error = "Failed to select the following item: " + testData.getData("Classification");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 1") + " "))) {
                    error = "Failed to wait for the following item: " + testData.getData("Nature 1");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 1") + " "))) {
                    error = "Failed to select the following item: " + testData.getData("Nature 1");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 2")))) {
                    error = "Failed to wait for the following item: " + testData.getData("Nature 2");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Nature 2")))) {
                    error = "Failed to select the following item: " + testData.getData("Nature 2");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Nature 3")))) {
                    error = "Failed to wait for the following item: " + testData.getData("Nature 3");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Nature 3")))) {
                    error = "Failed to select the following item: " + testData.getData("Nature 3");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.mechanismDropdownXPath())) {
                    error = "Failed to wait for the Mechanism dropdown";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.mechanismDropdownXPath())) {
                    error = "Failed to click on the Mechanism dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Mechanism 1") + " "))) {
                    error = "Failed to wait for the following item: " + testData.getData("Mechanism 1");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(testData.getData("Mechanism 1") + " "))) {
                    error = "Failed to select the following item: " + testData.getData("Mechanism 1");
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Mechanism 2")))) {
                    error = "Failed to wait for the following item: " + testData.getData("Mechanism 2");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Mechanism 2")))) {
                    error = "Failed to select the following item: " + testData.getData("Mechanism 2");
                    return false;
                }

                // Fill in the Follow Up Description text area and click the Follow Up Required checkbox, Follow Up Details textarea appears
                if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.treatmentProvidedDescriptionXPath())) {
                    error = "Failed to wait for the Follow Up Description field: ";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.treatmentProvidedDescriptionXPath(), testData.getData("Follow Up Description"))) {
                    error = "Failed to fill in the Follow Up Description field: " + testData.getData("Follow Up Description");
                    return false;
                }

//
//            // Fill in Treatment Facility Details text area
//            if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.TreatmentFacililtyDetailsXPath(), getData("Treatment Facility Details"))) {
//                error = "Failed to fill in the Treatment Facility Details text area: " + getData("Treatment Facility Details");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsThisInjuryRecordableCheckBoxXPath())) {
//                error = "Failed to click on the Is This Injury Recordable checkbox ";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath())) {
//                error = "Failed to wait for the Is It Reportable checkbox to appear ";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.IsItReportableCheckBoxXPath())) {
//                error = "Failed to click on the Is It Reportable checkbox";
//                return false;
//            }
                // Reportable to Dropdown appears after checking Is It Reportable dropdown
//            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveAndContinueButtonXPath())) {
//                error = "Failed to click on the Save And Continue button";
//                return false;
//            }
//
//            if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
//                if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
//                    error = "Webside too long to load wait reached the time out";
//                    return false;
//                }
//            }
//
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath())) {
//                error = "Failed to click on the Save And Continue button";
//                return false;
//            } else {
//                String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());
//
//                if (text.equals(" ")) {
//                    error = "String cannot be empty";
//                    return false;
//                } else if (text.equals(testData.getData("Record Saved"))) {
//                    narrator.stepPassed("Successfully saved the injured Persons record: " + text);
//                }
//            }
//
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.injuredPersonseditPhaseActive(), 2)) {
//                error = "Failed for edit phase";
//                return false;
//            }
                narrator.stepPassedWithScreenShot("Successfully entered the Incident Management/Injured Persons form");
            }

        } else if (getData("Injury/Illness on duty?").equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.injuryOrIlnessDropdownXPath())) {
                error = "Failed to wait for the injury or illness dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuryOrIlnessDropdownXPath())) {
                error = "Failed to click the injury or illness dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.anySupervisorXpath(getData("Injury/Illness on duty?")))) {
                error = "Failed to wait for the injury or illness element: ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(getData("Injury/Illness on duty?")))) {
                error = "Failed to click the injury or illness element: ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.supportingDocuments(), 2)) {
                error = "Failed to Supporting Documents ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully entered the Injured Persons form");
        }

        return true;
    }

}
