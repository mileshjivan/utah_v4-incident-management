/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "Capture Investigation Team",
        createNewBrowserInstance = false
)
public class FR17_Capture_Investigation_Team_Main_Scenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR17_Capture_Investigation_Team_Main_Scenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!CapturingInvestigationTeam()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Capturing Investigation Team - Main Scenario");
    }

    public boolean CapturingInvestigationTeam() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Save_textFieldXpath())) {
            error = "Failed to wait for Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Save_textFieldXpath())) {
            error = "Failed to click Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_DueDate_textFieldXpath())) {
            error = "Failed to wait for Investigation due date text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_DueDate_textFieldXpath(), testData.getData("InvestigationDueDate"))) {
            error = "Failed to EnterText: " + testData.getData("InvestigationDueDate") + ", into Investigation due date text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Scope_textFieldXpath())) {
            error = "Failed to wait for Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Scope_textFieldXpath())) {
            error = "Failed to click Investigation Scope text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_Scope_textFieldXpath(), testData.getData("InvestigationScope"))) {
            error = "Failed to EnterText: " + testData.getData("InvestigationScope") + ", into Investigation Scope text field.";
            return false;
        }
        narrator.stepPassed("Investigation Due Date: " + testData.getData("InvestigationDueDate"));
        narrator.stepPassed("Investigation Scope: " + testData.getData("InvestigationScope"));

        pause(500);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_InvestigationTeam_DropDownXpath())) {
            error = "Failed to wait for Investigation Team dropdown Button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_InvestigationTeam_DropDownXpath())) {
            error = "Failed to click Investigation Team dropdown Button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_InvestigationTeam_ADD_ButtonXpath())) {
            error = "Failed to wait for Investigation Team Add Button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_InvestigationTeam_ADD_ButtonXpath())) {
            error = "Failed to click Investigation Team Add Button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Fullname_SelectFieldXpath())) {
            error = "Failed to wait for Investigation Team Fullname select field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Fullname_SelectFieldXpath())) {
            error = "Failed to click Investigation Team Fullname select field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Fullname_Xpath(testData.getData("FullName")))) {
            error = "Failed to wait for Investigation Team Fullname: " + testData.getData("Fullname") + ".";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Fullname_Xpath(testData.getData("FullName")))) {
            error = "Failed to Select Investigation Team Fullname: " + testData.getData("Fullname") + ".";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Role_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team Fullname select field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_Role_TextFieldXpath(), testData.getData("Role"))) {
            error = "Failed to EnterText into Investigation Team Role: " + testData.getData("Role") + ".";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_StartDate_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team Start Date.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_StartDate_TextFieldXpath(), testData.getData("StartDate"))) {
            error = "Failed to EnterText into Investigation Team Start Date: " + testData.getData("Role") + ".";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_EndDate_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team End Date field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_EndDate_TextFieldXpath(), testData.getData("EndDate"))) {
            error = "Failed to EnterText into Investigation Team End Date: " + testData.getData("Role") + ".";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Hours_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team Hours field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Hours_TextFieldXpath())) {
            error = "Failed to click for Investigation Team Hours field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.investigationTeam_Hours_TextFieldXpath(), testData.getData("Hours"))) {
            error = "Failed to EnterText into Investigation Team Hours: " + testData.getData("Hours") + ".";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_InvestigationTeam_TitleXpath())) {
            error = "Failed to wait for Investigation Team Title.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_InvestigationTeam_TitleXpath())) {
            error = "Failed to click for Investigation Team Title.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured Investigation Team records.");
        narrator.stepPassed("Investigation Team Fullname: " + testData.getData("Fullname"));
        narrator.stepPassed("Investigation Team Hours: " + testData.getData("StartDate"));
        narrator.stepPassed("Investigation Team Hours: " + testData.getData("EndDate"));
        narrator.stepPassed("Investigation Team Role: " + testData.getData("Role"));
        narrator.stepPassed("Investigation Team Hours: " + testData.getData("Hours"));
 
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.investigationTeam_Save_ButtonXpath())) {
            error = "Failed to wait for Investigation Team Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.investigationTeam_Save_ButtonXpath())) {
            error = "Failed to click Investigation Team Save button.";
            return false;
        }
        pause(5000);

        return true;
    }

}
