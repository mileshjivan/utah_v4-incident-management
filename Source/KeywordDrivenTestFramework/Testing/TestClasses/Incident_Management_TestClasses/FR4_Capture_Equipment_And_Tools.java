/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.By;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Equipment and Tools Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Equipment_And_Tools extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    static String assetId = "";

    public FR4_Capture_Equipment_And_Tools() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!ValidatePanelsAreDisplayed()) {
            return narrator.testFailed("Failed to validate that the Persons Involved, Witness Statement and Equipment Involved panels are displayed - " + error);
        }
        if (!equipment()) {
            return narrator.testFailed("Failed to - " + error);
        }

        return narrator.finalizeTest("Successfully captured the details for Equipment and Tools");
    }

    public boolean ValidatePanelsAreDisplayed() {
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.PersonsInvolvedXpath())) {
            error = "Failed to validate that the Persons Involved panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.PersonsInvolvedXpath());

            if (text.equals(" ")) {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Persons Involved"))) {
                narrator.stepPassed("Successfully validated that the Persons Involved panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.WitnessStatementsXpath())) {
            error = "Failed to validate that the Witness Statement panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.WitnessStatementsXpath());

            if (text.equals(" ")) {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Witness Statements"))) {
                narrator.stepPassed("Successfully validated that the Witness Statement panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.EquipmentInvolvedTabXpath())) {
            error = "Failed to validate that the Equipment Involved panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.EquipmentInvolvedTabXpath());

            if (text.equals(" ")) {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Equipment Involved"))) {
                narrator.stepPassed("Successfully validated that the Equipment Involved panel is displayed: " + text);
            }
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Persons Involved, Witness Statement and Equipment Involved panels are displayed");
        return true;
    }

    public boolean equipment() {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath())) {
            error = "Failed to wait for Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath())) {
            error = "Failed to click Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.EuipmentValidateGridIsNotEditableXPath())) {
            error = "Failed to validate that the editable grid is displayed";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the Equipment & Tools non-editable grid is displayed");
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentAddXpath())) {
            error = "Failed to click Equipment and Tools Add button";
            return false;
        }
//        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 4)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
//        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 4)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
//        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 99999)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 99999)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }

        
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.assetTypeDropDownXpath(), 99999)) {
            error = "Failed to wait for Equipment and Tools - Asset Type ";
            return false;
        }    
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.assetTypeDropDownXpath())) {
            error = "Failed to click Equipment and Tools - Asset Type ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionContainsXpath(getData("Asset Type")))) {
            error = "Failed to click Equipment and Tools Asset Type option - " + getData("Asset Type");
            return false;
        }

        String[] asset = getData("Asset").split(",");
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.assetDropDownXpath())) {
            error = "Failed to click asset dropdown";
            return false;
        }

        for (int k = 0; k < asset.length; k++) {

            if (k + 1 == asset.length) {
                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionXpath(asset[k]))) {
                    error = "Failed to click asset - " + asset[k];
                    return false;
                }
                narrator.stepPassed("SuccessFully asset : " + asset[k]);
            } else {
                if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveDropdownXpath(asset[k]))) {
                    error = "Failed to click asset - " + asset[k];
                    return false;
                }
                narrator.stepPassed("Successfully selected asset type : " + asset[k]);
            }
        }
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.equipmentDescXpath(), getData("Equipment description"))) {
            error = "Failed to enter Equipment and Tools - Equipment description - " + getData("Equipment description");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.descrOfDamageXpath(), getData("Description of damage"))) {
            error = "Failed to enter Equipment and Tools - Description of damage - " + getData("Description of damage");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.damageCostXpath(), getData("Damage cost"))) {
            error = "Failed to enter Equipment and Tools - Damage cost - " + getData("Damage cost");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.equipmentDescXpath())) {
            error = "Failed to click Equipment and Tools - Asset Type ";
            return false;
        }

        SeleniumDriverInstance.pause(1500);
   
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.operatorEmTypeDropdownpath())) {

            error = "Failed to click Equipment and Tools - Operator employment type ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator employment type")))) {
            error = "Failed to select Equipment and Tools - Operator employment type - " + getData("Operator employment type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.operatorDropdownpath())) {
            error = "Failed to click Equipment and Tools - Operator  ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator")))) {
            error = "Failed to select Equipment and Tools - Operator  - " + getData("Operator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Operator")))) {
            error = "Failed to select Equipment and Tools - Operator  - " + getData("Operator");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.EquipAndTools_Save_Xpath())) {
            error = "Failed to wait for Equipment And Tools Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.EquipAndTools_Save_Xpath())) {
            error = "Failed to click Equipment And Tools Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.equipAndToolsId())) {
            error = "Failed to wait for Equipment & Tools Record number.";
            return false;
        }

        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.equipAndToolsId()).split("#");
        String equipAndToolsRec = Actionid[1];
        narrator.stepPassed("Equipment & Tools Record Number: Record #" + equipAndToolsRec);

//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.motorVehiTabXpath()))
//        {
//            error = "Failed to click Equipment and Tools - Motor Vehicle Accident Information tab";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully validated that the Motor Vechile Accident Information is displayed");
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.timeOfDayDropdownpath()))
//        {
//            error = "Failed to click Equipment and Tools - time Of Day dropdown";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Time of Day"))))
//        {
//            error = "Failed to select Equipment and Tools - time Of Day - " + getData("Time of Day");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.weatherCondDropdownpath()))
//        {
//            error = "Failed to click Equipment and Tools - Weather Conditions dropdown";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Weather Conditions"))))
//        {
//            error = "Failed to select Equipment and Tools - Weather Conditions - " + getData("Weather Conditions");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.roadConditionsCondDropdownpath()))
//        {
//            error = "Failed to click Equipment and Tools - Road Conditions dropdown";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Road Conditions"))))
//        {
//            error = "Failed to select Equipment and Tools - Road Conditions - " + getData("Road Conditions");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.roadTypeDropdownpath()))
//        {
//            error = "Failed to click Equipment and Tools - Road Type dropdown";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Road Type"))))
//        {
//            error = "Failed to select Equipment and Tools - Road Type - " + getData("Road Type");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.InsuranceCoverDropdownpath()))
//        {
//            error = "Failed to click Equipment and Tools - Insurance covered by Company dropdown";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyActiveOptionDropdownXpath(getData("Insurance covered by Company"))))
//        {
//            error = "Failed to select Equipment and Tools - Insurance covered by Company- " + getData("Insurance covered by Company");
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(VerificationAndAdditionalPageObject.deductibleAmountXpath(), getData("Deductible Amount")))
//        {
//            error = "Failed to enter Equipment and Tools - Deductible Amount - " + getData("Deductible Amount");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.saveEquipToolsXpath()))
//        {
//            error = "Failed to click  Equipment and Tools  save button";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully entered Equipment and Tools details");
//
//        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 4))
//        {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40))
//            {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
//        
//        String[] record = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.getAssetRecord())).getText().split("#");   
//        assetId = record[1];
//
////        if (!SeleniumDriverInstance.waitForElementPresentByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath()))
////        {
////            error = "Failed to click on the Save And Continue button 1";
////            return false;
////        } else
////        {
////            String text = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordSavedMessageXPath());
////
////            if (text.equals(" "))
////            {
////                error = "String cannot be empty";
////                return false;
////            } else if (text.equals(testData.getData("Record Saved")))
////            {
////                
////            }
////        }
//        narrator.stepPassedWithScreenShot("Successfully saved the Equipment & Tools record: " + assetId);
//        
//      
//        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.closeEquipToolXpath(),8))
//        {
//            error = "Failed to click  Equipment and Tools  close button";
//            return false;
//        }
//        SeleniumDriverInstance.pause(1000);
//        if (!SeleniumDriverInstance.clickElementByJavascript(VerificationAndAdditionalPageObject.closeEquipToolXpath()))
//        {
//            error = "Failed to click  Equipment and Tools  close button";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableEquipAndTool(), 20))
//        {
//            error = "Failed to click save Equipment and Tools details";
//            return false;
//        }
//        
//         if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.recordXpath(assetId),20)) {
//            error = "Failed to click to incident management record id : "+getRecordId();
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.waitTableEquipAndTool(), 20))
//        {
//            error = "Failed to save Equipment and Tools details";
//            return false;
//        }
////        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.euipmentInvolvedTabXpath()))
////        {
////            error = "Failed to click Equipment and Tools tab ";
////            return false;
////        }
        return true;
    }
}
