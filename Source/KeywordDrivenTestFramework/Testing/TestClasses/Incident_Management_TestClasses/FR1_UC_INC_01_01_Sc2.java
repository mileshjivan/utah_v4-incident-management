/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Logging an Incident without ticking the Project checkbox",
        createNewBrowserInstance = false
)

public class FR1_UC_INC_01_01_Sc2 extends BaseClass
{

    String error = "";

    public FR1_UC_INC_01_01_Sc2()
    {
        SikuliDriverUtility sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!loggingAnIncident())
        {
            return narrator.testFailed("Failed to log an Incident without ticking the Project checkbox - " + error);
        }

        return narrator.finalizeTest("Successfully logged an Incident without ticking the Project checkbox");
    }

    public boolean loggingAnIncident()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame ";
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame ";
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.environmentalHealth()))
        {
            error = "Failed to wait for Environmental Health Safety";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.environmentalHealth()))
        {
            error = "Failed to click Environmental Health Safety";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath()))
        {
            error = "Failed to click Incident Managment";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 60))
            {
                error = " Reached timeout - Saving ";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.AddNewBtn()))
        {
            error = "Failed to wait for add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.AddNewBtn()))
        {
            error = "Failed to click add button";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.incidenttitleXpath(), getData("Incident title")))
        {
            error = "Failed to enter  Incident title";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.IncidentDescription(), getData("Incident Description")))
        {
            error = "Failed to enter  Incident Description";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentOccured()))
        {
            error = "Failed to click Section where incident occurred dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.globalCompanyXpath()))
        {
            error = "Failed to click global Company dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.southAfricaXpath()))
        {
            error = "Failed to click South Africa dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.victorySiteXpath()))
        {
            error = "Failed to click Victory Site ";
            return false;
        }

        String occured = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.occuredText())).getText();

        narrator.stepPassed("Successfully selected an item for the Section where incident occurred dropdown: " + occured);

        if (!SeleniumDriverInstance.enterTextByXpath(IncidentPageObjects.specificLocationXpath(), getData("Specific Location")))
        {
            error = "Failed to enter to Specific Location ";
            return false;
        }

        narrator.stepPassed("Successfully entered the Specific location: " + getData("Specific Location"));

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.PinToMap()))
        {
            error = "Failed to click  Pin to Map";
            return false;
        }

        if (SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Project()))
        {
            error = "Failed to click the Project dropdown";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Project field is not displayed when the 'Link to a project?' checkbox has not been checked :");

        return true;
    }
}
