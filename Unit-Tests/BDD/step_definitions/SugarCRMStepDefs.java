/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BDD.step_definitions;

import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.sugarcrm.pages.SugarCRMPages;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author amarais
 */
public class SugarCRMStepDefs extends SugarCRMPages
{
    private SugarCRMPages pages;
    private final WebDriver driver = new ChromeDriver();
    
    @Before
    public void init(){
        pages = new SugarCRMPages();
    }

    @Given("^I login with my username '(.+)' and password '(.+)' of SugarCRM$")
    public void i_login_with_my_username_swkhumalo_and_password_sibusiso2015_of_sugarcrm(String username, String password){
        checkResult(pages.logintoSugarCRM(username, password));
    }

    @Then("^I should see my home page '(.+)' '(.+)'$")
    public void i_should_see_my_home_page(String firstname, String lastname){
        checkResult(pages.validateHomeScreen(firstname, lastname));
    }
    
    @Given("^I navigate to the Contacts page$")
    public void i_navigate_to_the_contacts_page(){
        checkResult(pages.navigateToContacts());
    }

    @When("^I create a contact with first name '(.+)', last name '(.+)', and email '(.+)'$")
    public void i_create_a_contact_with_first_name_last_name_and_email_(String firstname, String lastname, String email){
        checkResult(pages.createContact(firstname, lastname, email));
    }

    @Then("^I should see the contact with first name '(.+)' '(.+)' is created$")
    public void i_should_see_the_contact_with_first_name_is_created(String firstname, String lastname){
         checkResult(pages.validateContact(firstname, lastname));
    }
    
    @When("^I delete a contact with first name '(.+)'$")
    public void i_delete_a_contact_with_first_name_(String firstname){
        checkResult(pages.deleteContact(firstname));
    }

    @Then("^I should see the contact with first name '(.+)' '(.+)' is deleted  $")
    public void i_should_see_the_contact_with_first_name_is_deleted(String firstname, String lastname){
        
    }
    
    @When("^I logout of SugarCRM$")
    public void i_logout_of_SugarCRM(){
       checkResult(pages.logoutOfSugarCRM());
    }

    @Then("^I should see login page$")
    public void i_should_see_login_page(){
        checkResult(pages.validateLogout());
    }
    public TestResult checkResult(String result)
    {
        if (result != null)
        {
            return narrator.testFailed("Failed due - ");
        }
        return narrator.finalizeTest("Completed navigate to Ad-Hoc Actions");
    }
    @After()
     public void closeBrowser() {
       driver.quit();
     }

}
